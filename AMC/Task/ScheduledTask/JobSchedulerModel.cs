﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Task.ScheduledTask
{
    public class JobSchedulerModel
    {
        public string ClassName { get; set; }
        public string TriggerName { get; set; }
        public string GroupName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int IntervalInSecond { get; set; }
    }
}