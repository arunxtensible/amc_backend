﻿using AMC.DBContext;
using AMC.Models;
using OfficeOpenXml;
using Quartz;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Web;
using System.Web.Configuration;

namespace AMC.Task.ScheduledTask
{
    public class WaterPressureAlertEmailSender : IJob
    {
        AMCDBContext _db = new AMCDBContext();
        public System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            try
            {
                MailSender _sender = new MailSender();
                _sender.SendMail("Pressure");
            }
            catch (Exception ex)
            {
            }

            return null;
        }

    }
}