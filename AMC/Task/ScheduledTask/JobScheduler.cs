﻿using AMC.DBContext;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Task.ScheduledTask
{
    public static class JobScheduler
    {
       
        public static void Start()
        {
            AMCDBContext _db = new AMCDBContext();
            try
            {
                // Grab the Scheduler instance from the Factory
                ISchedulerFactory schedFact = new StdSchedulerFactory();
                IScheduler scheduler = schedFact.GetScheduler().GetAwaiter().GetResult();
             
                
                // and shut down it off
                //scheduler.Shutdown();
                // and start it off
                scheduler.Start();
                scheduler.Clear();

                List<JobSchedulerModel> jobList = new List<JobSchedulerModel>();
                try
                {
                    using (_db = new AMCDBContext())
                    {

                        jobList = _db.Database.SqlQuery<JobSchedulerModel>("EXEC GetJobList").ToList();
                    }

                }
                catch (Exception ex)
                {
                }


               
                if (jobList.Any())
                {
                    foreach (var jobDetails in jobList)
                    {
                        // define the job and tie it to our  class  JobBuilder.Create(Type.GetType(cls name, true))   or JobBuilder.Create<cls name>()
                        IJobDetail job = JobBuilder.Create(Type.GetType(jobDetails.ClassName, true)).WithIdentity(jobDetails.ClassName, jobDetails.GroupName).Build();
                       
                        // Trigger the job to run, and then repeat every 24 hours
                        ITrigger trigger = TriggerBuilder.Create()
                            .WithIdentity(jobDetails.TriggerName, jobDetails.GroupName)
                            .StartAt(new DateTimeOffset(jobDetails.StartDate.Year, jobDetails.StartDate.Month, jobDetails.StartDate.Day, jobDetails.StartDate.Hour, jobDetails.StartDate.Minute, jobDetails.StartDate.Second, DateTimeOffset.Now.Offset)) // Start at yyy/MM/ : 12.30 AM and run every 24 hours                    
                            .WithSimpleSchedule(x => x
                                .WithIntervalInSeconds(jobDetails.IntervalInSecond)
                                .RepeatForever())
                            .Build();
                        scheduler.ScheduleJob(job, trigger);
                        
                    }
                   
                }
               
              

            }
            catch (SchedulerException se)
            {
                // Error logging
            }
        }
    }
}