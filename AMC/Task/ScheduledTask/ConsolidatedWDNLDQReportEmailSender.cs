﻿
using AMC.DBContext;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Task.ScheduledTask
{
    public class ConsolidatedWDNLDQReportEmailSender : IJob
    {

        AMCDBContext _db = new AMCDBContext();
        public System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            try
            {
                MailSender _sender = new MailSender();
                _sender.SendMail("ConsolidatedWDNLDQReportEmailSender");
            }
            catch (Exception ex)
            {
            }

            return null;
        }
    }
}