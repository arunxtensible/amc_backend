﻿using AMC.DBContext;
using AMC.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Web;
using System.Web.Configuration;

namespace AMC.Task.ScheduledTask
{
    public class MailSender
    {
        AMCDBContext _db = new AMCDBContext();
        public void SendMail(string AlertName)
        {           

            string from = WebConfigurationManager.AppSettings["EmailId"];
            string PWD = WebConfigurationManager.AppSettings["EmailPWD"];
            List<AlertZoneModel> alerts = new List<AlertZoneModel>();
            try
            {
                using (_db = new AMCDBContext())
                {
                    try
                    {
                            alerts = _db.Database.SqlQuery<AlertZoneModel>("GetTodaysAlertZoneAlertName @AlertName={0}", AlertName).ToList();
                            if (alerts.Any())
                            {
                                foreach (var item in alerts)
                                {
                                    try
                                    {
                                        SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);

                                        smtpClient.UseDefaultCredentials = false;
                                        smtpClient.Credentials = new System.Net.NetworkCredential(from, PWD);
                                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                                        smtpClient.EnableSsl = true;
                                        MailMessage mail = new MailMessage();
                                        //Setting From , To and CC
                                        mail.From = new MailAddress(from, "AMC Water Plant");
                                        mail.IsBodyHtml = true;
                                        mail.Subject = item.Subject;
                                        string MailTo = item.MailTo;
                                        foreach (var address in MailTo.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                                        {
                                            mail.To.Add(address);
                                        }
                                        string MailCC = item.MailCC;
                                        foreach (var address in MailCC.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                                        {
                                            mail.CC.Add(address);
                                        }
                                        string MailBCC = item.MailCC;
                                        foreach (var address in MailBCC.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                                        {
                                            mail.Bcc.Add(address);
                                        }
                                        if (mail.To.Count > 0 || mail.CC.Count > 0 || mail.Bcc.Count > 0)
                                        {
                                                mail.Body = "Dear Sir/Madam, <p>Please find the attached alerts/report.</p>";
                                                if (AlertName != "ConsolidatedWDSLDQReportEmailSender" && AlertName != "ConsolidatedWDNLDQReportEmailSender" && AlertName != "WDNwithParentWDSReportEmailSender")
                                                {
                                                    mail.Attachments.Add(GetAttachment(GetAlertData(AlertName, item.ZoneName)));
                                                }
                                                else
                                                {
                                                    mail.Attachments.Add(GetReportAttachment(AlertName,item.ZoneName));
                                                }
                                            smtpClient.Send(mail);
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                            }
                            alerts.Clear();
                        
                    }
                    catch (Exception ex1)
                    {
                    }

                }
            }
            catch (Exception ex)
            {
            }
           
        }

        public Attachment GetAttachment(DataTable dataTable)
        {
            MemoryStream outputStream = new MemoryStream();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(outputStream))
            {
                ExcelWorksheet facilityWorksheet = package.Workbook.Worksheets.Add("sheet1");
                facilityWorksheet.Cells.LoadFromDataTable(dataTable, true);

                package.Save();
            }

            outputStream.Position = 0;
            Attachment attachment = new Attachment(outputStream, "Alert.xlsx", "application/vnd.ms-excel");

            return attachment;
        }

        public DataTable GetAlertData(string alertName, string zone)
        {
            AMCDBContext _db = new AMCDBContext();
            List<MailAlertModel> list = new List<MailAlertModel>();
            using (_db = new AMCDBContext())
            {

                list = _db.Database.SqlQuery<MailAlertModel>("GetAlertsDetailsForMail @AlertName={0}, @Zone={1}", alertName, zone).ToList();

            }

            return ToDataTable(list);
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }



        public Attachment GetReportAttachment(string ExcelFileName, string Zone)
        {

            MemoryStream outputStream = new MemoryStream();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            if (ExcelFileName == "ConsolidatedWDSLDQReportEmailSender")
            {
                using (ExcelPackage pck = new ExcelPackage(outputStream))
                {

                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");
                    ws.Cells["A1"].Value = "CLIENT";
                    ws.Cells["B1"].Value = "Smart City Ahmedabad Development Limited (SCADAL)";
                    ws.Cells["A2"].Value = "AGENCY";
                    ws.Cells["B2"].Value = "Chetas Control Systems Pvt Ltd Pune";
                    ws.Cells["A3"].Value = "Report Type";
                    ws.Cells["B3"].Value = "All WDS Consolidated LDQ Data Report";
                    ws.Cells["A4"].Value = "Date Of Report";
                    ws.Cells["B4"].Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    ws.Cells[1, 2, 1, 5].Merge = true;
                    ws.Cells[2, 2, 2, 5].Merge = true;
                    ws.Cells[3, 2, 3, 5].Merge = true;
                    ws.Cells[4, 2, 4, 5].Merge = true;




                    ws.Cells["A6"].Value = "Sr. No.";
                    ws.Cells["B6"].Value = "LocationName";
                    ws.Cells["C6"].Value = "Zone";
                    ws.Cells["D6"].Value = "LDQ (m3)";

                    List<WDSLdqReportDataModel> reportList = new List<WDSLdqReportDataModel>();
                    try
                    {
                        using (_db = new AMCDBContext())
                        {
                            _db.Database.CommandTimeout = 600;
                            reportList = _db.Database.SqlQuery<WDSLdqReportDataModel>("EXEC GetWDSLdqReport @DateAndtime={0}, @Zone={1}", DateTime.Now.ToString("dd-MMM-yyyy"), Zone).ToList();
                        }

                    }
                    catch (Exception ex)
                    {
                    }

                    int rowStart = 7;
                    int i = 1;
                    if (reportList.Any())
                    {
                        foreach (var item in reportList)
                        {


                            ws.Cells[string.Format("A{0}", rowStart)].Value = i;
                            ws.Cells[string.Format("B{0}", rowStart)].Value = item.LocationName;
                            ws.Cells[string.Format("C{0}", rowStart)].Value = item.Zone;
                            ws.Cells[string.Format("D{0}", rowStart)].Value = item.LDQ;
                            rowStart++;
                            i++;
                        }
                    }
                    ws.Cells["A:AZ"].AutoFitColumns();

                    pck.Save();
                }
            }
            else if (ExcelFileName == "ConsolidatedWDNLDQReportEmailSender")
            {
                using (ExcelPackage pck = new ExcelPackage(outputStream))
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");
                    ws.Cells["A1"].Value = "CLIENT";
                    ws.Cells["B1"].Value = "Smart City Ahmedabad Development Limited (SCADAL)";
                    ws.Cells["A2"].Value = "AGENCY";
                    ws.Cells["B2"].Value = "Chetas Control Systems Pvt Ltd Pune";
                    ws.Cells["A3"].Value = "Report Type";
                    ws.Cells["B3"].Value = "All WDN Consolidated LDQ Data Report";
                    ws.Cells["A4"].Value = "Date Of Report";
                    ws.Cells["B4"].Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    ws.Cells[1, 2, 1, 7].Merge = true;
                    ws.Cells[2, 2, 2, 7].Merge = true;
                    ws.Cells[3, 2, 3, 7].Merge = true;
                    ws.Cells[4, 2, 4, 7].Merge = true;




                    ws.Cells["A6"].Value = "Sr.No.";
                    ws.Cells["B6"].Value = "LocationName";
                    ws.Cells["C6"].Value = "Unit";
                    ws.Cells["D6"].Value = "Zone";
                    ws.Cells["E6"].Value = "Ward";
                    ws.Cells["F6"].Value = "LDQ";


                    List<WDNLdqReportDataModel> reportList = new List<WDNLdqReportDataModel>();
                    try
                    {
                        using (_db = new AMCDBContext())
                        {
                            _db.Database.CommandTimeout = 600;
                            reportList = _db.Database.SqlQuery<WDNLdqReportDataModel>("EXEC GetWDNLdqReport @DateAndtime={0}, @Zone={1}", DateTime.Now.ToString("dd-MMM-yyyy"), Zone).ToList();
                        }

                    }
                    catch (Exception ex)
                    {
                    }
                    int rowStart = 7;
                    int i = 1;
                    if (reportList.Any())
                    {
                        foreach (var item in reportList)
                        {


                            ws.Cells[string.Format("A{0}", rowStart)].Value = i;
                            ws.Cells[string.Format("B{0}", rowStart)].Value = item.LocationName;
                            ws.Cells[string.Format("C{0}", rowStart)].Value = item.Unit;
                            ws.Cells[string.Format("D{0}", rowStart)].Value = item.Zone;
                            ws.Cells[string.Format("E{0}", rowStart)].Value = item.Ward;
                            ws.Cells[string.Format("F{0}", rowStart)].Value = item.LDQ;
                            rowStart++;
                            i++;
                        }
                    }
                    ws.Cells["A:AZ"].AutoFitColumns();

                    pck.Save();
                }
            }
            else if (ExcelFileName == "WDNwithParentWDSReportEmailSender")
            {
                using (ExcelPackage pck = new ExcelPackage(outputStream))
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");
                    ws.Cells["A1"].Value = "CLIENT";
                    ws.Cells["B1"].Value = "Smart City Ahmedabad Development Limited (SCADAL)";
                    ws.Cells["A2"].Value = "AGENCY";
                    ws.Cells["B2"].Value = "Chetas Control Systems Pvt Ltd Pune";
                    ws.Cells["A3"].Value = "Report Type";
                    ws.Cells["B3"].Value = "WDN-WDS Report";
                    ws.Cells["A4"].Value = "Date Of Report";
                    ws.Cells["B4"].Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    ws.Cells[1, 2, 1, 5].Merge = true;
                    ws.Cells[2, 2, 2, 5].Merge = true;
                    ws.Cells[3, 2, 3, 5].Merge = true;
                    ws.Cells[4, 2, 4, 5].Merge = true;




                    ws.Cells["A6"].Value = "Sr. No.";
                    ws.Cells["B6"].Value = "Zone";
                    ws.Cells["C6"].Value = "Parent WDS";
                    ws.Cells["D6"].Value = "Ward";
                    ws.Cells["E6"].Value = "WDS Outlet Volume";
                    ws.Cells["F6"].Value = "WDN Location";
                    ws.Cells["G6"].Value = "Diameter (MTR)";
                    ws.Cells["H6"].Value = "Totaliser (m3)";
                    ws.Cells["I6"].Value = "Reverse Totaliser (m3)";
                    ws.Cells["J6"].Value = "Flow (m3)";
                    ws.Cells["K6"].Value = "CDQ (m3)";
                    ws.Cells["L6"].Value = "LDQ (m3)";
                    ws.Cells["M6"].Value = "Reverse CDQ (m3)";
                    ws.Cells["N6"].Value = "Reverse LDQ (m3)";
                    ws.Cells["O6"].Value = "Pressure (PSI)";
                    ws.Cells["P6"].Value = "Voltage (VDC)";
                    ws.Cells["Q6"].Value = "Power Status";
                    ws.Cells["R6"].Value = "Door Status";


                    List<ReportDataModelWDNDetailsWithParentWDS> reportList = new List<ReportDataModelWDNDetailsWithParentWDS>();
                    try
                    {
                        using (_db = new AMCDBContext())
                        {
                            _db.Database.CommandTimeout = 600;
                            reportList = _db.Database.SqlQuery<ReportDataModelWDNDetailsWithParentWDS>("EXEC Report_WDNWithParentWDSDetails @Zone={0},@ParentWDS={1},@Ward={2},@Location={3},@Datetime={4}", Zone, "All", "All", "All", DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")).ToList();

                        }

                    }
                    catch (Exception ex)
                    {
                    }

                    int rowStart = 7;
                    int i = 1;
                    if (reportList.Any())
                    {
                        foreach (var item in reportList)
                        {
                            ws.Cells[string.Format("A{0}", rowStart)].Value = i;
                            ws.Cells[string.Format("B{0}", rowStart)].Value = item.Zone;
                            ws.Cells[string.Format("C{0}", rowStart)].Value = item.ParentWDS;
                            ws.Cells[string.Format("D{0}", rowStart)].Value = item.Ward;
                            ws.Cells[string.Format("E{0}", rowStart)].Value = item.WDSOutletVolume;
                            ws.Cells[string.Format("F{0}", rowStart)].Value = item.LocationName;
                            ws.Cells[string.Format("G{0}", rowStart)].Value = item.Diameter;
                            ws.Cells[string.Format("H{0}", rowStart)].Value = item.Totaliser;
                            ws.Cells[string.Format("I{0}", rowStart)].Value = item.ReverseTotaliser;
                            ws.Cells[string.Format("J{0}", rowStart)].Value = item.Flow;
                            ws.Cells[string.Format("K{0}", rowStart)].Value = item.CDQ;
                            ws.Cells[string.Format("L{0}", rowStart)].Value = item.LDQ;
                            ws.Cells[string.Format("M{0}", rowStart)].Value = item.ReverseCDQ;
                            ws.Cells[string.Format("N{0}", rowStart)].Value = item.ReverseLDQ;
                            ws.Cells[string.Format("O{0}", rowStart)].Value = item.Pressure;
                            ws.Cells[string.Format("P{0}", rowStart)].Value = item.BatteryVoltage;
                            ws.Cells[string.Format("Q{0}", rowStart)].Value = item.PowerStatus;
                            ws.Cells[string.Format("R{0}", rowStart)].Value = item.DoorStatus;
                            rowStart++;
                            i++;
                        }
                    }
                    ws.Cells["A:AZ"].AutoFitColumns();

                    pck.Save();
                }
            }
            outputStream.Position = 0;
            Attachment attachment = new Attachment(outputStream, ExcelFileName.Replace("EmailSender", ""), "application/vnd.ms-excel");

            return attachment;
        }
       
    }
}