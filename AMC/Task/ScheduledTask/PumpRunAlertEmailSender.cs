﻿using AMC.DBContext;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Task.ScheduledTask
{
    public class PumpRunAlertEmailSender : IJob
    {
        AMCDBContext _db = new AMCDBContext();
        public System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            try
            {
                MailSender _sender = new MailSender();
                _sender.SendMail("pump_run");
            }
            catch (Exception ex)
            {
            }

            return null;
        }
    }
}