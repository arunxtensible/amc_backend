﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WardModel
    {
        public int WardID { get; set; }

        public int ZoneId { get; set; }

        public string WardName { get; set; }

    }
}