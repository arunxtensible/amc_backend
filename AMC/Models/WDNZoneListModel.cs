﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WDNZoneListModel
    {
        public int Id { get; set; }
        public string Zone { get; set; }
        public decimal Capacity { get; set; }
        public decimal LDQ { get; set; }
        public decimal CDQ { get; set; }

    }
}