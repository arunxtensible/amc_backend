﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class MapWDNNetworkByKPIModel
    {
        public string id { get; set; }
        public int srno { get; set; }
        public string name { get; set; }
        public string coordinate { get; set; }
        public bool isInstall { get; set; }
        public string kpi { get; set; }
    }

   
}