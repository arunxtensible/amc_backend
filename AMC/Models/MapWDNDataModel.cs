﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class MapWDNDataModel
    {
       // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
       
            public int srno { get; set; }
            public int id { get; set; }
            public int zoneId { get; set; }
            public int wardId { get; set; }
            public object wdsname { get; set; }
            public string locationName { get; set; }
            public object lineSize { get; set; }
            public object benchmarkWaterQtyinaday { get; set; }
            public object cdqNet { get; set; }
            public object cdqNegetative { get; set; }
            public object ldqNet { get; set; }
            public object ldqNegetative { get; set; }
            public object lastcommunicatedFlowRate { get; set; }
            public object lastCommunicatedLinePressure { get; set; }
            public object benchmarkMinPressureduringsupply { get; set; }
            public object minPressurePsiCurrentday { get; set; }
            public object maxPressureCurrentDay { get; set; }
            public DateTime lastCommunicated { get; set; }
            public object coordinate { get; set; }
            public bool isInstall { get; set; }
        
    }
}