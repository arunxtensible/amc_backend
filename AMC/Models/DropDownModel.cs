﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class DropDownModel
    {
        public object Value { get; set; }
        public string Name { get; set; }
    }
}