﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class MailAlertModel
    {
        public string SrNo { get; set; }
        public string Zone { get; set; }
        public string Ward { get; set; }
        public string Location { get; set; }
        public decimal LastValue { get; set; }
        public string LastUpdated { get; set; }
        public string description { get; set; }
    }
}