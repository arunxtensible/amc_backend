﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WTPInletOutletModel
    {
        public string WTPName { get; set; }
        public decimal Capacity { get; set; }
        public decimal LDQ { get; set; }
        public decimal CDQ { get; set; }
        public decimal UnitConsumption { get; set; }
        public int WTPId { get; set; }
        public DateTime LDRDateTime { get; set; }
    }
}