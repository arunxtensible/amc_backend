﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WDSInletOutletModel
    {
        public int ZoneId { get; set; }
        public string LDRDateTime { get; set; }
        public string  ZoneName { get; set; }
        public decimal  ZoneLDQ { get; set; }
        public decimal  ZoneCDQ { get; set; }
        public decimal ZoneUnitConsumption { get; set; }
    }
}