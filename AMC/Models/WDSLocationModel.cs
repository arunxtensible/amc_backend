﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WDSLocationModel
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
    }
}