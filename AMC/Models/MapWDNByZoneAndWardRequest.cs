﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class MapWDNByZoneAndWardRequest
    {
        public int zoneId { get; set; }
        public int wardId { get; set; }
    }

    public class MapWDSByZoneRequest
    {
        public int zoneId { get; set; }
    }
}