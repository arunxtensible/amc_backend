﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WDNLocationListModel
    {
        public int Id { get; set; }
        public int ZoneId { get; set; }
        public int WardId { get; set; }
        public string Location { get; set; }
        public decimal Capacity { get; set; }
        public decimal LDQ { get; set; }
        public decimal CDQ { get; set; }

    }
}