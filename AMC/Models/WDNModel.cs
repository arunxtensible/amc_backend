﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WDNModel
    {
        public int WDNID { get; set; }
        public string WDNUniqueId { get; set; }
        public int ZoneId { get; set; }
        public int WardId { get; set; }
        public int WDSId { get; set; }
        public string Location { get; set; }
        public decimal LineSize { get; set; }
        public decimal CDQNet { get; set; }
        public decimal CDQNegative { get; set; }
        public decimal LDQNet { get; set; }
        public decimal LDQNegative { get; set; }

        public decimal Benchmark_water_qty_in_days { get; set; }

        public decimal Last_communicated_flow_rate { get; set; }
        public decimal Last_communicated_line_pressure { get; set; }
        public decimal Min_pressure_psi_current_day { get; set; }

        public decimal Max_pressure_current_day { get; set; }

        public string Coordinate { get; set; }
        public bool IsInstalled { get; set; }

        public bool IsDeleted { get; set; }
        [Display(Name ="PLC IP Address")]
        public string PLCIPAddress { get; set; }
    }
}