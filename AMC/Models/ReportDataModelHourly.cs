﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class ReportDataModelHourly
    {
        public decimal FlowRate { get; set; }
        public decimal Totaliser { get; set; }
        public decimal Pressure { get; set; }
        public decimal CDQ { get; set; }
        public decimal LDQ { get; set; }
        public decimal BatteryVoltage { get; set; }
        public string DateAndTime { get; set; }
    }
    public class ReportDataModelMonthly
    {
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal PeakPressure { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal LDQ { get; set; }
        public string DateAndTime { get; set; }
    }


    public class ReportDataModelDB2DBDataHourly
    {
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double Flow { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double Totaliser { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double Pressure { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double CDQ { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double LDQ { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double BatteryVoltage { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class WDNLdqReportDataModel
    {
        public string LocationName { get; set; }
        public string Unit { get; set; }
        public string Zone { get; set; }
        public string Ward { get; set; }
        public decimal LDQ { get; set; }
    }

    public class WDSLdqReportDataModel
    {
        public string Zone { get; set; }
        public string LocationName { get; set; }
        public decimal LDQ { get; set; }
    }

    public class ReportDataModelWDSDetailsWithParentWTP
    {

        public string Zone { get; set; }
        public string ParentWTP { get; set; }
        public string LocationName { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal Diameter { get; set; }

        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal InletFlow { get; set; }

        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal OutletFlow { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal Pressure { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal PeakLevelMTR { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal PeakPressure { get; set; }
        public string TotalPumpingHour { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal KWH { get; set; }

        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal LDQInlet { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal LDQOutlet { get; set; }
    }
    public class ReportDataModelWDNDetailsWithParentWDS
    {
       
        public string Zone { get; set; }
        public string ParentWDS { get; set; }
        public string Ward { get; set; }
        public string LocationName { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal WDSOutletVolume { get; set; }

        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal Diameter { get; set; }

        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal Flow { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal Totaliser { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal ReverseTotaliser { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal Pressure { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal CDQ { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal LDQ { get; set; }

        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal ReverseCDQ { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal ReverseLDQ { get; set; }
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal BatteryVoltage { get; set; }
        public string PowerStatus { get; set; }
        public string DoorStatus { get; set; }
    }

}