﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class AlertMailConfigurationSettingModel
    {
        public string SrNo { get; set; }
        public string AlertName { get; set; }
        public string ZoneName { get; set; }
        public string mailTime { get; set; }
        public string MailTo { get; set; }
        public string MailCC { get; set; }
        public string MailBCC { get; set; }
        public string subject { get; set; }
    }

}