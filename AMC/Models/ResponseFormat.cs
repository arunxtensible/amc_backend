﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class ResponseFormat
    {
        public string StatusCode { get; set; }
        public string ResponseMessage { get; set; }
        public object Result { get; set; }
    }
}