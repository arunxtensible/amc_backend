﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class ReportParentWDS
    {
        public int Id { get; set; }
        public string ParentWDS { get; set; }
    }

    public class ReportWard
    {
        public int Id { get; set; }
        public string Ward { get; set; }
    }

    public class ReportWDNLocation
    {
        public int Id { get; set; }
        public string Location { get; set; }
    }
}