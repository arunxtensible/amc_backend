﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AMC.Models
{
    public class WDSInletOutletLocationModel
    {
        public string LocationName { get; set; }
        public decimal Capacity { get; set; }
        public decimal CDQ { get; set; }
        public decimal LDQ { get; set; }
        public decimal UnitConsumption { get; set; }
        public DateTime LDR_DateTime { get; set; }
        [NotMapped]
        public SelectList ZoneList { get; set; }
    }
}