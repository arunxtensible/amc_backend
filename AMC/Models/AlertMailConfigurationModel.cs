﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class AlertMailConfigurationModel
    {
        public int Id { get; set; } = 0;

        [DisplayName("Alert Type")]
        public string AlertType { get; set; } = "-";
        public string EastMailIds { get; set; } = "-";
        public string WestMailIds { get; set; } = "-";
        public string SouthMailIds { get; set; } = "-";
        public string NorthMailIds { get; set; } = "-";
        public DateTime MailTime { get; set; } = DateTime.Now;
        public string EmailSubject { get; set; } = "-";
    }
}