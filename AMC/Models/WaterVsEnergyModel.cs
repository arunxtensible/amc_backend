﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WaterVsEnergyModel
    {
        public decimal? panelstatus { get; set; }
        public string LastUpdated { get; set; }
        public string Location { get; set; }
        public string description { get; set; }
    }
}