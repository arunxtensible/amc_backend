﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class UFWModel
    {
        public int Id { get; set; }
        public string LocationName { get; set; }
        public string LocationsTappingFrom { get; set; }
        public decimal QtyInMLD { get; set; }
        public string Type { get; set; }
        public decimal Total { get; set; }
    }
}