﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class AlertZoneModel
    {
        public string ZoneName { get; set; }
        public string Subject { get; set; }
        public string MailTo { get; set; }
        public string MailCC { get; set; }
        public string MailBCC { get; set; }
								
    }
}