﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class MapWDNNetworkByWardModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public int id { get; set; }
        public string coordinate { get; set; }
        public string name { get; set; }
        public string wdsname { get; set; }
        public decimal lineSize { get; set; }
        public decimal batteryvoltage { get; set; }
        public decimal cdq { get; set; }
        public decimal negative_CDQ { get; set; }
        public decimal ldq { get; set; }
        public decimal negative_LDQ { get; set; }
        public decimal flow { get; set; }
        public decimal pressure { get; set; }
        public decimal totaliser { get; set; }
        public decimal totaliser2 { get; set; }
        public string mainSupplyStatus { get; set; }
        public string panelStatus { get; set; }
        public DateTime ldr { get; set; }


                                           //public int id { get; set; }
                                           //public int srno { get; set; }
                                           //public string name { get; set; }
                                           //public string coordinate { get; set; }
                                           //public bool isInstall { get; set; }
                                           //public int wardId { get; set; }
                                           //public string wdsname { get; set; }
                                           //public string locationName { get; set; }
                                           //public string lineSize { get; set; }
                                           //public decimal benchmarkWaterQtyinaday { get; set; }
                                           //public decimal cdqNet { get; set; }
                                           //public decimal cdqNegetative { get; set; }
                                           //public decimal ldqNet { get; set; }
                                           //public decimal ldqNegetative { get; set; }
                                           //public decimal lastcommunicatedFlowRate { get; set; }
                                           //public decimal lastCommunicatedLinePressure { get; set; }
                                           //public decimal benchmarkMinPressureduringsupply { get; set; }
                                           //public decimal minPressurePsiCurrentday { get; set; }
                                           //public decimal maxPressureCurrentDay { get; set; }
                                           //public string lastCommunicated { get; set; }
                                           //public DateTime today { get; set; }
                                           //public DateTime prevday { get; set; }
                                           //public DateTime ldr { get; set; }
                                           //public decimal diameter { get; set; }
                                           //public decimal cdq { get; set; }
                                           //public decimal prevcdq { get; set; }
                                           //public decimal ldq { get; set; }
                                           //public decimal prevldq { get; set; }
                                           //public decimal revcdq { get; set; }
                                           //public decimal prevrevcdq { get; set; }
                                           //public decimal revldq { get; set; }
                                           //public decimal prevrevldq { get; set; }
                                           //public decimal door { get; set; }
                                           //public decimal btry { get; set; }
                                           //public decimal totaliser { get; set; }
                                           //public decimal totaliser2 { get; set; }
                                           //public decimal prevtotaliser { get; set; }
                                           //public decimal prevtotaliser2 { get; set; }
                                           //public decimal flow { get; set; }
                                           //public decimal prevflow { get; set; }
                                           //public decimal pressure { get; set; }
                                           //public decimal prevpressure { get; set; }
                                           //public decimal batteryvoltage { get; set; }
                                           //public decimal prevbatteryvoltage { get; set; }
                                           //public decimal negative_CDQ { get; set; }
                                           //public decimal negative_LDQ { get; set; }
                                           //public bool mainSupplyStatus { get; set; }
                                           //public int panelStatus { get; set; }



    }
}