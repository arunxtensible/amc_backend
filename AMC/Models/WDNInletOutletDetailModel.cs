﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WDNInletOutletDetailModel
    {
        public int siteId { get; set; }
        public string power_status { get; set; }
        public string door_status { get; set; }
        public decimal totalisedflow1_current { get; set; }
        public decimal totalisedflow2_current { get; set; }
        public decimal flow_current { get; set; }
        public decimal LDQ_current { get; set; }
        public decimal CDQ_current { get; set; }
        public decimal negative_ldq_current { get; set; }
        public string KPI_current { get; set; }
        public decimal negative_cdq_current { get; set; }
        public decimal pressure_current { get; set; }
        public decimal peakpressure_current { get; set; }
        public decimal batteryvoltage_current { get; set; }

        public string KPI_Prev { get; set; }
        public decimal totalisedflow1_prev { get; set; }
        public decimal totalisedflow2_prev { get; set; }
        public decimal flow_prev { get; set; }
        public decimal LDQ_prev { get; set; }
        public decimal CDQ_prev { get; set; }
        public decimal negative_cdq_prev { get; set; }
        public decimal negative_ldq_prev { get; set; }
        public decimal pressure_prev { get; set; }
        public decimal peakpressure_prev { get; set; }
        public decimal batteryvoltage_prev { get; set; }
        public string ldr_date { get; set; }

        public string ParentWDS { get; set; }
        public decimal Diameter { get; set; }
        public int ZoneId { get; set; }
    }
}