﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AMC.Models
{
    public class DataModel
    {
        [NotMapped]
        public SelectList ZoneList { get; set; }
    }
}