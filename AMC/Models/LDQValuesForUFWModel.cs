﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class LDQValuesForUFWModel
    {
        public string WTPOutletLDQ { get; set; }
        public string WDSInletLDQ { get; set; }
        public string UFWLastDayMLD { get; set; }
        public string UFWKnownCionsumption { get; set; }
        public string UFWMLD { get; set; }
    }
}