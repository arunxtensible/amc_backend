﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WDNSynchronisationModel
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}