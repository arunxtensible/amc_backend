﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class MapWDSDataModel
    {    
        public int WDSId { get; set; }
        public string WDSName { get; set; }
        public string coordinate { get; set; }
    }

    public class MapWDSNetworkByKPIModel
    {
        public int WDSId { get; set; }
        public string WDSName { get; set; }
        public string coordinate { get; set; }
        public string kpi { get; set; }
    }
}