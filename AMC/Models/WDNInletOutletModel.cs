﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WDNInletOutletModel
    {
        public string WDNName { get; set; }
        public decimal Capacity { get; set; }
        public decimal LDQ { get; set; }
        public decimal CDQ { get; set; }
        public int WDNId { get; set; }

        public decimal Capacity_Sum { get; set; }
        public decimal LDQ_Sum { get; set; }
        public decimal CDQ_Sum { get; set; }
        public string LDRDateAndTime { get; set; }
    }
}