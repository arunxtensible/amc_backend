﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AMC.Models
{
    public class DashboardAlertsModel
    {
        public decimal TamperingAlerts { get; set; } = 0;
        public decimal WaterPressureAlerts { get; set; } = 0;
        public decimal WaterQualityAlerts { get; set; } = 0;
        public decimal PowerCutAlerts { get; set; } = 0;
        public decimal CommunicationAlerts { get; set; } = 0;
        public decimal FlowDeviiationAlerts { get; set; } = 0;
        public decimal PumpingHourAlerts { get; set; } = 0;
        public decimal ZeroFlowAlerts { get; set; } = 0;
        public decimal WaterVSEnergyAlerts { get; set; } = 0;
        public decimal BatteryVoltageAlerts { get; set; } = 0;
        public decimal WaterVSEnergy { get; set; } = 0;
        public decimal PopulationVSDemandCurrentDay { get; set; } = 0;
        public decimal PopulationVSDemandPreviousDay { get; set; } = 0;
    }
}