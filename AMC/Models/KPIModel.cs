﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class KPIModel
    {
        public int KpiId { get; set; }
        public string KpiName { get; set; }
    }
}