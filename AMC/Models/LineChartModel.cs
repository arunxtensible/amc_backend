﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class LineChartModel
    {
        public string name { get; set; }
        public decimal[] data { get; set; }
    }
    public class LineChartIterationDateModel
    {
        public string DateAndTime { get; set; }
    }
    public class LineChartIterationDateTimeModel
    {
        public DateTime DateAndTime { get; set; }
    }
    public class LineChartResponseModel
    {
        public LineChartModel[] ChartModel;
    }
}