﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class UserVM
    {
        public int UserID { get; set; }
        public string LoginId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailId { get; set; }
        public string ContactNo { get; set; }
        public int RoleId { get; set; }
        public int ZoneId { get; set; }
        public string Location { get; set; }
    }
}