﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WTPGroundWaterModel
    {
        public string LocationName { get; set; }
        public int RPT_INLET_FLOW_CRNT_TOTAL { get; set; }
        public int RPT_OUTLET_FLOW_CRNT_TOTAL { get; set; }
        public int last_day_qty { get; set; }
    }
}