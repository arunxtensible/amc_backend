﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WDNReportConfigurationModel
    {
        public int Id { get; set; } = 0;
        public string Zone { get; set; } = "-";
        public string Ward { get; set; } = "-";
        public string MailIds { get; set; } = "-";
        public DateTime MailTime { get; set; } = DateTime.Now;
        public string EmailSubject { get; set; } = "-";
        public string Type { get; set; } = "-";
    }
}