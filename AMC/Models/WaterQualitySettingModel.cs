﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WaterQualitySettingModel
    {
        [DisplayName("Parameter Name")]
        public string PropertyName { get; set; }

        [DisplayName("Generate alert if value less than")]
        public decimal Min { get; set; }

        [DisplayName("Generate alert if value greater than")]
        public decimal Max { get; set; }

        public TimeSpan TimeToRun { get; set; }
    }
}