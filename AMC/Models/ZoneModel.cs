﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class ZoneModel
    {
        public int ZoneID { get; set; }
        public string ZoneName { get; set; }
        public string HintText { get; set; }


    }
}