﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class MapZoneModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);         
            public int zoneId { get; set; }
            public string zoneName { get; set; }
            public string zoneCentralLocation { get; set; }
            public int wtpId { get; set; }
            public string wtPname { get; set; }
            public string kmlPath { get; set; } 

    }
}