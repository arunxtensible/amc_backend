﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AMC.Models
{
    public class ReportModel
    {
        [NotMapped]
        public SelectList ReportZoneList { get; set; }
    }

    public class WDNWDSReportModel
    {
        [NotMapped]
        public SelectList ReportZoneList { get; set; }
        public SelectList ReportParentWDSList { get; set; }
        public SelectList ReportWardList { get; set; }
        public SelectList ReportLocationList { get; set; }
    }

    public class WDSReportModel
    {
        [NotMapped]
        public SelectList ReportZoneList { get; set; }
    }
}