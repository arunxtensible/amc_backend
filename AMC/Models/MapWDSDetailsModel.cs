﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class MapWDSDetailsModel
    {
        public int WDSId { get; set; }
        public string WDSName { get; set; }
        public string ldr { get; set; }
        public decimal InletFlow { get; set; }
        public decimal SumpLevel { get; set; }
        public decimal OutletFlowRate { get; set; }
        public decimal Pressure { get; set; }
        public decimal InletVolume { get; set; }
        public decimal OutletVolume { get; set; }
        public decimal UnitConsumption { get; set; }
        public decimal KWHM3 { get; set; }
        public decimal SumpCapacity { get; set; }

        public string PumpStatus { get; set; }

        public string PumpDesign { get; set; }
        public string PumpRun { get; set; }
        public string IntakeSource { get; set; }

        public string KPI_Current { get; set; }
        public decimal PeakLevelMTR_Current { get; set; }
        public decimal InletVolume_Current { get; set; }
        public decimal OutletVolume_Current { get; set; }
        public decimal PeakPressure_Current { get; set; }
        public string TotalPumpingHour_Current { get; set; }
        public decimal UnitConsumption_Current { get; set; }
        public decimal KWHM3_Current { get; set; }

        



    }
}