﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class MapZoneLineModel
    {
        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
            public int id { get; set; }
            public string line { get; set; }
            public string coordinates { get; set; }
            public string color { get; set; }
            public int weight { get; set; }
            public double opacity { get; set; }
            public int zoneId { get; set; }
            public string setView { get; set; }
            public int zoomLevel { get; set; }
        
    }
}