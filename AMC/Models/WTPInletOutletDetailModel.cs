﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WTPInletOutletDetailModel
    {
        public decimal InletFlow { get; set; }
        public decimal SumpLevel { get; set; }
        public decimal OutletFlowRate { get; set; }
        public decimal Pressure { get; set; }
        public decimal SumpCapacity { get; set; }
        public string PumpStatus { get; set; }
        public string PumpDesign { get; set; }
        public string PumpRun { get; set; }
        public string IntakeSource { get; set; }

        public string KPI_Current { get; set; }
        public decimal PeakLevelMTR_Current { get; set; }
        public decimal InletVolume_Current { get; set; }
        public decimal OutletVolume_Current { get; set; }
        public decimal PeakPressure_Current { get; set; }
        public string TotalPumpingHour_Current { get; set; }
        //public decimal TotalPumpingHour_Current { get; set; }
        public decimal UnitConsumption_Current { get; set; }
        public decimal KWHM3_Current { get; set; }

        public string KPI_prev { get; set; }
        public decimal PeakLevelMTR_prev { get; set; }
        public decimal InletVolume_prev { get; set; }
        public decimal OutletVolume_prev { get; set; }
        public decimal PeakPressure_prev { get; set; }
        public string TotalPumpingHour_prev { get; set; }
        //public decimal TotalPumpingHour_prev { get; set; }
        public decimal UnitConsumption_prev { get; set; }
        public decimal KWHM3_prev { get; set; }
        public string LDR_DateTime { get; set; }
    }
}