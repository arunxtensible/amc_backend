﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WTPModel
    {
        public int WTPId { get; set; }
        public string WTPName { get; set; }
        public decimal Capacity { get; set; }
    }
}