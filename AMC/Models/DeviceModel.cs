﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class DeviceModel
    {
        public int ID { get; set; }

        public string DeviceName { get; set; }

        public string Coordinates { get; set; }
     
        public bool IsInstalled { get; set; }

        public bool IsDeleted { get; set; }
    }
}