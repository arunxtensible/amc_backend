﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WDSModel
    {
        public int WDSId { get; set; }
        public string WDSName { get; set; }
        public string Type { get; set; }

        public decimal Capacity { get; set; }

        public int PumpCount { get; set; }

        public string Coordinates { get; set; }

        public int ZoneId { get; set; }

        public int WorkingPumpCount { get; set; }

        public int StandbyPumpCount { get; set; }

        public bool IsDeleted { get; set; }
    }

    public enum WDSType
    {
        WDS
    }
}