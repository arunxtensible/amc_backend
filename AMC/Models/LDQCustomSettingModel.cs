﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class LDQCustomSettingModel
    {
        [DisplayName("Property Name")]
        public string LDQPropertyName { get; set; }
        [DisplayName("LDQ Value (In MLD)")]
        public decimal LDQValues { get; set; }
    }
}