﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.Models
{
    public class WTPGroundWaterDataModel
    {
        public string LocationName { get; set; }
        public decimal RPT_INLET_FLOW_CRNT_TOTAL { get; set; }
        public decimal RPT_OUTLET_FLOW_CRNT_TOTAL { get; set; }
        public decimal last_day_qty { get; set; }
    }
}