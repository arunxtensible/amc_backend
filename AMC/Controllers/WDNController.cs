﻿using AMC.IServices;
using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;

namespace AMC.Controllers
{
    public class WDNController : Controller
    {
        IWdn _iWDN;
        IZone _iZone;
        IWard _iWard;
        IWTPInletOutlet _iWTPInOut;
        IWDNInletOutlet _iWDNInOut;

        public WDNController(IWdn iwdn, IZone izone, IWard iward, IWTPInletOutlet iWTPInOut, IWDNInletOutlet iWDNInOut)
        {
            _iWDN = iwdn;
            _iZone = izone;
            _iWard = iward;
            _iWTPInOut = iWTPInOut;
            _iWDNInOut = iWDNInOut;
        }

        public ActionResult Index()
        {
            List<WDNModel> _WdnList = _iWDN.GetAllWDN();
            return View(_WdnList);
        }

        public ActionResult Zone()
        {           
            List<WDNZoneListModel> _lst = _iWDN.GetAllZones();
            return View(_lst);
        }

        public ActionResult Ward(int Id)
        {
            List<WDNWardListModel> _lst = _iWDN.GetAllWards(Id);
            ViewBag.ZoneId = Id;
            return View(_lst);
        }

        public ActionResult Location(int Id)
        {
            List<WDNLocationListModel> _lst = _iWDN.GetAllLocations(Id);
            ViewBag.WardId = Id;
            return View(_lst);
        }

        public ActionResult WDNZoneGraphData()
        {
            decimal CDQ_Sum = 0;
            decimal LDQ_Sum = 0;
            List<WDNInletOutletModel> WDNInlets = _iWDNInOut.GetWDNZoneGraphData();
            if (WDNInlets.ToList().Count > 0)
            {
                foreach (var item in WDNInlets.ToList())
                {
                    CDQ_Sum = CDQ_Sum + item.CDQ;
                    LDQ_Sum = LDQ_Sum + item.LDQ;
                    item.CDQ_Sum = item.CDQ_Sum + CDQ_Sum;
                    item.LDQ_Sum = item.LDQ_Sum + LDQ_Sum;

                }
            }

            ///New table logic
           // List<WDNInletOutletModel> WDNInlets = _iWDNInOut.GetWDNZoneGraphData();

            return Json(new
            {
                list = WDNInlets
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult WDNWardGraphData(int Id)
        {
            decimal CDQ_Sum = 0;
            decimal LDQ_Sum = 0;
            List<WDNInletOutletModel> WDNInlets = _iWDNInOut.GetWDNWardGraphData(Id);
            if (WDNInlets.ToList().Count > 0)
            {
                foreach (var item in WDNInlets.ToList())
                {
                    CDQ_Sum = CDQ_Sum + item.CDQ;
                    LDQ_Sum = LDQ_Sum + item.LDQ;
                    item.CDQ_Sum = item.CDQ_Sum + CDQ_Sum;
                    item.LDQ_Sum = item.LDQ_Sum + LDQ_Sum;

                }
            }
            ///New table logic
            //List<WDNInletOutletModel> WDNInlets = _iWDNInOut.GetWDNWardGraphData(Id);
            return Json(new
            {
                list = WDNInlets
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult WDNLocationGraphData(int Id)
        {
            List<WDNInletOutletModel> WDNInlets = new List<WDNInletOutletModel>();
            try
            {
                WDNInlets = _iWDNInOut.GetWDNLocationGraphData(Id);
            }
            catch (Exception ex)
            {
            }
                    
            return Json(new
            {
                list = WDNInlets
            }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult ZoneListForDropDown()
        {
            List<SelectListItem> _zone_list = new List<SelectListItem>();

            List<ZoneModel> _zone_list_m = _iZone.GetAllZones();
            _iWDN.GetAllZones();

            for (int i = 0; i < _zone_list_m.Count; i++)
            {
                _zone_list.Add(new SelectListItem
                {
                    Value = _zone_list_m[i].ZoneID.ToString(),
                    Text = _zone_list_m[i].ZoneName.ToString()
                });
            }

            return Json(_zone_list);
        }


        [HttpPost]
        public JsonResult WardListForDropDown(int Id)
        {
            List<SelectListItem> _zone_list = new List<SelectListItem>();

            List<WardModel> _zone_list_m = _iWard.GetAllWards(Id);

            for (int i = 0; i < _zone_list_m.Count; i++)
            {
                _zone_list.Add(new SelectListItem
                {
                    Value = _zone_list_m[i].WardID.ToString(),
                    Text = _zone_list_m[i].WardName.ToString()
                });
            }

            return Json(_zone_list);
        }

        [HttpGet]
        public ActionResult GetZoneByWardId(int id)
        {
            int _zone = _iWard.GetZoneIdByWardId(id);
            return RedirectToAction("Ward", new { id = _zone });
          
        }

        public ActionResult GetWDNInletDetailsByWDNName(Int32 wdn_id, string TagName)
        {

            List<WDNInletOutletDetailModel> WDNInlets = _iWDNInOut.WDNInletOutletDetailByWDNName(TagName);
            if (WDNInlets.ToList().Count > 0)
            {
                foreach (var item in WDNInlets)
                {
                    TempData["Search"] = item.ParentWDS;
                }
            }
            return Json(new
            {
                list = WDNInlets
            }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult Add(WDNModel model)
        {
            var res = _iWDN.Create(model);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Update(WDNModel model)
        {
            var res = _iWDN.Update(model);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            var res = _iWDN.Delete(id);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
    }
}