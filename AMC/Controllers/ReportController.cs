﻿using AMC.IServices;
using AMC.Models;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AMC.Controllers
{
    public class ReportController : Controller
    {
        IReport _IReport;
        IData _iData;


        public ReportController(IReport IReport, IData iData)
        {
            _IReport = IReport;
            _iData = iData;
        }

        // GET: Setting
        public ActionResult GetHourlyReport()
        {
            List<ReportModel> model = new List<ReportModel>();
            ReportModel obj = new ReportModel();
            obj.ReportZoneList = new SelectList(_iData.GetWDNZones(), "ZoneId", "ZoneName");
            model.Add(obj);
            return View(model);
        }

        public ActionResult GetMonthlyReport()
        {
            List<ReportModel> model = new List<ReportModel>();
            ReportModel obj = new ReportModel();
            obj.ReportZoneList = new SelectList(_iData.GetWDNZones(), "ZoneId", "ZoneName");
            model.Add(obj);
            return View(model);
        }

        [HttpGet]
        public ActionResult GetWDNReport(string Zone, string Ward, string Location, string ReportType, string Datetime)
        {
            if (ReportType == "Hourly")
            {
                //List<ReportDataModelHourly> reportList = _IReport.GetWDNReportByType(Zone, Ward, Location, ReportType, Datetime);
                //return Json(reportList, JsonRequestBehavior.AllowGet);
                List<ReportDataModelDB2DBDataHourly> reportList = _IReport.GetWDNReportByDb2DBReader(Location,  Datetime);
                return Json(reportList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<ReportDataModelMonthly> reportList = _IReport.GetWDNReportByTypeMonthly(Zone, Ward, Location, ReportType, Datetime);
                if (reportList.Any())
                {
                    int nonZeroLdqCount = Convert.ToInt32(reportList.Where(w => w.LDQ > 0).ToList().Count);
                    decimal sumOfLdq = Convert.ToDecimal(reportList.Sum(s=>s.LDQ));
                    decimal LDQAvg = sumOfLdq / (Convert.ToDecimal(nonZeroLdqCount));
                    ViewBag.LDQSum = sumOfLdq;
                    ViewBag.LDQAverage = LDQAvg;

                    int nonZeroPressureCount = Convert.ToInt32(reportList.Where(w => w.PeakPressure > 0).ToList().Count);
                    decimal sumOfPressure = Convert.ToDecimal(reportList.Sum(s => s.LDQ));
                    decimal PressureAvg = sumOfPressure / (Convert.ToDecimal(nonZeroPressureCount));
                    ViewBag.PressureSum = sumOfPressure;
                    ViewBag.PressureAverage = PressureAvg;
                    ReportDataModelMonthly obj = new ReportDataModelMonthly();
                    obj.DateAndTime = "Total";
                    obj.LDQ = sumOfLdq;
                    obj.PeakPressure = sumOfPressure;
                    reportList.Add(obj);
                    ReportDataModelMonthly obj1 = new ReportDataModelMonthly();
                    obj1.DateAndTime = "Average";
                    obj1.LDQ = LDQAvg;
                    obj1.PeakPressure = PressureAvg;
                    reportList.Add(obj1);
                }
                return Json(reportList, JsonRequestBehavior.AllowGet);
            }
           

        }


        public FileContentResult DownloadHourlyExcel(string Zone, string Ward, string Location, string ReportType, string DateAndTime)
        {

            var fileDownloadName = String.Format(Location+"_HourlyReport.xlsx").Replace(" ","");
            const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            // Pass your ef data to method
            // List<ReportDataModel> reportList = _IReport.GetWDNReportByType("Central", "Dariyapur", "BatataMill", "", DateTime.Now.ToString());

            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");
            ws.Cells["A1"].Value = "CLIENT";
            ws.Cells["B1"].Value = "Smart City Ahmedabad Development Limited (SCADAL)";
            ws.Cells["A2"].Value = "AGENCY";
            ws.Cells["B2"].Value = "Chetas Control Systems Pvt Ltd Pune";
            ws.Cells["A3"].Value = "Report Type";
            ws.Cells["B3"].Value = "Hourly Report";
            ws.Cells["A4"].Value = "Date Of Report";
            ws.Cells["B4"].Value = DateAndTime;

            ws.Cells[1, 2, 1, 7].Merge = true;
            ws.Cells[2, 2, 2, 7].Merge = true;
            ws.Cells[3, 2, 3, 7].Merge = true;
            ws.Cells[4, 2, 4, 7].Merge = true;


            ws.Cells["A5"].Value = "Zone";
            ws.Cells["B5"].Value = Zone;
            ws.Cells["C5"].Value = "Ward";
            ws.Cells["D5"].Value = Ward;
            ws.Cells["E5"].Value = "Location";
            ws.Cells["F5"].Value = Location;


            ws.Cells["A6"].Value = "DateTime (HH:MM:SS)";
            ws.Cells["B6"].Value = "Flow Rate (m3/Hr)";
            ws.Cells["C6"].Value = "Totaliser (m3)";
            ws.Cells["D6"].Value = "Pressure (PSI)";
            ws.Cells["E6"].Value = "CDQ (m3)";
            ws.Cells["F6"].Value = "LDQ (m3)";
            ws.Cells["G6"].Value = "Battery voltage (Vol)";
            // List<ReportDataModelHourly> reportList = _IReport.GetWDNReportByType(Zone,Ward,Location,ReportType, DateAndTime);
            List<ReportDataModelDB2DBDataHourly> reportList = _IReport.GetWDNReportByDb2DBReader(Location, DateAndTime);
            int rowStart = 7;
            //if (reportList.Any())
            //{
            //    foreach (var item in reportList)
            //    {


            //        ws.Cells[string.Format("A{0}", rowStart)].Value = item.DateAndTime;
            //        ws.Cells[string.Format("B{0}", rowStart)].Value = item.FlowRate;
            //        ws.Cells[string.Format("C{0}", rowStart)].Value = item.Totaliser;
            //        ws.Cells[string.Format("D{0}", rowStart)].Value = item.Pressure;
            //        ws.Cells[string.Format("E{0}", rowStart)].Value = item.CDQ;
            //        ws.Cells[string.Format("F{0}", rowStart)].Value = item.LDQ;
            //        ws.Cells[string.Format("G{0}", rowStart)].Value = item.BatteryVoltage;
            //        rowStart++;
            //    }
            //}
            if (reportList.Any())
            {
                foreach (var item in reportList)
                {


                    ws.Cells[string.Format("A{0}", rowStart)].Value = item.Timestamp.ToString("dd/MM/yyyy HH:mm:ss");
                    ws.Cells[string.Format("B{0}", rowStart)].Value = Math.Round(item.Flow,2);
                    ws.Cells[string.Format("C{0}", rowStart)].Value = Math.Round(item.Totaliser,2);
                    ws.Cells[string.Format("D{0}", rowStart)].Value = Math.Round(item.Pressure,2);
                    ws.Cells[string.Format("E{0}", rowStart)].Value = Math.Round(item.CDQ,2);
                    ws.Cells[string.Format("F{0}", rowStart)].Value = Math.Round(item.LDQ,2);
                    ws.Cells[string.Format("G{0}", rowStart)].Value = Math.Round(item.BatteryVoltage,2);
                    rowStart++;
                }
            }
            ws.Cells["A:AZ"].AutoFitColumns();
            var fsr = new FileContentResult(pck.GetAsByteArray(), contentType);
            fsr.FileDownloadName = fileDownloadName;

            return fsr;


        }

        public FileContentResult DownloadMonthlyExcel(string Zone, string Ward, string Location, string ReportType, string DateAndTime)
        {

            var fileDownloadName = String.Format(Location + "_MOnthlyReport.xlsx").Replace(" ", "");
            const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            // Pass your ef data to method
            // List<ReportDataModel> reportList = _IReport.GetWDNReportByType("Central", "Dariyapur", "BatataMill", "", DateTime.Now.ToString());

            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");
            ws.Cells["A1"].Value = "CLIENT";
            ws.Cells["B1"].Value = "Smart City Ahmedabad Development Limited (SCADAL)";
            ws.Cells["A2"].Value = "AGENCY";
            ws.Cells["B2"].Value = "Chetas Control Systems Pvt Ltd Pune";
            ws.Cells["A3"].Value = "Report Type";
            ws.Cells["B3"].Value = "Hourly Report";
            ws.Cells["A4"].Value = "Date Of Report";
            ws.Cells["B4"].Value = "From Date :"+ Convert.ToDateTime(DateAndTime).AddDays(-29).ToString("dd/MMM/yyyy" ) +"   To Date : "+ DateAndTime;

            ws.Cells[1, 2, 1, 6].Merge = true;
            ws.Cells[2, 2, 2, 6].Merge = true;
            ws.Cells[3, 2, 3, 6].Merge = true;
            ws.Cells[4, 2, 4, 6].Merge = true;


            ws.Cells["A5"].Value = "Zone";
            ws.Cells["B5"].Value = Zone;
            ws.Cells["C5"].Value = "Ward";
            ws.Cells["D5"].Value = Ward;
            ws.Cells["E5"].Value = "Location";
            ws.Cells["F5"].Value = Location;


            ws.Cells["A6"].Value = "DateTime (HH:MM:SS)";
            ws.Cells["C6"].Value = "LDQ (m3)";
            ws.Cells["E6"].Value = "Peak Pressure (PSI)";
            ws.Cells[6, 1, 6, 2].Merge = true;
            ws.Cells[6, 3, 6, 4].Merge = true;
            ws.Cells[6, 5, 6, 6].Merge = true;

            List<ReportDataModelMonthly> reportList = _IReport.GetWDNReportByTypeMonthly(Zone, Ward, Location, ReportType, DateAndTime);
            int rowStart = 7;
            if (reportList.Any())
            {
                
                    int nonZeroLdqCount = Convert.ToInt32(reportList.Where(w => w.LDQ > 0).ToList().Count);
                    decimal sumOfLdq = Convert.ToDecimal(reportList.Sum(s => s.LDQ));
                    decimal LDQAvg = sumOfLdq / (Convert.ToDecimal(nonZeroLdqCount));
                    ViewBag.LDQSum = sumOfLdq;
                    ViewBag.LDQAverage = LDQAvg;

                    int nonZeroPressureCount = Convert.ToInt32(reportList.Where(w => w.PeakPressure > 0).ToList().Count);
                    decimal sumOfPressure = Convert.ToDecimal(reportList.Sum(s => s.LDQ));
                    decimal PressureAvg = sumOfPressure / (Convert.ToDecimal(nonZeroPressureCount));
                    ViewBag.PressureSum = sumOfPressure;
                    ViewBag.PressureAverage = PressureAvg;
                    ReportDataModelMonthly obj = new ReportDataModelMonthly();
                    obj.DateAndTime = "Total";
                    obj.LDQ = sumOfLdq;
                    obj.PeakPressure = sumOfPressure;
                    reportList.Add(obj);
                    ReportDataModelMonthly obj1 = new ReportDataModelMonthly();
                    obj1.DateAndTime = "Average";
                    obj1.LDQ = LDQAvg;
                    obj1.PeakPressure = PressureAvg;
                    reportList.Add(obj1);
                
                foreach (var item in reportList)
                {


                    ws.Cells[string.Format("A{0}", rowStart)].Value = item.DateAndTime;
                    ws.Cells[string.Format("C{0}", rowStart)].Value = Math.Round(item.LDQ, 2);
                    ws.Cells[string.Format("E{0}", rowStart)].Value = Math.Round(item.PeakPressure, 2);
                    ws.Cells[rowStart, 1, rowStart, 2].Merge = true;
                    ws.Cells[rowStart, 3, rowStart, 4].Merge = true;
                    ws.Cells[rowStart, 5, rowStart, 6].Merge = true;
                    rowStart++;
                }
            }
            ws.Cells["A:AZ"].AutoFitColumns();
            var fsr = new FileContentResult(pck.GetAsByteArray(), contentType);
            fsr.FileDownloadName = fileDownloadName;

            return fsr;


        }


        public ActionResult GetConsolidatedWDNLdqReport()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetWDNLDqReport(string Datetime)
        {
            
                List<WDNLdqReportDataModel> reportList = _IReport.GetWDNLdqReport(Datetime);
                return Json(reportList, JsonRequestBehavior.AllowGet);           


        }
        public FileContentResult DownloadWDNLdqExcel(string DateAndTime)
        {

            var fileDownloadName = String.Format( "WDNConsolidatedLdqReport.xlsx").Replace(" ", "");
            const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            // Pass your ef data to method
            // List<ReportDataModel> reportList = _IReport.GetWDNReportByType("Central", "Dariyapur", "BatataMill", "", DateTime.Now.ToString());

            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");
            ws.Cells["A1"].Value = "CLIENT";
            ws.Cells["B1"].Value = "Smart City Ahmedabad Development Limited (SCADAL)";
            ws.Cells["A2"].Value = "AGENCY";
            ws.Cells["B2"].Value = "Chetas Control Systems Pvt Ltd Pune";
            ws.Cells["A3"].Value = "Report Type";
            ws.Cells["B3"].Value = "All WDN Consolidated LDQ Data Report";
            ws.Cells["A4"].Value = "Date Of Report";
            ws.Cells["B4"].Value = DateAndTime;

            ws.Cells[1, 2, 1, 7].Merge = true;
            ws.Cells[2, 2, 2, 7].Merge = true;
            ws.Cells[3, 2, 3, 7].Merge = true;
            ws.Cells[4, 2, 4, 7].Merge = true;




            ws.Cells["A6"].Value = "LocationName";
            ws.Cells["B6"].Value = "Unit";
            ws.Cells["C6"].Value = "Zone";
            ws.Cells["D6"].Value = "Ward";
            ws.Cells["E6"].Value = "LDQ";
            
            List < WDNLdqReportDataModel > reportList = _IReport.GetWDNLdqReport(DateAndTime);
            int rowStart = 7;
            //if (reportList.Any())
            //{
            //    foreach (var item in reportList)
            //    {


            //        ws.Cells[string.Format("A{0}", rowStart)].Value = item.DateAndTime;
            //        ws.Cells[string.Format("B{0}", rowStart)].Value = item.FlowRate;
            //        ws.Cells[string.Format("C{0}", rowStart)].Value = item.Totaliser;
            //        ws.Cells[string.Format("D{0}", rowStart)].Value = item.Pressure;
            //        ws.Cells[string.Format("E{0}", rowStart)].Value = item.CDQ;
            //        ws.Cells[string.Format("F{0}", rowStart)].Value = item.LDQ;
            //        ws.Cells[string.Format("G{0}", rowStart)].Value = item.BatteryVoltage;
            //        rowStart++;
            //    }
            //}
            if (reportList.Any())
            {
                foreach (var item in reportList)
                {


                    ws.Cells[string.Format("A{0}", rowStart)].Value = item.LocationName;
                    ws.Cells[string.Format("B{0}", rowStart)].Value = item.Unit;
                    ws.Cells[string.Format("C{0}", rowStart)].Value = item.Zone;
                    ws.Cells[string.Format("D{0}", rowStart)].Value = item.Ward;
                    ws.Cells[string.Format("E{0}", rowStart)].Value = item.LDQ;
                    rowStart++;
                }
            }
            ws.Cells["A:AZ"].AutoFitColumns();
            var fsr = new FileContentResult(pck.GetAsByteArray(), contentType);
            fsr.FileDownloadName = fileDownloadName;

            return fsr;


        }


        public ActionResult GetConsolidatedWDSLdqReport1()
        {
            return View();
        }

        public ActionResult GetConsolidatedWDSLdqReport()
        {
            List<WDSReportModel> model = new List<WDSReportModel>();
            WDSReportModel obj = new WDSReportModel();
            obj.ReportZoneList = new SelectList(_iData.GetZones(), "ZoneId", "ZoneName");
            model.Add(obj);
            return View(model);
        }
        [HttpGet]
        public ActionResult GetZonewiseWDSLDqReport(string Zone,string Datetime)
        {

            List<ReportDataModelWDSDetailsWithParentWTP> reportList = _IReport.GetZonewiseWDSLDqReport(Zone, Datetime);
            return Json(reportList, JsonRequestBehavior.AllowGet);


        }



        [HttpGet]
        public ActionResult GetWDSLDqReport(string Datetime)
        {

            List<WDSLdqReportDataModel> reportList = _IReport.GetWDSLdqReport(Datetime);
            return Json(reportList, JsonRequestBehavior.AllowGet);


        }

        public FileContentResult DownloadWDSLdqExcel(string DateAndTime)
        {

            var fileDownloadName = String.Format("WDSConsolidatedLdqReport.xlsx").Replace(" ", "");
            const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            // Pass your ef data to method
            // List<ReportDataModel> reportList = _IReport.GetWDNReportByType("Central", "Dariyapur", "BatataMill", "", DateTime.Now.ToString());

            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");
            ws.Cells["A1"].Value = "CLIENT";
            ws.Cells["B1"].Value = "Smart City Ahmedabad Development Limited (SCADAL)";
            ws.Cells["A2"].Value = "AGENCY";
            ws.Cells["B2"].Value = "Chetas Control Systems Pvt Ltd Pune";
            ws.Cells["A3"].Value = "Report Type";
            ws.Cells["B3"].Value = "All WDS Consolidated LDQ Data Report";
            ws.Cells["A4"].Value = "Date Of Report";
            ws.Cells["B4"].Value = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

            ws.Cells[1, 2, 1, 5].Merge = true;
            ws.Cells[2, 2, 2, 5].Merge = true;
            ws.Cells[3, 2, 3, 5].Merge = true;
            ws.Cells[4, 2, 4, 5].Merge = true;




            ws.Cells["A6"].Value = "Sr. No.";
            ws.Cells["B6"].Value = "LocationName";
            ws.Cells["C6"].Value = "Zone";
            ws.Cells["D6"].Value = "LDQ";

            List<WDSLdqReportDataModel> reportList = _IReport.GetWDSLdqReport(DateAndTime);
            int rowStart = 7;
            int i = 1;
            if (reportList.Any())
            {
                foreach (var item in reportList)
                {


                    ws.Cells[string.Format("A{0}", rowStart)].Value = i;
                    ws.Cells[string.Format("B{0}", rowStart)].Value = item.LocationName;
                    ws.Cells[string.Format("C{0}", rowStart)].Value = item.Zone;
                    ws.Cells[string.Format("D{0}", rowStart)].Value = item.LDQ;
                    rowStart++;
                    i++;
                }
            }
            ws.Cells["A:AZ"].AutoFitColumns();
            var fsr = new FileContentResult(pck.GetAsByteArray(), contentType);
            fsr.FileDownloadName = fileDownloadName;

            return fsr;


        }



        public ActionResult WDNReportWithParentWDS()
        {
            List<WDNWDSReportModel> model = new List<WDNWDSReportModel>();
            WDNWDSReportModel obj = new WDNWDSReportModel();
            obj.ReportZoneList = new SelectList(_iData.GetReportZones(), "ZoneId", "ZoneName");
            obj.ReportParentWDSList = new SelectList(_iData.GetReportParentWDS(), "Id", "ParentWDS");
            obj.ReportWardList = new SelectList(_iData.GetReportWard(), "Id", "Ward");
            obj.ReportLocationList = new SelectList(_iData.GetReportWDNLocations(), "Id", "Location");
            model.Add(obj);
            return View(model);
        }


        [HttpGet]
        public ActionResult GetParentWDSByZone(string Zone)
        {

            List<ReportParentWDS> List = _iData.GetReportParentWDSByZone(Zone).ToList();
            return Json(List, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult GetWardByParentWDS(string ParentWDS)
        {

            List<ReportWard> List = _iData.GetReportWardByParentWDS(ParentWDS).ToList();
            return Json(List, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult GetWDNLocationsByWard(string parentWDS, string Ward)
        {

            List<ReportWDNLocation> List = _iData.GetReportWDNLocationsByWard(parentWDS,Ward).ToList();
            return Json(List, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult GetWDNReportWithParentWDSDetails(string Zone, string ParentWDS, string Ward, string Location, string Datetime)
        {
                List<ReportDataModelWDNDetailsWithParentWDS> reportList = _IReport.GetWDNReportWithParentWDSDetails(Zone,ParentWDS,Ward,Location, Datetime);
                return Json(reportList, JsonRequestBehavior.AllowGet);
           

        }


        public FileContentResult DownloadWDNReportWithParentWDSExcel(string Zone, string ParentWDS, string Ward, string Location, string Datetime)
        {

            var fileDownloadName = String.Format("WDN-WDSReport.xlsx").Replace(" ", "");
            const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");
            ws.Cells["A1"].Value = "CLIENT";
            ws.Cells["B1"].Value = "Smart City Ahmedabad Development Limited (SCADAL)";
            ws.Cells["A2"].Value = "AGENCY";
            ws.Cells["B2"].Value = "Chetas Control Systems Pvt Ltd Pune";
            ws.Cells["A3"].Value = "Report Type";
            ws.Cells["B3"].Value = "WDN-WDS Report";
            ws.Cells["A4"].Value = "Date Of Report";
            ws.Cells["B4"].Value = Datetime;

            ws.Cells[1, 2, 1, 5].Merge = true;
            ws.Cells[2, 2, 2, 5].Merge = true;
            ws.Cells[3, 2, 3, 5].Merge = true;
            ws.Cells[4, 2, 4, 5].Merge = true;




            ws.Cells["A6"].Value = "Sr. No.";
            ws.Cells["B6"].Value = "Zone";
            ws.Cells["C6"].Value = "Parent WDS";
            ws.Cells["D6"].Value = "Ward";
            ws.Cells["E6"].Value = "WDS Outlet Volume";
            ws.Cells["F6"].Value = "WDN Location";
            ws.Cells["G6"].Value = "Diameter (MTR)";
            ws.Cells["H6"].Value = "Totaliser (m3)";
            ws.Cells["I6"].Value = "Reverse Totaliser (m3)";
            ws.Cells["J6"].Value = "Flow (m3)";
            ws.Cells["K6"].Value = "CDQ (m3)";
            ws.Cells["L6"].Value = "LDQ (m3)";
            ws.Cells["M6"].Value = "Reverse CDQ (m3)";
            ws.Cells["N6"].Value = "Reverse LDQ (m3)";
            ws.Cells["O6"].Value = "Pressure (PSI)";
            ws.Cells["P6"].Value = "Voltage (VDC)";
            ws.Cells["Q6"].Value = "Power Status";
            ws.Cells["R6"].Value = "Door Status";
            

            List<ReportDataModelWDNDetailsWithParentWDS> reportList = _IReport.GetWDNReportWithParentWDSDetails(Zone,ParentWDS,Ward,Location,Datetime);
            int rowStart = 7;
            int i = 1;
            if (reportList.Any())
            {
                foreach (var item in reportList)
                {
                    ws.Cells[string.Format("A{0}", rowStart)].Value = i;
                    ws.Cells[string.Format("B{0}", rowStart)].Value = item.Zone;
                    ws.Cells[string.Format("C{0}", rowStart)].Value = item.ParentWDS;
                    ws.Cells[string.Format("D{0}", rowStart)].Value = item.Ward;
                    ws.Cells[string.Format("E{0}", rowStart)].Value = item.WDSOutletVolume;
                    ws.Cells[string.Format("F{0}", rowStart)].Value = item.LocationName;
                    ws.Cells[string.Format("G{0}", rowStart)].Value = item.Diameter;
                    ws.Cells[string.Format("H{0}", rowStart)].Value = item.Totaliser;
                    ws.Cells[string.Format("I{0}", rowStart)].Value = item.ReverseTotaliser;
                    ws.Cells[string.Format("J{0}", rowStart)].Value = item.Flow;
                    ws.Cells[string.Format("K{0}", rowStart)].Value = item.CDQ;
                    ws.Cells[string.Format("L{0}", rowStart)].Value = item.LDQ;
                    ws.Cells[string.Format("M{0}", rowStart)].Value = item.ReverseCDQ;
                    ws.Cells[string.Format("N{0}", rowStart)].Value = item.ReverseLDQ;
                    ws.Cells[string.Format("O{0}", rowStart)].Value = item.Pressure;
                    ws.Cells[string.Format("P{0}", rowStart)].Value = item.BatteryVoltage;
                    ws.Cells[string.Format("Q{0}", rowStart)].Value = item.PowerStatus;
                    ws.Cells[string.Format("R{0}", rowStart)].Value = item.DoorStatus;
                    rowStart++;
                    i++;
                }
            }
            ws.Cells["A:AZ"].AutoFitColumns();
            var fsr = new FileContentResult(pck.GetAsByteArray(), contentType);
            fsr.FileDownloadName = fileDownloadName;

            return fsr;


        }


        public FileContentResult DownloadZonewiseWDSReportExcel(string Zone, string Datetime)
        {

            var fileDownloadName = String.Format("ConsolidatedWDSReport.xlsx").Replace(" ", "");
            const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;


            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Report");
            ws.Cells["A1"].Value = "CLIENT";
            ws.Cells["B1"].Value = "Smart City Ahmedabad Development Limited (SCADAL)";
            ws.Cells["A2"].Value = "AGENCY";
            ws.Cells["B2"].Value = "Chetas Control Systems Pvt Ltd Pune";
            ws.Cells["A3"].Value = "Report Type";
            ws.Cells["B3"].Value = "Consolidated WDS Report";
            ws.Cells["A4"].Value = "Date Of Report";
            ws.Cells["B4"].Value = Datetime;

            ws.Cells[1, 2, 1, 5].Merge = true;
            ws.Cells[2, 2, 2, 5].Merge = true;
            ws.Cells[3, 2, 3, 5].Merge = true;
            ws.Cells[4, 2, 4, 5].Merge = true;




            ws.Cells["A6"].Value = "Sr. No.";
            ws.Cells["B6"].Value = "Zone";
            ws.Cells["C6"].Value = "Parent WTP";
            ws.Cells["D6"].Value = "WDS Name";
            ws.Cells["E6"].Value = "Diameter";
            ws.Cells["F6"].Value = "InletFlow (m3/Hr)";
            ws.Cells["G6"].Value = "OutletFlow (m3/Hr)";
            ws.Cells["H6"].Value = "Pressure (PSI)";
            ws.Cells["I6"].Value = "PeakLevel (MTR)";
            ws.Cells["J6"].Value = "PeakPressure (PSI)";
            ws.Cells["K6"].Value = "TotalPumpingHour (HH:MM)";
            ws.Cells["L6"].Value = "Unit Consumption (KWH)";
            ws.Cells["M6"].Value = "LDQInlet (m3)";
            ws.Cells["N6"].Value = "LDQOutlet (m3)";

            decimal LDQInletTotal = 0;
            decimal LDQOutletTotal = 0;
            List<ReportDataModelWDSDetailsWithParentWTP> reportList = _IReport.GetZonewiseWDSLDqReport(Zone, Datetime);
            int rowStart = 7;
            int i = 1;
            if (reportList.Any())
            {
                foreach (var item in reportList)
                {
                    ws.Cells[string.Format("A{0}", rowStart)].Value = i;
                    ws.Cells[string.Format("B{0}", rowStart)].Value = item.Zone;
                    ws.Cells[string.Format("C{0}", rowStart)].Value = item.ParentWTP;
                    ws.Cells[string.Format("D{0}", rowStart)].Value = item.LocationName;
                    ws.Cells[string.Format("E{0}", rowStart)].Value = item.Diameter;
                    ws.Cells[string.Format("F{0}", rowStart)].Value = item.InletFlow;
                    ws.Cells[string.Format("G{0}", rowStart)].Value = item.OutletFlow;
                    ws.Cells[string.Format("H{0}", rowStart)].Value = item.Pressure;
                    ws.Cells[string.Format("I{0}", rowStart)].Value = item.PeakLevelMTR;
                    ws.Cells[string.Format("J{0}", rowStart)].Value = item.PeakPressure;
                    ws.Cells[string.Format("K{0}", rowStart)].Value = item.TotalPumpingHour;
                    ws.Cells[string.Format("L{0}", rowStart)].Value = item.KWH;
                    ws.Cells[string.Format("M{0}", rowStart)].Value = item.LDQInlet;
                    ws.Cells[string.Format("N{0}", rowStart)].Value = item.LDQOutlet;
                    rowStart++;
                    i++;
                    LDQInletTotal = LDQInletTotal + item.LDQInlet;
                    LDQOutletTotal = LDQOutletTotal + item.LDQOutlet;
                }
                ws.Cells[string.Format("B{0}", rowStart)].Value = "Total";
              
                ws.Cells[string.Format("M{0}", rowStart)].Value = LDQInletTotal;
                ws.Cells[string.Format("N{0}", rowStart)].Value = LDQOutletTotal;
                ws.Cells[rowStart, 2, rowStart, 12].Merge = true;
            }
            ws.Cells["A:AZ"].AutoFitColumns();
            var fsr = new FileContentResult(pck.GetAsByteArray(), contentType);
            fsr.FileDownloadName = fileDownloadName;

            return fsr;


        }

    }


}