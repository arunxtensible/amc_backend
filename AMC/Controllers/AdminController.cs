﻿using AMC.IServices;
using AMC.Models;
using AMC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AMC.Controllers
{
    public class AdminController : ApiController
    {
       private IDevice _iDevice;
        private IWard _iWard;
        private IWdn _iWdn;
        private IWds _iWds;
        private IWtp _iWtp;
        private IZone _iZone;
        public AdminController() : this(new DeviceRepository(), new WardRepository(), new WDNRepository(), new WDSRepository(), new WTPRepository(), new Zone())
        {
           
        }
      
        public AdminController(IDevice idevice, IWard iWard, IWdn iWdn, IWds iWds, IWtp iWtp, IZone iZone)
        {
            _iDevice = idevice;
            _iWard = iWard;
            _iWdn = iWdn;
            _iWds = iWds;
            _iWtp = iWtp;
            _iZone = iZone;
        }

        [HttpGet]
        public ResponseFormat GetDevices()
        {
            ResponseFormat response = new ResponseFormat();
            try
            {
                List<DeviceModel> _deviceList = _iDevice.GetAllDevice();
                response.StatusCode = "success";
                response.ResponseMessage = (_deviceList.Count == 0) ? "No data available" : string.Empty;
                response.Result = _deviceList;
            }
            catch (Exception ex)
            {
                response.StatusCode = "error";
                response.ResponseMessage = "Issue to retrive device data";
            }
            return response;
        }

        public ResponseFormat GetWards()
        {
            ResponseFormat response = new ResponseFormat();
            try
            {
                List<WardModel> _wardList = _iWard.GetAllWard();
                response.StatusCode = "success";
                response.ResponseMessage = (_wardList.Count == 0) ? "No data available" : string.Empty;
                response.Result = _wardList;
            }
            catch (Exception ex)
            {
                response.StatusCode = "error";
                response.ResponseMessage = "Issue to retrive device data";
            }
            return response;
        }

        public ResponseFormat GetWdns()
        {
            ResponseFormat response = new ResponseFormat();
            try
            {
                List<WDNModel> _wdnList = _iWdn.GetAllWDN();
                response.StatusCode = "success";
                response.ResponseMessage = (_wdnList.Count == 0) ? "No data available" : string.Empty;
                response.Result = _wdnList;
            }
            catch (Exception ex)
            {
                response.StatusCode = "error";
                response.ResponseMessage = "Issue to retrive device data";
            }
            return response;
        }

        public ResponseFormat GetWds()
        {
            ResponseFormat response = new ResponseFormat();
            try
            {
                List<WDSModel> _wnsList = _iWds.GetAllWDS();
                response.StatusCode = "success";
                response.ResponseMessage = (_wnsList.Count == 0) ? "No data available" : string.Empty;
                response.Result = _wnsList;
            }
            catch (Exception ex)
            {
                response.StatusCode = "error";
                response.ResponseMessage = "Issue to retrive device data";
            }
            return response;
        }

        public ResponseFormat GetWtps()
        {
            ResponseFormat response = new ResponseFormat();
            try
            {
                List<WTPModel> _wtpList = _iWtp.GetAllWTP();
                response.StatusCode = "success";
                response.ResponseMessage = (_wtpList.Count == 0) ? "No data available" : string.Empty;
                response.Result = _wtpList;
            }
            catch (Exception ex)
            {
                response.StatusCode = "error";
                response.ResponseMessage = "Issue to retrive device data";
            }
            return response;
        }

        public ResponseFormat GetZones()
        {
            ResponseFormat response = new ResponseFormat();
            try
            {
                List<ZoneModel> _zoneList = _iZone.GetAllZones();
                response.StatusCode = "success";
                response.ResponseMessage = (_zoneList.Count == 0) ? "No data available" : string.Empty;
                response.Result = _zoneList;
            }
            catch (Exception ex)
            {
                response.StatusCode = "error";
                response.ResponseMessage = "Issue to retrive device data";
            }
            return response;
        }

        public ResponseFormat GetDashboardAlertData()
        {
            ResponseFormat response = new ResponseFormat();
            try
            {
                List<DashboardAlertsModel> alertsModels = _iZone.GetDashboardAlertData();
                response.StatusCode = "success";
                response.ResponseMessage = (alertsModels.Count == 0) ? "No data available" : string.Empty;
                response.Result = alertsModels;
            }
            catch (Exception ex)
            {
                response.StatusCode = "error";
                response.ResponseMessage = "Issue to retrive device data";
            }
            return response;
        }

        public ResponseFormat WTPGroundWaterData()
        {
            ResponseFormat response = new ResponseFormat();
            try
            {
                List<WTPGroundWaterDataModel> wTPGrounds = _iZone.GetAllWTPGroundWater();
                response.StatusCode = "success";
                response.ResponseMessage = (wTPGrounds.Count == 0) ? "No data available" : string.Empty;
                response.Result = wTPGrounds;
            }
            catch (Exception ex)
            {
                response.StatusCode = "error";
                response.ResponseMessage = "Issue to retrive device data";
            }
            return response;
        }


    }
}