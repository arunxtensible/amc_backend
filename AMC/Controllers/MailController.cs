﻿using AMC.DBContext;
using AMC.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace AMC.Controllers
{
    public class MailController : Controller
    {
        // GET: Mail
        public ActionResult Index()
        {
            return View();
        }
        AMCDBContext _db = new AMCDBContext();
        public JsonResult SendAlertMail()
        {
            List<AlertNameClass> _name = new List<AlertNameClass>();
            _name.Add(new AlertNameClass { AlertName = "Tamper" });
            _name.Add(new AlertNameClass { AlertName = "Pressure" });
            _name.Add(new AlertNameClass { AlertName = "MainSupplyStatus" });
            _name.Add(new AlertNameClass { AlertName = "pump_run" });
            _name.Add(new AlertNameClass { AlertName = "communication" });
            _name.Add(new AlertNameClass { AlertName = "deviation" });
            _name.Add(new AlertNameClass { AlertName = "zeroflow" });
            _name.Add(new AlertNameClass { AlertName = "water_qaulity" });

            string from = WebConfigurationManager.AppSettings["EmailId"];
            string PWD = WebConfigurationManager.AppSettings["EmailPWD"];
            List<AlertZoneModel> alerts = new List<AlertZoneModel>();
            try
            {
                using (_db = new AMCDBContext())
                {
                    try
                    {
                        foreach (var _alerts in _name)
                        {                            
                            alerts = _db.Database.SqlQuery<AlertZoneModel>("GetTodaysAlertZoneAlertName @AlertName={0}", _alerts.AlertName).ToList();
                            if (alerts.Any())
                            {
                                foreach (var item in alerts)
                                {
                                    try
                                    {
                                        SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);

                                        smtpClient.UseDefaultCredentials = false;
                                        smtpClient.Credentials = new System.Net.NetworkCredential(from, PWD);
                                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                                        smtpClient.EnableSsl = true;
                                        MailMessage mail = new MailMessage();
                                        //Setting From , To and CC
                                        mail.From = new MailAddress(from, "AMC Water Plant");
                                        mail.IsBodyHtml = true;
                                        mail.Subject = item.Subject;
                                        string MailTo = item.MailTo;
                                        foreach (var address in MailTo.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                                        {
                                            mail.To.Add(address);
                                        }
                                        string MailCC = item.MailCC;
                                        foreach (var address in MailCC.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                                        {
                                            mail.CC.Add(address);
                                        }
                                        string MailBCC = item.MailCC;
                                        foreach (var address in MailBCC.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries))
                                        {
                                            mail.Bcc.Add(address);
                                        }
                                        if (mail.To.Count > 0 || mail.CC.Count > 0 || mail.Bcc.Count > 0)
                                        {
                                            mail.Body = "Dear Sir/Madam, <p>Please find the attached alerts/report.</p>";
                                            mail.Attachments.Add(GetAttachment(GetAlertData(_alerts.AlertName, item.ZoneName)));
                                            smtpClient.Send(mail);
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                            }
                            alerts.Clear();
                        }
                    }
                    catch (Exception ex1)
                    {
                    }
                   
                }


            }
            catch (Exception ex)
            {
            }
            return null;
        }



        public Attachment GetAttachment(DataTable dataTable)
        {
            MemoryStream outputStream = new MemoryStream();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage package = new ExcelPackage(outputStream))
            {
                ExcelWorksheet facilityWorksheet = package.Workbook.Worksheets.Add("sheet1");
                facilityWorksheet.Cells.LoadFromDataTable(dataTable, true);

                package.Save();
            }

            outputStream.Position = 0;
            Attachment attachment = new Attachment(outputStream, "Alert.xlsx", "application/vnd.ms-excel");

            return attachment;
        }

        public DataTable GetAlertData(string alertName, string zone)
        {
            AMCDBContext _db = new AMCDBContext();
            List<MailAlertModel> list = new List<MailAlertModel>();
            using (_db = new AMCDBContext())
            {
                
                list = _db.Database.SqlQuery<MailAlertModel>("GetAlertsDetailsForMail @AlertName={0}, @Zone={1}", alertName, zone).ToList();
                
            }

            return ToDataTable(list);
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }
    }

    public class AlertNameClass
    {
        public string AlertName { set; get; }
    }
}