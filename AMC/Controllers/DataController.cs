﻿using AMC.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AMC.Controllers
{
    public class DataController : Controller
    {
        IData _iData;
        public DataController(IData iData)
        {
            _iData = iData;
        }
        // GET: Data
        [HttpGet]
        public ActionResult GetZoneList()
        {
            IEnumerable<ZoneMaster> Zones ;
            try
            {
                Zones= _iData.GetZones();
            }
            catch (Exception ex)
            {

                Zones=null;
            }
            
            return Json(Zones, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetWTPList()
        {
            IEnumerable<WTP_Master> WTP_list;
            try
            {
                WTP_list = _iData.GetWTPList();
            }
            catch (Exception ex)
            {

                WTP_list = null;
            }

            return Json(WTP_list, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetWDSList(int ZoneId)
        {
            IEnumerable<WDSMaster> WDS_list;
            try
            {
                WDS_list = _iData.GetWDSList(ZoneId);
            }
            catch (Exception ex)
            {

                WDS_list = null;
            }

            return Json(WDS_list, JsonRequestBehavior.AllowGet);
        }
    }
}