﻿using AMC.IServices;
using AMC.Models;
using AMC.Services;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web.Http;

namespace AMC.Controllers
{
    public class AccountController : ApiController
    {
       private IUser _user;
        public AccountController() : this(new UserRepository())
        {
           
        }
      
        public AccountController(IUser user)
        {
            _user = user;
        }

        [HttpPost]
        public object login(Login login)
        {

            //bool Authontication = false;
            //if (login.UserName == "abc" && login.Password == "abc")
            //{
            //    Authontication = true;
            //}


            bool Authontication = _user.AuthonticateUser(login);
            if (Authontication)
            {
                return Request.CreateResponse(HttpStatusCode.OK, GetToken(login.UserName));
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadGateway, message: "User name and password is invalid");
            }
        }

        [HttpGet]
        public HttpResponseMessage get()
        {

            return Request.CreateResponse(HttpStatusCode.OK);

        }

        public Object GetToken(string userName)
        {
            string key = "8Zz5tw0Ionm3XPZZfN0NOml3z9FMfmpgXwovR9fp6ryDIoGRM8EPHAB6iHsc90fb"; //Secret key which will be used later during validation    
            //var issuer = "http://mysite.com";  //normally this will be your site URL    
            var issuer = "https://amcwdnchetas.dyndns.org:8383";  //normally this will be your site URL    

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            permClaims.Add(new Claim("valid", "1"));
            //permClaims.Add(new Claim("userid", "1"));
            permClaims.Add(new Claim("name", userName));

            //Create Security Token object by giving required parameters    
            //var token = new JwtSecurityToken(issuer, //Issure    
            //                issuer,  //Audience  
            var token = new JwtSecurityToken(null, //Issure    
          null,  //Audience    
          permClaims,
          expires: DateTime.Now.AddDays(1),
          signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);
            return new { token = jwt_token };
        }
    }
}