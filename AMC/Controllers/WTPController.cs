﻿using AMC.IServices;
using AMC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace AMC.Controllers
{
    public class WTPController : Controller
    {
        IWTPInletOutlet _iWTPInOut;
        IWtp _iWTP;

        public WTPController(IWTPInletOutlet iWTPInOut, IWtp iWTP)
        {
            _iWTPInOut = iWTPInOut;
            _iWTP = iWTP;
        }


        public ActionResult Index()
        {
            List<WTPModel> _WtpList = _iWTP.GetAllWTP();
            return View(_WtpList);
        }
        // GET: WTP
        public ActionResult WTPInletDashboard()
        {
            List<WTPInletOutletModel> WTPInlets = _iWTPInOut.GetAllInlet("Inlet",null);
            decimal capacity = 0;
            decimal CDQ = 0;
            decimal LDQ = 0;
            decimal UnitConsumption = 0;
            string LDRDateTime = "";
            DateTime dt=DateTime.Now-new TimeSpan(1,0,0);
            if (WTPInlets.Count > 0)
            {     
                foreach (var item in WTPInlets)
                {
                    capacity = capacity + item.Capacity;
                    CDQ = CDQ + item.CDQ;
                    LDQ = LDQ + item.LDQ;
                    UnitConsumption = UnitConsumption + item.UnitConsumption;
                    if (item.LDRDateTime > dt)
                    {
                        LDRDateTime = item.LDRDateTime.ToString("dd/MM/yyyy hh:mm tt");
                    }
                    
                   
                }               
            }
            ViewBag.Capacity = Math.Round(Convert.ToDecimal(capacity), 2); 
            ViewBag.CDQ = Math.Round(Convert.ToDecimal(CDQ), 2); 
            ViewBag.LDQ = Math.Round(Convert.ToDecimal(LDQ), 2); 
            ViewBag.UnitConsumption = Math.Round(Convert.ToDecimal(UnitConsumption), 2); 
            ViewBag.LDRDateTIME = LDRDateTime;
           
            return View(WTPInlets);
        }
        public ActionResult WTPListForGraph()
        {

            List<WTPInletOutletModel> WTPInlets = _iWTPInOut.GetAllInlet("Inlet",null);
            return Json(new
            {
                list = WTPInlets
            }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult WTPListForGraphOutlet()
        {

            List<WTPInletOutletModel> WTPInlets = _iWTPInOut.GetAllInlet("Outlet",null);
            return Json(new
            {
                list = WTPInlets
            }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetWTPInletDetailsByWTPName(Int32 wtp_id, string Type)
        {

            List<WTPInletOutletDetailModel> WTPInlets = _iWTPInOut.WTPInletOutletDetailByWTPName(wtp_id, Type);
            return Json(new
            {
                list = WTPInlets
            }, JsonRequestBehavior.AllowGet);

        }


        public ActionResult WTPOutletDashboard()
        {
            string searchVal = null;
            if (TempData["Search"] != null)
            {
                searchVal = TempData["Search"].ToString();
                //TempData.Keep("Search");
            }
            List<WTPInletOutletModel> WTPInlets = new List<WTPInletOutletModel>();
            if(searchVal==null)
            WTPInlets = _iWTPInOut.GetAllInlet("Outlet", searchVal);
            else
                WTPInlets = _iWTPInOut.GetAllInlet("Outlet", searchVal);
            decimal capacity = 0;
            decimal CDQ = 0;
            decimal LDQ = 0;
            decimal UnitConsumption = 0;
            string LDRDateTime = "";
            DateTime dt = DateTime.Now - new TimeSpan(1, 0, 0);
            if (WTPInlets.Count > 0)
            {
                foreach (var item in WTPInlets)
                {
                    capacity = capacity + item.Capacity;
                    CDQ = CDQ + item.CDQ;
                    LDQ = LDQ + item.LDQ;
                    UnitConsumption = UnitConsumption + item.UnitConsumption;
                    //LDRDateTime = item.LDRDateTime.ToString("dd/MM/yyyy hh: mm tt");
                    if (item.LDRDateTime > dt)
                    {
                        LDRDateTime = item.LDRDateTime.ToString("dd/MM/yyyy hh: mm tt");
                        dt = item.LDRDateTime;
                    }
                }
            }
            ViewBag.Capacity = Math.Round(Convert.ToDecimal(capacity), 2);
            ViewBag.CDQ = Math.Round(Convert.ToDecimal(CDQ), 2);
            ViewBag.LDQ = Math.Round(Convert.ToDecimal(LDQ), 2);
            ViewBag.UnitConsumption = Math.Round(Convert.ToDecimal(UnitConsumption), 2);
            ViewBag.LDRDateTIME = LDRDateTime;
            return View(WTPInlets);
        }


        public ActionResult WTPGroundWater()
        {
            List<WTPGroundWaterModel> _WtpList = _iWTP.GetAllWTPGroundWater();
            return View(_WtpList);
        }

      


        public ActionResult Add(WTPModel model)
        {
            var res = _iWTP.Add(model);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Update(WTPModel model)
        {
            var res = _iWTP.Update(model);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            var res = _iWTP.Delete(id);
            return Json(res, JsonRequestBehavior.AllowGet);


        }
    }
}