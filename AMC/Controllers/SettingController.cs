﻿using AMC.IServices;
using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AMC.Controllers
{
    public class SettingController : Controller
    {
        ISetting _ISetting;
        public SettingController(ISetting ISetting)
        {
            _ISetting = ISetting;
        }

        // GET: Setting
        public ActionResult RunSynchronization()
        {
          
            return View();
        }

       [HttpPost]
        public JsonResult RunSynchronization(string startDate, string endDate, string Type)
        {
            bool IsSync = false;
            try
            {
                if (Type == "Synch WDN")
                {
                    IsSync = _ISetting.SyncWDNData(startDate, endDate);
                }
                else if (Type == "Synch Pump Status")
                {
                    IsSync = _ISetting.SyncPumpData(startDate, endDate);
                }
                else if (Type == "Synch Alarm Data")
                {
                    IsSync = _ISetting.SyncAlarmData(startDate, endDate);
                }
                else if (Type == "Synch WDS And WTP")
                {
                    IsSync = _ISetting.Sync_WDS_WTPData(startDate, endDate);
                }
            }
            catch (Exception ex)
            {
            }           
               
            return Json(new
            {
                list = IsSync
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LDQCustomSetting()
        {
            List<LDQCustomSettingModel> model = new List<LDQCustomSettingModel>();
            model = _ISetting.GetWTP_WDS_LDQValues();
            return View(model);
        }

        [HttpPost]
        public JsonResult ChangeLDQValue(string PropertyName, string value)
        {
            bool IsSync = false;
            try
            {
                    IsSync = _ISetting.SetCustomLDQValue(PropertyName, value);
               
            }
            catch (Exception ex)
            {
            }

            return Json(new
            {
                list = IsSync
            }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult WaterQualitySetting()
        {
            List<WaterQualitySettingModel> model = new List<WaterQualitySettingModel>();
            model = _ISetting.GetWaterQualityValues();
            return View(model);
        }

        [HttpPost]
        public JsonResult SetWaterQualityValue(string PropertyName, string Min, string Max)
        {
            bool IsSync = false;
            try
            {
                IsSync = _ISetting.SetWaterQualityValue(PropertyName, Min, Max);

            }
            catch (Exception ex)
            {
            }

            return Json(new
            {
                list = IsSync
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetWaterQualityAnalysisTime(string Time)
        {
            bool IsSync = false;
            try
            {
                IsSync = _ISetting.SetWaterQualityAnalysisTime(Time);

            }
            catch (Exception ex)
            {
            }

            return Json(new
            {
                list = IsSync
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AlertMailConfigurations()
        {
            List<AlertMailConfigurationModel> model = new List<AlertMailConfigurationModel>();
            model = _ISetting.GetAlertMailConfigValues();
            return View(model);
        }


        [HttpPost]
        public JsonResult SetAlertMailConfigValue(int Id, string EastMailIds, string WestMailIds, string SouthMailIds, string NorthMailIds, string EmailSubject, DateTime MailTime)
        {
            bool IsSync = false;
            try
            {
                IsSync = _ISetting.SetAlertMailConfigValue(Id, EastMailIds, WestMailIds, SouthMailIds, NorthMailIds, EmailSubject, MailTime);

            }
            catch (Exception ex)
            {
            }

            return Json(new
            {
                list = IsSync
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult WDNReportConfiguration()
        {
            List<WDNReportConfigurationModel> model = new List<WDNReportConfigurationModel>();
            model = _ISetting.GetWDNReportConfigValues("Daily");
            return View(model);
        }

        [HttpPost]
        public JsonResult SetWDNReportConfigValue(string Ids, string Values, string EmailSubject)//, string EastMailIds, string WestMailIds, string SouthMailIds, string NorthMailIds, string EmailSubject, DateTime MailTime)
        {
            bool IsSync = false;
            try
            {
                int i = 0;
                string[] vals = Values.Split('|');

                foreach (string id in Ids.Split('|'))
                {
                    if(i != 0 )
                    {
                        IsSync = _ISetting.SetWDNReportConfigValue(int.Parse(id), vals[i], EmailSubject);
                    }
                    i = i + 1;
                }                              

            }
            catch (Exception ex)
            {
            }

            return Json(new
            {
                list = IsSync
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AlertMailConfigurationSetting()
        {
            List<AlertMailConfigurationSettingModel> model = new List<AlertMailConfigurationSettingModel>();
            try
            {
                model = _ISetting.GetAlertMailConfigurationSetting("All", "All");

            }
            catch (Exception ex)
            {
            }
            return View(model);
        }

        public JsonResult GetAlertMailConfigurationDetails(string alertname, string zone)
        {
            List<AlertMailConfigurationSettingModel> model = new List<AlertMailConfigurationSettingModel>();
            try
            {
                model = _ISetting.GetAlertMailConfigurationSetting(alertname, zone);

            }
            catch (Exception ex)
            {
            }

            return Json(new
            {
                list = model
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetAlertMailConfigurationSetting(string alertname, string zone, string mailtime, string mailto, string mailcc, string mailbcc, string subject)
        {
            bool IsSync = false;
            try
            {
                IsSync = _ISetting.SetAlertMailConfigurationSetting(alertname, zone, mailtime, mailto, mailcc, mailbcc, subject);
                if (IsSync)
                {
                    Task.ScheduledTask.JobScheduler.Start();
                }

            }
            catch (Exception ex)
            {
            }

            return Json(new
            {
                list = IsSync
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RestartApplication()
        {
           
             Task.ScheduledTask.JobScheduler.Start();
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}