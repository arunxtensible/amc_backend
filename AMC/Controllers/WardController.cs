﻿using AMC.IServices;
using AMC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AMC.Controllers
{
    public class WardController : Controller
    {
        IWard _iWard;
        //Mapper map = null;
        public WardController(IWard iward)
        {
            _iWard = iward;
        }
        // GET: Zone

        public ActionResult Index()
        {
            //List<WardModel> _WardList = _iWard.GetAllWard();
            List<WardModel> _WardList = _iWard.GetAllWards();
            return View(_WardList);
        }
        public ActionResult Get()
        {
            List<WardModel> _WardList = _iWard.GetAllWard();
            var res = _WardList.Select(y => new DropDownModel
            {
                Name = y.WardName,
                Value = y.WardID
            });
            return Json(res, JsonRequestBehavior.AllowGet);
        }


    }
}