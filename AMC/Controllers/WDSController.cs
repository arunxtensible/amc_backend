﻿using AMC.IServices;
using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AMC.Controllers
{
    public class WDSController : Controller
    {
        IWds _iWDS;
        IZone _iZone;
        public WDSController(IWds iwds, IZone izone)
        {
            _iWDS = iwds;
            _iZone = izone;
        }

        public ActionResult Index()
        {
            List<WDSModel> _WdsList = _iWDS.GetAllWDS();
            return View(_WdsList);
        }

        /// <summary>
        /// WDS inlet
        /// </summary>
        /// <returns></returns>
        public ActionResult WDSInletDashboard()
        {
            List<WDSInletOutletModel> WTPInlets = _iWDS.GetAllZoneInletOutlet("Inlet");
           
            decimal CDQ = 0;
            decimal LDQ = 0;
            decimal UnitConsumption = 0;
            string LDRDateTime = "";
            DateTime dt = DateTime.Now - new TimeSpan(10, 10, 10);
            if (WTPInlets.Count > 0)
            {
                foreach (var item in WTPInlets)
                {
                    if (!item.ZoneName.Contains("Booster"))
                    {
                        CDQ = CDQ + item.ZoneCDQ;
                        LDQ = LDQ + item.ZoneLDQ;
                        UnitConsumption = UnitConsumption + item.ZoneUnitConsumption;
                    }
                    if (Convert.ToDateTime(item.LDRDateTime) > dt)
                    {
                       
                        dt = Convert.ToDateTime(item.LDRDateTime);
                    }


                }
            }
           
            ViewBag.CDQ = Math.Round(Convert.ToDecimal(CDQ), 2);
            ViewBag.LDQ = Math.Round(Convert.ToDecimal(LDQ), 2);
            ViewBag.UnitConsumption = Math.Round(Convert.ToDecimal(UnitConsumption), 2);
            ViewBag.LDRDateTIME = dt.ToString("dd/MM/yyyy hh:mm:ss tt");

            return View(WTPInlets);
        }

        /// <summary>
        /// WDS outlet
        /// </summary>
        /// <returns></returns>
        public ActionResult WDSOutletDashboard()
        {
            List<WDSInletOutletModel> WTPInlets = _iWDS.GetAllZoneInletOutlet("Outlet");

            decimal CDQ = 0;
            decimal LDQ = 0;
            decimal UnitConsumption = 0;
            string LDRDateTime = "";
            DateTime dt = DateTime.Now - new TimeSpan(10, 10, 10);
            if (WTPInlets.Count > 0)
            {
                foreach (var item in WTPInlets)
                {
                    if (!item.ZoneName.Contains("Booster"))
                    {
                        CDQ = CDQ + item.ZoneCDQ;
                        LDQ = LDQ + item.ZoneLDQ;
                        UnitConsumption = UnitConsumption + item.ZoneUnitConsumption;
                    }
                    if (Convert.ToDateTime(item.LDRDateTime) > dt)
                    {
                        dt = Convert.ToDateTime(item.LDRDateTime);
                    }


                }
            }

            ViewBag.CDQ = Math.Round(Convert.ToDecimal(CDQ), 2);
            ViewBag.LDQ = Math.Round(Convert.ToDecimal(LDQ), 2); 
            ViewBag.UnitConsumption = Math.Round(Convert.ToDecimal(UnitConsumption), 2); 
            ViewBag.LDRDateTIME = dt.ToString("dd/MM/yyyy hh:mm:ss tt");

            return View(WTPInlets);
        }

        /// <summary>
        /// Common for in-out by type
        /// </summary>
        /// <param name="Type"></param>
        /// <returns></returns>

        public ActionResult WDSListForGraph(string Type)
        {

            List<WDSInletOutletModel> WTPInlets = _iWDS.GetAllZoneInletOutlet(Type);
            return Json(new
            {
                list = WTPInlets
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult WDSInletLocations(int? id)
        {
            List<WDSInletOutletLocationModel> WTPInlets = _iWDS.GetWDSLocationsByZoneId(id,"Inlet");
            decimal Capacity = 0;
            decimal CDQ = 0;
            decimal LDQ = 0;
            decimal UnitConsumption = 0;
            string LDRDateTime = "";
            DateTime dt = DateTime.Now - new TimeSpan(10, 10, 10);
            if (WTPInlets.Count > 0)
            {
                foreach (var item in WTPInlets)
                {
                    Capacity = Capacity + item.Capacity;
                    CDQ = CDQ + item.CDQ;
                    LDQ = LDQ + item.LDQ;
                    UnitConsumption = UnitConsumption + item.UnitConsumption;
                    if (Convert.ToDateTime(item.LDR_DateTime) > dt)
                    {
                        //LDRDateTime = item.LDR_DateTime.ToString();
                        dt = Convert.ToDateTime(item.LDR_DateTime);
                    }
                    item.ZoneList.ToList();

                }
            }

            ViewBag.Capacity = Math.Round(Convert.ToDecimal(Capacity), 2);
            ViewBag.CDQ = Math.Round(Convert.ToDecimal(CDQ), 2);
            ViewBag.LDQ = Math.Round(Convert.ToDecimal(LDQ), 2); ;
            ViewBag.UnitConsumption = Math.Round(Convert.ToDecimal(UnitConsumption), 2); ;
            ViewBag.LDRDateTIME = dt.ToString("dd/MM/yyyy hh:mm:ss tt");

            ViewBag.ZoneId = id;

            return View(WTPInlets);
        }

        public ActionResult WDSOutletLocations(int? id)
        {
            string searchVal = null;
            if (TempData["Search"] != null)
            {
                searchVal = TempData["Search"].ToString();
                //TempData.Keep("Search");
            }

            List<WDSInletOutletLocationModel> WTPInlets = new List<WDSInletOutletLocationModel>();
            if (searchVal != null)
            {
                try
                {
                    WTPInlets = _iWDS.GetWDSLocationsByZoneIdSearchLocation(id, searchVal, "Outlet");
                }
                catch (Exception ex)
                {
                    WTPInlets = _iWDS.GetWDSLocationsByZoneId(id, "Outlet");
                }
                
            }
            else {
                WTPInlets = _iWDS.GetWDSLocationsByZoneId(id, "Outlet");
            }

            decimal Capacity = 0;
            decimal CDQ = 0;
            decimal LDQ = 0;
            decimal UnitConsumption = 0;
            string LDRDateTime = "";
            DateTime dt = DateTime.Now - new TimeSpan(10, 10, 10);
            if (WTPInlets.Count > 0)
            {
                foreach (var item in WTPInlets)
                {
                    Capacity = Capacity + item.Capacity;
                    CDQ = CDQ + item.CDQ;
                    LDQ = LDQ + item.LDQ;
                    UnitConsumption = UnitConsumption + item.UnitConsumption;
                    if (Convert.ToDateTime(item.LDR_DateTime) > dt)
                    {
                        dt = Convert.ToDateTime(item.LDR_DateTime);
                    }
                    item.ZoneList.ToList();

                }
            }
            ViewBag.Capacity = Math.Round(Convert.ToDecimal(Capacity), 2);
            ViewBag.CDQ = Math.Round(Convert.ToDecimal(CDQ), 2);
            ViewBag.LDQ = Math.Round(Convert.ToDecimal(LDQ), 2);
            ViewBag.UnitConsumption = Math.Round(Convert.ToDecimal(UnitConsumption), 2);
            ViewBag.LDRDateTIME = dt.ToString("dd/MM/yyyy hh:mm:ss tt");

            ViewBag.ZoneId = id;

            return View(WTPInlets);
        }
        //public ActionResult GetWDSLocationsByZoneId(int? id)
        //{
        //    List<WDSInletOutletLocationModel> WTPInlets = _iWDS.GetWDSLocationsByZoneId(id);

        //    decimal CDQ = 0;
        //    decimal LDQ = 0;
        //    string LDRDateTime = "";
        //    DateTime dt = DateTime.Now - new TimeSpan(10, 10, 10);
        //    if (WTPInlets.Count > 0)
        //    {
        //        foreach (var item in WTPInlets)
        //        {

        //            CDQ = CDQ + item.CDQ;
        //            LDQ = LDQ + item.LDQ;
        //            if (Convert.ToDateTime(item.LDR_DateTime) > dt)
        //            {
        //                LDRDateTime = item.LDR_DateTime.ToString();
        //                dt = Convert.ToDateTime(item.LDR_DateTime);
        //            }
        //            item.ZoneList.ToList();

        //        }
        //    }

        //    ViewBag.CDQ = Math.Round(Convert.ToDecimal(CDQ), 2);
        //    ViewBag.LDQ = Math.Round(Convert.ToDecimal(LDQ), 2); ;
        //     ViewBag.LDRDateTIME = LDRDateTime;

        //    ViewBag.ZoneId = id;

        //    return View(WTPInlets);
        //}

        public ActionResult GetWDSLocationsForGraphByZoneId(int ZoneId,string Type)
        {

            List<WDSInletOutletLocationModel> WTPInlets = _iWDS.GetWDSLocationsByZoneId(ZoneId, Type);
            return Json(new
            {
                list = WTPInlets
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetWDSDetailsByLocationName(string LocationName, string Type)
        {

            List<WDSInletOutletLocationDetailsModel> WTPInlets = _iWDS.WDSInletOutletDetailByLocationName(LocationName, Type);
            if (WTPInlets.ToList().Count > 0)
            {
                foreach (var item in WTPInlets)
                {
                    TempData["Search"] = item.IntakeSource;
                }
            }
            return Json(new
            {
                list = WTPInlets
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Add(WDSModel model)
        {
            var res = _iWDS.Create(model);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Update(WDSModel model)
        {
            var res = _iWDS.Update(model);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            var res = _iWDS.Delete(id);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetWDSTypes()
        {
            var res = Enum.GetValues(typeof(WDSType)).Cast<WDSType>().Select(y => new DropDownModel
            {
                Name = y.ToString(),
                Value = y.ToString()
            });
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Get()
        {
            List<WDSModel> _WdsList = _iWDS.GetAllWDS();
            var res = _WdsList.Select(y => new DropDownModel
            {
                Name = y.WDSName,
                Value = y.WDSId
            });
            return Json(res, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ZoneListForWDS()
        {
            List<SelectListItem> _zone_list = new List<SelectListItem>();

            List<ZoneModel> _zone_list_m = _iZone.GetAllZones();

            for (int i = 0; i < _zone_list_m.Count; i++)
            {
                _zone_list.Add(new SelectListItem
                {
                    Value = _zone_list_m[i].ZoneID.ToString(),
                    Text = _zone_list_m[i].ZoneName.ToString()
                });
            }

            return Json(_zone_list);
        }

        [HttpPost]
        public JsonResult SetWDSFillupTime(string Time,string WDSName)
        {
            bool IsSetTime = false;
            try
            {
                IsSetTime = _iWDS.SetWDSFillUpTime(Time, WDSName);
               
            }
            catch (Exception ex)
            {
            }

            return Json(new
            {
                list = IsSetTime
            }, JsonRequestBehavior.AllowGet);
        }
    }
}