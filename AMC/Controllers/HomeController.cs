﻿using AMC.Models;
using AMC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AMC.IServices;

namespace AMC.Controllers
{
    public class HomeController : Controller
    {
        IUser _user;

        public HomeController(IUser user)
        {

            _user = user;
        }
      

        [AllowAnonymous]
        public ActionResult Index()
        {
          
            return View();
        }

        [HttpPost]
        public ActionResult Index(Login login)
        {
           bool Authontication = _user.AuthonticateUser(login);
            if (Authontication)
            {
                FormsAuthentication.SetAuthCookie(login.UserName, true);                

                string DashBoardURL = Url.Action("Index", "Dashboard");
                return RedirectToAction("../Dashboard/Index");
            }
            else
            {
                ViewBag.msg = "Invalid credentials. Please try again.";

            }
            return View();
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
            return RedirectToAction("Index");
        }

        public ActionResult Download()
        {
            string file = @"~/assets/images/AMC_Android_Build.apk";
           
            return File(file, "application/vnd.android.package-archive", System.IO.Path.GetFileName(file));
        }
    }
}