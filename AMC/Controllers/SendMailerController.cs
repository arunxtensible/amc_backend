﻿using AMC.DBContext;
using AMC.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace AMC.Controllers
{
    public class SendMailerController : Controller
    {
        AMCDBContext _db = new AMCDBContext();
        [HttpGet]
        public ActionResult Index( )
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string from = "chetascontrolsystems@gmail.com"; //example:- sourabh9303@gmail.com  

                    using (MailMessage mail = new MailMessage(from, "arun@xtensible.in,vishal@xtensible.in,rnd10@chetascontrol.com"))
                    {
                        mail.IsBodyHtml = true;
                        mail.Subject = "AMC Test";// objModelMail.Subject;
                       
                        mail.Body = GetEmailBody();// objModelMail.Body;
                       /* if (fileUploader != null)
                        {
                            string fileName = Path.GetFileName(fileUploader.FileName);
                            mail.Attachments.Add(new Attachment(fileUploader.InputStream, fileName));
                        }*/

                        using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                        {
                            smtp.Credentials = new NetworkCredential("chetascontrolsystems@gmail.com", "ChetaServer$#@1989");
                            smtp.EnableSsl = true;
                            smtp.Send(mail);
                            ViewBag.Message = "Sent";
                        }
                    }
                }
                catch (Exception ex)
                {

                }

                return View("Index");
            }
            else
            {
                return View();
            }
        }
        public string GetEmailBody()
        {
            string body = null;
            List<DashboardAlertsModel> list = new List<DashboardAlertsModel>();
            using (_db = new AMCDBContext())
            {
                list = _db.Database.SqlQuery<DashboardAlertsModel>("GetAllDashboardAlertsData").ToList();
            }
            body = "<h1>AMC Alerts</h1><table>";
            body =body+ "<tr><th style=' border: 1px solid black;'>Alert Name</th><th style=' border: 1px solid black;text-align:center;'> Alert Count </th></tr>";
            if (list.ToList().Any())
            {
                foreach (var item in list)
                {
                    body = body + "<tr><td style=' border: 1px solid black;'>Tampring </td><td style=' border: 1px solid black;text-align:center;'>" + item.TamperingAlerts+ "</td></tr>";
                    body = body + "<tr><td style=' border: 1px solid black;'>Water Pressure</td><td style=' border: 1px solid black;text-align:center;'>" + item.WaterPressureAlerts + "</td></tr>";
                    body = body + "<tr><td style=' border: 1px solid black;'>Water Quality</td><td style=' border: 1px solid black;text-align:center;'>" + item.TamperingAlerts + "</td></tr>";
                    body = body + "<tr><td style=' border: 1px solid black;'>Power Cut</td><td style=' border: 1px solid black;text-align:center;'>" + item.PowerCutAlerts + "</td></tr>";
                    body = body + "<tr><td style=' border: 1px solid black;'>Communication</td><td style=' border: 1px solid black;text-align:center;'>" + item.CommunicationAlerts + "</td></tr>";
                    body = body + "<tr><td style=' border: 1px solid black;'>Flow Deviation Alerts</td><td style=' border: 1px solid black;text-align:center;'>" + item.FlowDeviiationAlerts + "</td></tr>";
                    body = body + "<tr><td style=' border: 1px solid black;'>Pumping Hours</td><td style=' border: 1px solid black;text-align:center;'>" + item.PumpingHourAlerts + "</td></tr>";
                    body = body + "<tr><td style=' border: 1px solid black;'>Zero Flow</td><td style=' border: 1px solid black;text-align:center;'>" + item.ZeroFlowAlerts + "</td></tr>";
                    body = body + "<tr><td style=' border: 1px solid black;'>Water VS Energy </td><td style=' border: 1px solid black;text-align:center;'>" + item.WaterVSEnergyAlerts + "</td></tr>";
                    body = body + "<tr><td style=' border: 1px solid black;'>Battery Voltage</td><td style=' border: 1px solid black;text-align:center;'>" + item.BatteryVoltageAlerts + "</td></tr>";

                }

               
            }
            body = body + "</table>";
            return body;
        }

    }
}