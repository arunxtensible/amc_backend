﻿using AMC.IServices;
using AMC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AMC.Controllers
{
    public class ZoneController : Controller
    {
        IZone _iZone;
        public ZoneController(IZone izone)
        {
            _iZone = izone;
        }
        // GET: Zone

        public ActionResult Index()
        {
            List<ZoneModel> _ZoneList = _iZone.GetAllZones();
            return View(_ZoneList);
        }

        public ActionResult Add(ZoneModel model)
        {
            var res = _iZone.Add(model);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Get()
        {
            List<ZoneModel> _ZoneList = _iZone.GetAllZones();
            var res = _ZoneList.Select(y => new DropDownModel
            {
                Name = y.ZoneName,
                Value = y.ZoneID
            });
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult AddorUpdate(ZoneModel model)
        //{
        //    var res = _iZone.Add(model);
        //    return Json(res, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult Update(ZoneModel model)
        {
            var res = _iZone.Update(model);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            var res = _iZone.Delete(id);
            return Json(res, JsonRequestBehavior.AllowGet);
        }


    }
}