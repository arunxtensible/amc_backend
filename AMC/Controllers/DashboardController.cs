﻿using AMC.IServices;
using AMC.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AMC.Controllers
{
   
    public class DashboardController : Controller
    {
        IData _iData;

        public DashboardController(IData iData)
        {
            _iData = iData;
        }

        [HttpGet]
        public JsonResult UpdateDashBoardValues()
        {
            DashboardAlertsModel alerts_model = new DashboardAlertsModel();

            alerts_model = _iData.GetDashboardAlertData();

            return Json(alerts_model, JsonRequestBehavior.AllowGet);
        }

        // GET: Dashboard

        [Authorize]
        public ActionResult Index()
        {
            List<DashboardModel> model = new List<DashboardModel>();
            DashboardModel obj = new DashboardModel();
            DashboardAlertsModel alerts_model = new DashboardAlertsModel();

            alerts_model = _iData.GetDashboardAlertData();

            obj.WDNZoneList = new SelectList(_iData.GetWDNZones(), "ZoneId", "ZoneName");
            obj.ZoneListByType = new SelectList(_iData.GetZonesByType("All"), "ZoneId", "ZoneName");
            obj.ZoneList = new SelectList(_iData.GetZones(), "ZoneId", "ZoneName");
            obj.WTPLIst = new SelectList(_iData.GetWTPList(), "WTPId", "WTPName");

            MapAlertsData(alerts_model);

            //System.Data.DataTable dt = new System.Data.DataTable();
            //dt = _iData.GetWTPChartData();

            ////var wtp_1 = new int[] { 20, 28, 29, 15, 18, 13, 11, 15, 18, 16, 10, 30, 28 };
            ////var wtp_2 = new string[] { "16 Dec 2021", "17 Nov 2021", "18 Nov 2021", "19 Nov 2021", "20 Nov 2021", "21 Nov 2021", "22 Nov 2021", "23 Nov 2021", "24 Nov 2021", "25 Nov 2021", "26 Nov 2021", "27 Nov 2021" };

            //var wtp_1 = dt.AsEnumerable().Select(r => r.Field<int>("val_1")).ToArray();
            //var wtp_2 = dt.AsEnumerable().Select(r => r.Field<string>("val_2")).ToArray();

            //ViewBag.wtp_data = wtp_1;
            //ViewBag.wtp_labels = wtp_2;
            //ViewBag.wtp_label = "Kotarpur 650 (MLD)-Inlet Flow (m3/hr)";

            model.Add(obj);
            return View(model);
        }
        public JsonResult GetZoneByType(string Type)
        {
            List<ZoneMaster> wardList = _iData.GetZonesByType(Type).ToList();
            return Json(wardList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetwardByzonenad(int ZoneId)
        {

            List<WardMaster> wardList = _iData.GetWardByZoneId(ZoneId);
            return Json(wardList , JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult GetLocationByWardZone(string zone, string ward)
        {

            List<WDNMaster> wardList = _iData.GetWDNMaster(zone, ward);
            return Json(wardList, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult GetLocationByZone(string zone)
        {

            List<WDNMaster> wardList = _iData.GetLocations(zone);
            return Json(wardList, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult GetDashboardGraphs(string Type)
        {
            List<DashboardGraphsModel> dashboardGraphs = _iData.GetDashboardGraphData();
            return Json(new
            {
                list = dashboardGraphs
            }, JsonRequestBehavior.AllowGet);

        }

        private Boolean MapAlertsData(DashboardAlertsModel m)
        {
            ViewBag.TamperingAlerts = m.TamperingAlerts.ToString("0.00");
            ViewBag.WaterPressureAlerts = m.WaterPressureAlerts.ToString("0.00");
            ViewBag.WaterQualityAlerts = m.WaterQualityAlerts.ToString("0.00");
            ViewBag.PowerCutAlerts = m.PowerCutAlerts.ToString("0.00");
            ViewBag.CommunicationAlerts = m.CommunicationAlerts.ToString("0.00");
            ViewBag.FlowDeviiationAlerts = m.FlowDeviiationAlerts.ToString("0.00");
            ViewBag.PumpingHourAlerts = m.PumpingHourAlerts.ToString("0.00");
            ViewBag.ZeroFlowAlerts = m.ZeroFlowAlerts.ToString("0.00");
            ViewBag.WaterVSEnergyAlerts = m.WaterVSEnergyAlerts.ToString("0.00");
            ViewBag.BatteryVoltageAlerts = m.BatteryVoltageAlerts.ToString("0.00");
            ViewBag.WaterVSEnergy = m.WaterVSEnergy.ToString("0.00");
            ViewBag.PopulationVSDemandCurrentDay = m.PopulationVSDemandCurrentDay.ToString("0.00");
            ViewBag.PopulationVSDemandPreviousDay = m.PopulationVSDemandPreviousDay.ToString("0.00");
            return true;
        }

        //[HttpGet]
        //public JsonResult UpdateWtpChartData(string KPIName, string WTPName, int Days)
        //{
        //    object Data =null;
        //    System.Data.DataTable dt = new System.Data.DataTable();
        //    dt = _iData.GetWTPChartData(WTPName, KPIName, Days);

        //    //var wtp_1 = new int[] { 20, 28, 29, 15, 18, 13, 11, 15, 18, 16, 10, 30, 28 };
        //    //var wtp_2 = new string[] { "16 Dec 2021", "17 Nov 2021", "18 Nov 2021", "19 Nov 2021", "20 Nov 2021", "21 Nov 2021", "22 Nov 2021", "23 Nov 2021", "24 Nov 2021", "25 Nov 2021", "26 Nov 2021", "27 Nov 2021" };

        //    int[] wtp_1 = dt.AsEnumerable().Select(r => r.Field<int>("val_1")).ToArray();
        //    string[] wtp_2 = dt.AsEnumerable().Select(r => r.Field<string>("val_2")).ToArray();

        //    Data = new { data = wtp_1, lables = wtp_2 } ;

        //    //model.Add(obj);

        //    return Json(Data, JsonRequestBehavior.AllowGet); 
        //}
        [HttpGet]
        public JsonResult UpdateWtpChartData(string KPIName, string WTPName, int Days)
        {
            object Data = null;
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = _iData.GetWTPChartData(WTPName, KPIName, Days);
            //List<ChartModel> listObject = dt.AsEnumerable()
            //                      .Select(x => new ChartModel()
            //                      {
            //                          Value1 = x.Field<string>("val_2"),
            //                          Value2 = x.Field<int>("val_1")
            //                      }).ToList();
            //string[] SubArray1= new string[400];
            //string[] SubArray2 = new string[400];
            //string[,] MainArray = new string[6,2];
            //string[,] MainArray2 = new string[1,6];
            //int i = 0;
            //foreach (var item in listObject)
            //{
            //    SubArray1[i]= item.Value1.ToString();
            //    SubArray2[i]= item.Value2.ToString();
            //    MainArray[i, 0] = item.Value1.ToString();
            //    MainArray[i, 1] = item.Value2.ToString();
            //    MainArray2[0, i] = MainArray[i, 0];
            //    //MainArray2[i, 0] = MainArray[i, 1];
            //    i++;
            //}
           
            //var jsonSerialiser = new System.Web.Script.Serialization.JavaScriptSerializer();
            //var json = jsonSerialiser.Serialize(listObject);
            //string chartObject = json.ToString().Replace("{", "[").Replace("}", "]").Replace("\"Value1\":", "").Replace("\"Value2\":", "").Replace("\"","'");
            

            //return Json(new
            //{
            //    list = listObject.ToArray()
            //}, JsonRequestBehavior.AllowGet);

            //var wtp_1 = new int[] { 20, 28, 29, 15, 18, 13, 11, 15, 18, 16, 10, 30, 28 };
            //var wtp_2 = new string[] { "16 Dec 2021", "17 Nov 2021", "18 Nov 2021", "19 Nov 2021", "20 Nov 2021", "21 Nov 2021", "22 Nov 2021", "23 Nov 2021", "24 Nov 2021", "25 Nov 2021", "26 Nov 2021", "27 Nov 2021" };

            decimal[] wtp_1 = dt.AsEnumerable().Select(r => r.Field<decimal>("val_1")).ToArray();
            string[] wtp_2 = dt.AsEnumerable().Select(r => r.Field<string>("val_2")).ToArray();

            Data = new { data = wtp_1, lables = wtp_2 };

            //model.Add(obj);

            return Json(Data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetWDSData(int x)
        {
            var a = _iData.GetWDSList(x);
            var  c= a.AsEnumerable().Select(s => new Tuple<int,string> (s.WDSId,s.WDSName)).ToList();
                       
            return Json(c, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult UpdateWdsChartData(string ZoneName, string KPIName, string WDSName, int Days, string iterationDatetime)
        {
            object Data = null;
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = _iData.GetWDSChartData(ZoneName, WDSName, KPIName, Days, iterationDatetime);

            decimal[] wtp_1 = dt.AsEnumerable().Select(r => r.Field<decimal>("val_1")).ToArray();
            string[] wtp_2 = dt.AsEnumerable().Select(r => r.Field<string>("val_2")).ToArray();

            Data = new { data = wtp_1, lables = wtp_2 };

            return Json(Data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult UpdateWdsChartDataWithComparison(string ZoneName, string Location, string KPIName, string FromDate, string ToDate)
        {
            object Data = null;
            if (KPIName == "Please Select" || ZoneName == "Select Zone" ||  Location == "Select" || (Convert.ToDateTime(ToDate) < Convert.ToDateTime(FromDate)))
            {
                Data = new { data = new LineChartModel[0], lables = new string[0] };
                return Json(Data, JsonRequestBehavior.AllowGet);

            }
            List<LineChartIterationDateModel> iterationDateList = new List<LineChartIterationDateModel>();
            iterationDateList = _iData.GetWDSChartDataLabel(ZoneName, Location, KPIName, FromDate, ToDate);
            int recordCount = iterationDateList.Count;
            if (recordCount <= 0)
            {
                Data = new { data = new LineChartModel[0], lables = new string[0] };
                return Json(Data, JsonRequestBehavior.AllowGet);
            }
            LineChartModel[] model = _iData.GetWDSChartDataCompare(ZoneName, Location, KPIName, FromDate, ToDate);

            string[] labels = new string[recordCount];
            int i = 0;
            foreach (var item in iterationDateList)
            {
                // labels[i] = Convert.ToDateTime(item.DateAndTime).ToString("dd/MM/yyyy hh:mm:ss tt");
                labels[i] = item.DateAndTime;
                i++;
            }

            Data = new { data = model, lables = labels };
            return Json(Data, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult UpdateWdnChartData(string ZoneName, string WardName, string Location, string KPIName, string Days, int iterationdays=1)
        {
              object Data = null;
             if (String.IsNullOrEmpty(Days))
            {
                return Json(Data, JsonRequestBehavior.AllowGet);
            }
            System.Data.DataTable dt = new System.Data.DataTable();
              dt = _iData.GetWDNChartData(ZoneName, WardName, Location, KPIName, Days, iterationdays);
    
            decimal[] wtp_1 = dt.AsEnumerable().Select(r => r.Field<decimal>("val_1")).ToArray();
            string[] wtp_2 = dt.AsEnumerable().Select(r => r.Field<string>("val_2")).ToArray();

            Data = new { data = wtp_1, lables = wtp_2 };

            return Json(Data, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult UpdateWdnChartDataWithComparison(string ZoneName, string WardName, string Location, string KPIName, string FromDate, string ToDate)
        {
            object Data = null;
            if (KPIName=="Please Select" || ZoneName == "Select Zone" || WardName == "Select Ward" || Location == "Select Location" || (Convert.ToDateTime(ToDate)< Convert.ToDateTime(FromDate)))
            {
                Data = new { data = new LineChartModel[0], lables = new string[0] };
                return Json(Data, JsonRequestBehavior.AllowGet);

            }
            List<LineChartIterationDateModel> iterationDateList = new List<LineChartIterationDateModel>();
            iterationDateList = _iData.GetWDNChartDataLabel(ZoneName, WardName, Location, KPIName, FromDate, ToDate);
            int recordCount= iterationDateList.Count;
            if (recordCount <= 0)
            {
                Data = new { data = new LineChartModel[0], lables = new string[0] };
                return Json(Data, JsonRequestBehavior.AllowGet);
            }
            LineChartModel[] model = _iData.GetWDNChartDataCompare(ZoneName, WardName, Location, KPIName, FromDate, ToDate);
            
            string[] labels = new string[recordCount];
            int i = 0;
            foreach (var item in iterationDateList)
            {
               // labels[i] = Convert.ToDateTime(item.DateAndTime).ToString("dd/MM/yyyy hh:mm:ss tt");
                labels[i] = item.DateAndTime;
                i++;
            }
            
            Data = new { data = model, lables = labels };
            return Json(Data, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        //public JsonResult UpdateWdnChartDataWithComparison(string ZoneName, string WardName, string Location, string KPIName, string Days, int iterationdays = 1)
        //{
        //    object Data = null;
        //    LineChartModel[] model = _iData.GetWDNChartDataCompare(ZoneName, WardName, Location, KPIName, Days, iterationdays);
        //    DateTime iterationDateAndTime = Convert.ToDateTime(Days);
        //    string[] labels = new string[iterationdays];
        //    for (int i = 0; i < iterationdays; i++)
        //    {
        //        labels[i] = Convert.ToDateTime(iterationDateAndTime).AddDays(-i).ToString("dd/MM/yyyy hh:mm:ss tt");
        //    }
        //    Data = new { data = model, lables = labels };
        //    return Json(Data, JsonRequestBehavior.AllowGet);
        //}

        //[HttpGet]
        //public JsonResult UpdateWdnChartDataWithComparison(string ZoneName, string WardName, string Location, string KPIName, string Days, int iterationdays = 1)
        //{
        //    object Data = null;

        //    int arrayItem = 0;
        //    System.Data.DataTable dt = new System.Data.DataTable();
        //    dt = _iData.GetWDNChartData(ZoneName, WardName, Location, KPIName, Days, iterationdays);
        //    string[] label = new string[dt.Rows.Count];
        //    List<Dictionary<string, object>> allSeries = new List<Dictionary<string, object>>();
        //    foreach (DataRow dr1 in dt.Rows)
        //    {
        //        Dictionary<string, object> aSeries = new Dictionary<string, object>();
        //        aSeries["name"] = dr1["val_2"];
        //        aSeries["data"] = new List<int>();
        //        int N = dr1.ItemArray.Length;
        //        for (int i = 0; i < 1; i++)
        //        {
        //            if (i == 0)
        //            {
        //                label[arrayItem] = dr1[1].ToString();
        //            }
        //            int val = Convert.ToInt16(dr1[i]);
        //            ((List<int>)aSeries["data"]).Add(val);
        //        }
        //        allSeries.Add(aSeries);
        //    }
        //    string jsonSeries = Newtonsoft.Json.JsonConvert.SerializeObject(allSeries);
        //    string[] wtp21 = new string[] { "vishal", "pawar" };
        //    Data = new { data = jsonSeries.Replace("[15]", "[15,20]").Replace("\"name\":", " \"name\": ").Replace("\"data\":", " \"data\": "), lables = label };
        //    //Data = new { data = "[{  name: 'Tokyo',   data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]    }, {  name: 'London', data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]}]", lables = label };
        //    return Json(Data, JsonRequestBehavior.AllowGet);
        //    //if (String.IsNullOrEmpty(Days))
        //    //{
        //    //    return Json(Data, JsonRequestBehavior.AllowGet);
        //    //}
        //    //System.Data.DataTable dt = new System.Data.DataTable();
        //    //dt = _iData.GetWDNChartData(ZoneName, WardName, Location, KPIName, Days, iterationdays);
        //    decimal[] wtp2_dec = new decimal[] { 3, 4, 3, 5, 4, 10, 12 };
        //    //string []wtp2 = new string[] { "Monday", "Tuesday",   "Wednesday",   "Thursday",   "Friday", "Saturday",  "Sunday" };
        //    //dynamic obj2 = new  {  items = new[] {    new {name = "John" , index = [ 3, 4, 3, 5, 4, 10, 12 ]},
        //    //                                          new {name = "Jane" , index = [ 13, 14, 13, 15, 14, 20, 21 ]}
        //    //                                     }
        //    //                    };


        //    decimal[]wtp2_dec1 = new decimal[] { 13, 14, 13, 15, 14, 20, 21 };
        //    //string wtp2 = "[ 'Monday', 'Tuesday',    'Wednesday',   'Thursday',   'Friday', 'Saturday',  'Sunday'  ]";
        //    string[] wtp1 = new string[] { "{name: 'John',data:"+ wtp2_dec + " }, {name: 'Jane',data:"+ wtp2_dec1+"" };
        //    //string wtp1 = "[{name: 'John',data: [3, 4, 3, 5, 4, 10, 12] }, {name: 'Jane',data: [1, 3, 4, 3, 3, 5, 4]}]";

        //    //decimal[] wtp_1 = dt.AsEnumerable().Select(r => r.Field<decimal>("val_1")).ToArray();
        //    //string[] wtp_2 = dt.AsEnumerable().Select(r => r.Field<string>("val_2")).ToArray();

        //    //Data = new { data = wtp_1, lables = wtp_2 };
        //  //  Data = new { data = wtp1, lables = obj2 };

        //    return Json(Data, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult GetTampringAlertsData(string alert_type)
        {

            List<TampringAlerts> tampringAlerts = _iData.GetTampringAlertsData(alert_type);
            return Json(new
            {
                list = tampringAlerts
            }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult UpdateWdsProjectScoreChartData(string ZoneName, string WDSLocation, string KPIName, string Days)
        {
            object Data = null;
            System.Data.DataTable dt = new System.Data.DataTable();

            dt = _iData.GetWDSProjectScoreChartData(ZoneName, WDSLocation, KPIName, Days);

            int[] wtp_1 = dt.AsEnumerable().Select(r => r.Field<int>("val_1")).ToArray();
            string[] wtp_2 = dt.AsEnumerable().Select(r => r.Field<string>("val_2")).ToArray();

            System.Data.DataTable dt2 = new System.Data.DataTable();
            dt2 = _iData.GetWDSProjectScoreChartDataPressure(ZoneName, WDSLocation, KPIName, Days);

            int[] wtp_3 = dt2.AsEnumerable().Select(r => r.Field<int>("val_1")).ToArray();
            string[] wtp_4 = dt2.AsEnumerable().Select(r => r.Field<string>("val_2")).ToArray();

            Data = new { data = wtp_1, lables = wtp_2, data2 = wtp_3, lables2 = wtp_4 };
            
            return Json(Data, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult UpdateWdnProjectScoreChartData(string ZoneName, string WardName, string Location, string KPIName, int Days)
        {
            object Data = null;
            System.Data.DataTable dt = new System.Data.DataTable();
            dt = _iData.GetWDNProjectScoreChartData(ZoneName, WardName, Location, KPIName, Days);

            int[] wtp_1 = dt.AsEnumerable().Select(r => r.Field<int>("val_1")).ToArray();
            string[] wtp_2 = dt.AsEnumerable().Select(r => r.Field<string>("val_2")).ToArray();

            Data = new { data = wtp_1, lables = wtp_2 };

            return Json(Data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetWDSLocationByZone(int ZoneId)
        {

            List<WDSLocationModel> wardList = _iData.GetWDSLocationByZone(ZoneId);
            return Json(wardList, JsonRequestBehavior.AllowGet);

        }

        public ActionResult UFW(string type)
        {
            string Type = type;
            if (String.IsNullOrEmpty(Type))
                 Type = "Select All Locations";
            List<UFWModel> _UFWList = _iData.GetUFWDetailsByType(Type);
            if (_UFWList.ToList().Count > 0)
            {
                foreach (var item in _UFWList.ToList())
                {
                    ViewBag.Total = item.Total;
                    break;
                }
            }
            List<LDQValuesForUFWModel> _UFWLDQDetails = _iData.GetUFW_LDQDetails();
            if (_UFWLDQDetails.ToList().Count > 0)
            {
                foreach (var item in _UFWLDQDetails.ToList())
                {
                    ViewBag.WTPOutletLDQ = item.WTPOutletLDQ;
                    ViewBag.WDSInletLDQ = item.WDSInletLDQ;
                    ViewBag.UFWLastDayMLD = item.UFWLastDayMLD;
                    ViewBag.UFWKnownCionsumption = item.UFWKnownCionsumption;
                    ViewBag.UFWMLD = item.UFWMLD;
                    break;
                }
            }
            return View(_UFWList);
        }

        public ActionResult WaterVsEnergy()
        {
            List<WaterVsEnergyModel> _WtpList =_iData.GetWaterVsEnergyDetails();
            return View(_WtpList);
        }
    }
}