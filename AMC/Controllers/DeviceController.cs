﻿using AMC.IServices;
using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AMC.Controllers
{
    public class DeviceController : Controller
    {
        IDevice _iDevice;
        public DeviceController(IDevice idevice)
        {
            _iDevice = idevice;
        }
        // GET: Zone

        public ActionResult Index()
        {
            List<DeviceModel> _WardList = _iDevice.GetAllDevice();
            return View(_WardList);
        }

        public ActionResult Add(DeviceModel model)
        {
            var res = _iDevice.Add(model);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Update(DeviceModel model)
        {
            var res = _iDevice.Update(model);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            var res = _iDevice.Delete(id);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

    }
}