﻿$(document).ready(function () {

    var wdnProjectScoreChart = null;
    function KpiWdnProjectScoreChanged() {

        var zonename = $('#ZoneListCharViewWDN').find(":selected").text();
        var wardname = $('#ward2').find(":selected").text();
        var location = $('#location2').find(":selected").text();
        var kpivalue = $('#kpi-wdn').find(":selected").text();
        var wdndaysvalue = 7;//$('#KPI_datetime_wdn_chart').find(":selected").text();//5 //$('#wdndays').val();
        var wdnchartlable = $('#kpi-wdn').find(":selected").text(); //$('#kpi-wdn').val();

        if (wdnProjectScoreChart != null) {
            wdnProjectScoreChart.destroy();
        }


        $.get("/Dashboard/UpdateWdnProjectScoreChartData", { "ZoneName": zonename, "WardName": wardname, "Location": location, "KPIName": kpivalue, "Days": wdndaysvalue  }, function (Result) {
            console.log(Result);
            wdnchart_data = wdnchart_data;
            wdnchart_labels = wdnchart_labels;

            var wdnchart_data = Result.data;
            var wdnchart_labels = Result.lables;
            var wdnchart_label = wdnchartlable;//"New Label1";

            loadWdnProjectScoreChart(wdnchart_data, wdnchart_labels, wdnchart_label);
        });


    }

    function loadWdnProjectScoreChart(wdnchart_data, wdnchart_labels, wdnchart_label) {
        var ctxProjectScore = document.getElementById("projectwdnchart").getContext('2d');

        wdnProjectScoreChart = new Chart(ctxProjectScore, {
            type: 'line',
            data: {
                labels: wdnchart_labels,
                datasets: [{
                    label: 'City_corner Totalliser',
                    data: wdnchart_data,
                    borderWidth: 1,
                    fill: false,
                    borderColor: '#469529'
                }
                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

    }

    function loadWDNList() {
        $('#wdn')
            .empty()
            .append('<option selected="selected" value="">select</option>')
            ;
        $.get("/Dashboard/GetWDNData", {
            "x": $("#ZoneListForChartWDN").val()
        }, function (Result) {
            $.each(Result, function (index) {
                $('#wdn')
                    .append($("<option></option>")
                        .attr("value", Result[index].Item1)
                        .text(Result[index].Item2));
            });
        });
    }


    $("#kpi-wdn-project-score").on("change", KpiWdnProjectScoreChanged);
    //$("#ddl-wdn").on("change", KpiWdnChanged);
    //$("#wdndays").on("change", KpiWdnChanged);
    //$("#ZoneListForChartWDN").on("change", loadWDNList);
});