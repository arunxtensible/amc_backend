﻿$(document).ready(function () {

    var wdsChart = null;
    function KpiWdsChanged() {

        var zonename = 'Central';
        var kpivalue = $('#kpi-wds').find(":selected").text(); //$('#kpi-wds').val();
        var ddlvalue = $('#wds').find(":selected").text(); //$('#ddl-wds').val();
        var wdsdaysvalue = $('#wdsdays').val();
        var wdschartlable = $('#kpi-wds').find(":selected").text(); //$('#kpi-wds').val();

        if (wdsChart != null) {
            wdsChart.destroy();
        }

        //-----------old charts
        //$.get("/Dashboard/UpdateWdsChartData", { "ZoneName": zonename, "KPIName": kpivalue, "WDSName": ddlvalue, "Days": wdsdaysvalue }, function (Result) {
        //    console.log(Result);
        //    wdschart_data = wdschart_data;
        //    wdschart_labels = wdschart_labels;

        //    var wdschart_data = Result.data;
        //    var wdschart_labels = Result.lables;
        //    var wdschart_label = wdschartlable;

        //    loadWdsChart(wdschart_data, wdschart_labels, wdschart_label);
        //});

        //---------------------new hicharts
        $.get("/Dashboard/UpdateWdsChartData", { "ZoneName": zonename, "KPIName": kpivalue, "WDSName": ddlvalue, "Days": wdsdaysvalue }, function (Result) {
            var YAxisText = $('#kpi-wds  option:selected').text();
            var ChartTitle = $('#wds  option:selected').text();
            var unit = 'm3';
            if ($('#kpi-wds  option:selected').is(":contains('hr')")) {
                unit = 'm3/hr';
            }
            else if ($('#kpi-wds  option:selected').is(":contains('MTR')")) {
                unit = 'MTR';
            }
            else if ($('#kpi-wds  option:selected').is(":contains('KWH')")) {
                unit = 'KWH';
            }
            else if ($('#kpi-wds  option:selected').is(":contains('PSI')")) {
                unit = 'PSI';
            }
            if (ChartTitle === 'select') {
                ChartTitle = '';
            }
            console.log("UpdateWDSChartData====");
            console.log(Result['data']);
            console.log(Result);
            var result2 = Result;
            console.log(result2['data']);
            Highcharts.chart('wdschartcontainer', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Water Distributon Station :- '+ChartTitle
                },
                subtitle: {
                    text: 'Source: Ahmedabad Municipal Corporation'
                },
                xAxis: {
                    categories: result2['lables'],
                    crosshair: true,
                    title: {
                        text: 'Date And Time'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: YAxisText
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr>' +
                        '<td style="padding:0"><b>{point.y:.2f} ' + unit + '</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: YAxisText,
                    data: result2['data']

                }]
            });
        });

    }

    function loadWdsChart(wdschart_data, wdschart_labels, wdschart_label) {
        var ctx = document.getElementById("wdschart").getContext('2d');
        wdsChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: wdschart_labels,
                datasets: [{
                    label: wdschart_label,
                    backgroundColor: '#57b7fa',
                    barThickness: 25,
                    maxBarThickness: 50,
                    borderColor: '#57b7fa',
                    data: wdschart_data,
                }]
            },

            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            },
        });
    }

    function loadWDSList() {
        $('#wds')
            .empty()
            .append('<option selected="selected" value="">select</option>')
            ;
        $.get("/Dashboard/GetWDSData", {
            "x": $("#ZoneListForChartWDS").val()
        }, function (Result) {
            $.each(Result, function (index) {
                $('#wds')
                    .append($("<option></option>")
                        .attr("value", Result[index].Item1)
                        .text(Result[index].Item2));
            });
        });
    }


    $("#kpi-wds").on("change", KpiWdsChanged);
    $("#ddl-wds").on("change", KpiWdsChanged);
    $("#wdsdays").on("change", KpiWdsChanged);
    $("#ZoneListForChartWDS").on("change", loadWDSList);
});