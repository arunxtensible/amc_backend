﻿$(document).ready(function () {

    var wdsProjectScoreChart = null;
    function KpiWdsProjectScoreChanged() {

        var zonename = $('#ZoneListProjectWDS').val();
        var wdslocation = $('#wds-project-score').find(":selected").text();
        var kpivalue = $('#kpi-wdn').val();
        var wdsdaysvalue = $('#datetimepicker_ForProjectScoreWDS').val(); //$('#wdndays').val();
        console.log(wdsdaysvalue);
        if (wdsdaysvalue === 'Invalid date' || wdsdaysvalue==='') {
            return;
        }
        wdsdaysvalue = moment(wdsdaysvalue).format("YYYY-MM-DD HH:mm:ss");
        console.log(wdsdaysvalue);
        var wdschartlable = $('#kpi-wdn').find(":selected").text(); //$('#kpi-wdn').val();

        if (wdsProjectScoreChart != null) {
            wdsProjectScoreChart.destroy();
        }


        $.get("/Dashboard/UpdateWdsProjectScoreChartData", { "ZoneName": zonename, "WDSLocation": wdslocation, "KPIName": kpivalue, "Days": wdsdaysvalue }, function (Result) {
            console.log(Result);
            wdschart_data = wdschart_data;
            wdschart_labels = wdschart_labels;

            var wdschart_data = Result.data;
            var wdschart_labels = Result.lables;
            var pressure_data = Result.data2;
            var pressure_labels = Result.lables2;
            loadWdsProjectScoreChart(wdschart_data, wdschart_labels, pressure_data, pressure_labels);
        });


    }

    function loadWdsProjectScoreChart(wdschart_data, wdschart_labels,  pressure_data, pressure_labels) {
        var ctxProjectScore = document.getElementById("projectwdschart").getContext('2d');

        wdsProjectScoreChart = new Chart(ctxProjectScore, {
            type: 'line',
            data: {
                labels: wdschart_labels,
                datasets: [{
                    label: 'Flow',
                    data: wdschart_data,
                    borderWidth: 1,
                    fill: false,
                    borderColor: '#469529'
                }, {
                        label: 'Pressure',
                        data: pressure_data,
                        borderWidth: 1,
                        fill: false,
                        borderColor: 'red'
                    }
                ]
            }
            ,
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

    }

    function loadWDSList() {
        $('#wds')
            .empty()
            .append('<option selected="selected" value="">select</option>')
            ;
        $.get("/Dashboard/GetWDSData", {
            "x": $("#ZoneListForChartWDS").val()
        }, function (Result) {
            $.each(Result, function (index) {
                $('#wds')
                    .append($("<option></option>")
                        .attr("value", Result[index].Item1)
                        .text(Result[index].Item2));
            });
        });
    }


    $("#kpi-wds-project-score").on("change", KpiWdsProjectScoreChanged);
    $("#wds-project-score").on("change", KpiWdsProjectScoreChanged);
    $("#ZoneListProjectWDS").on("change", KpiWdsProjectScoreChanged);
    $("#datetimepicker_ForProjectScoreWDS").on("change", KpiWdsProjectScoreChanged);
    
    //$("#ddl-wds").on("change", KpiWdsChanged);
    //$("#wdsdays").on("change", KpiWdsChanged);
    //$("#ZoneListForChartWDS").on("change", loadWDSList);
});