﻿$(document).ready(function () {
    
    var wdnChart = null; 
    $("#KPI_datetime_wdn_chart").change(function () {
        KpiWdnChanged();
    });
    $("#wdn_days").change(function () {
        KpiWdnChanged();
    });
  $("#location2").change(function () {
        KpiWdnChanged();
    });
    //function KpiWdnChangedWithCompare() {
    //    $('#wdnchartcontainer').css('display', 'none');
    //    $('#wdnchartcontainerWithCompare').css('display', 'none');
    //    $('#wdnchartLoader').css('display', 'block');
    //    var zonename = $('#ZoneListCharViewWDN').find(":selected").text();
    //    var wardname = $('#ward2').find(":selected").text();
    //    var location = $('#location2').find(":selected").text();
    //    //var kpivalue = $('#kpi-wdn').find(":selected").text();
    //    var kpivalue = $('#choices-multiple-remove-button').find(":selected").text();
    //    var wdndaysvalue = $('#KPI_datetime_wdn_chart').val().toString();//5 
    //    var wdn_days = $('#wdn_days').val();
    //    //$('#wdndays').val();  var date = //var ldr = moment(element.ldr).format("DD/MM/YYYY HH:mm");
    //    var wdnchartlable = $('#kpi-wdn').find(":selected").text(); //$('#kpi-wdn').val();

    //    if (wdnChart !== null) {
    //        wdnChart.destroy();
    //    }
    //    var kpiString = kpivalue;
    //    kpiString = kpiString.replaceAll(")", "),");
    //    var count = (kpiString.match(/,/g) || []).length;
    //    //$.get("/Dashboard/UpdateWdnChartData", { "ZoneName": zonename, "WardName": wardname, "Location": location, "KPIName": kpivalue, "Days": wdndaysvalue, "iterationdays": wdn_days }, function (Result) {
    //    //    console.log(Result);
    //    //    wdnchart_data = wdnchart_data;
    //    //    wdnchart_labels = wdnchart_labels;

    //    //    var wdnchart_data = Result.data;
    //    //    var wdnchart_labels = Result.lables;
    //    //    var wdnchart_label = wdnchartlable;//"New Label1";

    //    //    loadWdnChart(wdnchart_data, wdnchart_labels, wdnchart_label);
    //    //});

    //    //---------------------new hicharts
    //    if (count == 1 || kpivalue=='') {
    //        //----------------BarChar-----new hicharts
    //        $.get("/Dashboard/UpdateWdnChartData", { "ZoneName": zonename, "WardName": wardname, "Location": location, "KPIName": kpivalue, "Days": wdndaysvalue, "iterationdays": wdn_days }, function (Result) {
    //            //var YAxisText = $('#choices-multiple-remove-button:selected').text();
    //            //var ChartTitle = $('#location2  option:selected').text();
    //            //var unit = 'm3';
    //            //if ($('#kpi-wdn  option:selected').is(":contains('hr')")) {
    //            //    unit = 'm3/hr';
    //            //}
    //            //else if ($('#kpi-wdn  option:selected').is(":contains('MTR')")) {
    //            //    unit = 'MTR';
    //            //}
    //            //else if ($('#kpi-wdn  option:selected').is(":contains('VDC')")) {
    //            //    unit = 'VDC';
    //            //}
    //            //else if ($('#kpi-wdn  option:selected').is(":contains('PSI')")) {
    //            //    unit = 'PSI';
    //            //}
    //            //if (ChartTitle === 'Select Location') {
    //            //    ChartTitle = '';
    //            //}
    //            var YAxisText = kpiString;
    //            var ChartTitle = $('#location2  option:selected').text();
    //            var unit = 'm3';
    //            if (kpiString.includes('hr')) {
    //                unit = 'm3/hr';
    //            }
    //            else if (kpiString.includes('MTR')) {
    //                unit = 'MTR';
    //            }
    //            else if (kpiString.includes('VDC')) {
    //                unit = 'VDC';
    //            }
    //            else if (kpiString.includes('PSI')) {
    //                unit = 'PSI';
    //            }
    //            if (ChartTitle === 'Select Location') {
    //                ChartTitle = '';
    //            }

    //            console.log('JSON Result - : ' + Result);
    //            var result2 = Result;
    //            console.log('labels :- ' + result2['lables']);
    //            console.log('Data :- ' + result2['data']);
    //            $('#wdnchartcontainer').css('display', 'block');
    //            $('#wdnchartLoader').css('display', 'none');
    //            $('#wdnchartcontainerWithCompare').css('display', 'none');
    //            Highcharts.chart('wdnchartcontainer', {
    //                chart: {
    //                    type: 'column'
    //                },
    //                title: {
    //                    text: 'Water Distributon Network :- ' + ChartTitle
    //                },
    //                subtitle: {
    //                    text: 'Source: Ahmedabad Municipal Corporation'
    //                },
    //                xAxis: {
    //                    categories: result2['lables'],
    //                    crosshair: true,
    //                    title: {
    //                        text: 'Date And Time'
    //                    }
    //                },
    //                yAxis: {
    //                    min: 0,
    //                    title: {
    //                        text: YAxisText
    //                    }
    //                },
    //                tooltip: {
    //                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    //                    pointFormat: '<tr>' +
    //                        '<td style="padding:0"><b>{point.y:.2f} ' + unit + '</b></td></tr>',
    //                    footerFormat: '</table>',
    //                    shared: true,
    //                    useHTML: true
    //                },
    //                plotOptions: {
    //                    column: {
    //                        pointPadding: 0.2,
    //                        borderWidth: 0
    //                    }
    //                },
    //                series: [{
    //                    name: YAxisText,
    //                    data: result2['data']

    //                }]
    //            });


    //        });

    //    }
    //    else {
    //        $.get("/Dashboard/UpdateWdnChartDataWithComparison", { "ZoneName": zonename, "WardName": wardname, "Location": location, "KPIName": kpivalue, "Days": wdndaysvalue, "iterationdays": wdn_days }, function (Result) {
    //            var YAxisText = $('#kpi-wdn  option:selected').text();
    //            var ChartTitle = $('#location2  option:selected').text();
    //            var unit = 'm3';
    //            if ($('#kpi-wdn  option:selected').is(":contains('hr')")) {
    //                unit = 'm3/hr';
    //            }
    //            else if ($('#kpi-wdn  option:selected').is(":contains('MTR')")) {
    //                unit = 'MTR';
    //            }
    //            else if ($('#kpi-wdn  option:selected').is(":contains('VDC')")) {
    //                unit = 'VDC';
    //            }
    //            else if ($('#kpi-wdn  option:selected').is(":contains('PSI')")) {
    //                unit = 'PSI';
    //            }
    //            if (ChartTitle === 'Select Location') {
    //                ChartTitle = '';
    //            }
    //            var result2 = Result;
    //            console.log('labels :- ' + result2['lables']);
    //            console.log('Data :- ' + result2['data']);
    //            var jsonResponse = result2['data'];
    //            console.log(' json data res : ' + jsonResponse);
    //            var seriesOutput = jsonResponse;
    //            //var jsonResponse = '[{ "name": "09/02/2022 11:40:15 AM", "data": [15,20]}]';


    //            $('#wdnchartLoader').css('display', 'none');
    //            $('#wdnchartcontainer').css('display', 'none');
    //            $('#wdnchartcontainerWithCompare').css('display', 'block');

    //            //Highcharts.chart('wdnchartcontainerWithCompare', {
    //            //    chart: {
    //            //        type: 'area'
    //            //    },
    //            //    title: {
    //            //        text: 'Historic and Estimated Worldwide Population Growth by Region'
    //            //    },
    //            //    subtitle: {
    //            //        text: 'Source: Wikipedia.org'
    //            //    },
    //            //    xAxis: {
    //            //        categories: result2['lables'],
    //            //        tickmarkPlacement: 'on',
    //            //        title: {
    //            //            enabled: false
    //            //        }
    //            //    },
    //            //    yAxis: {
    //            //        title: {
    //            //            text: 'Billions'
    //            //        },
    //            //        labels: {
    //            //            formatter: function () {
    //            //                return this.value / 1000;
    //            //            }
    //            //        }
    //            //    },
    //            //    tooltip: {
    //            //        split: true,
    //            //        valueSuffix: ' millions'
    //            //    },
    //            //    plotOptions: {
    //            //        area: {
    //            //            stacking: 'normal',
    //            //            lineColor: '#666666',
    //            //            lineWidth: 1,
    //            //            marker: {
    //            //                lineWidth: 1,
    //            //                lineColor: '#666666'
    //            //            }
    //            //        }
    //            //    },
    //            //    series: [{ name: "flow", data: [15, 27,0,37] }
    //            //        , { name: "pressure", data: [8, 39,17,0] }
    //            //             ]
    //            //});


    //            Highcharts.chart('wdnchartcontainerWithCompare', {
    //                chart: {
    //                    type: 'line'
    //                },
    //                title: {
    //                    text: ''
    //                },
    //                subtitle: {
    //                    text: 'Source: Ahmedabad Municipal Corporation'
    //                },
    //                xAxis: {
    //                    categories: result2['lables']
    //                },
    //                yAxis: {
    //                    title: {
    //                        text: ''
    //                    }
    //                },
    //                plotOptions: {
    //                    line: {
    //                        dataLabels: {
    //                            enabled: true
    //                        },
    //                        enableMouseTracking: false
    //                    }
    //                },
    //                series: seriesOutput
    //                //series: result2['data']

    //            });
    //        });

    //    }
        

    //}
    function KpiWdnChanged() {
        $('#wdnchartcontainer').css('display', 'none');
        $('#wdnchartLoader').css('display', 'block');
        var zonename = $('#ZoneListCharViewWDN').find(":selected").text();
        var wardname = $('#ward2').find(":selected").text();
        var location = $('#location2').find(":selected").text();
        var kpivalue = $('#kpi-wdn').find(":selected").text();
        var wdndaysvalue = $('#KPI_datetime_wdn_chart').val().toString();//5 
        var wdn_days = $('#wdn_days').val();
        //$('#wdndays').val();  var date = //var ldr = moment(element.ldr).format("DD/MM/YYYY HH:mm");
        var wdnchartlable = $('#kpi-wdn').find(":selected").text(); //$('#kpi-wdn').val();

        if (wdnChart !== null) {
            wdnChart.destroy();
        }


        //$.get("/Dashboard/UpdateWdnChartData", { "ZoneName": zonename, "WardName": wardname, "Location": location, "KPIName": kpivalue, "Days": wdndaysvalue, "iterationdays": wdn_days }, function (Result) {
        //    console.log(Result);
        //    wdnchart_data = wdnchart_data;
        //    wdnchart_labels = wdnchart_labels;

        //    var wdnchart_data = Result.data;
        //    var wdnchart_labels = Result.lables;
        //    var wdnchart_label = wdnchartlable;//"New Label1";

        //    loadWdnChart(wdnchart_data, wdnchart_labels, wdnchart_label);
        //});

        //---------------------new hicharts
        $.get("/Dashboard/UpdateWdnChartData", { "ZoneName": zonename, "WardName": wardname, "Location": location, "KPIName": kpivalue, "Days": wdndaysvalue, "iterationdays": wdn_days }, function (Result) {
            var YAxisText = $('#kpi-wdn  option:selected').text();
            var ChartTitle = $('#location2  option:selected').text();
            var unit = 'm3';
            if ($('#kpi-wdn  option:selected').is(":contains('hr')")) {
                unit = 'm3/hr';
            }
            else if ($('#kpi-wdn  option:selected').is(":contains('MTR')")) {
                unit = 'MTR';
            }
            else if ($('#kpi-wdn  option:selected').is(":contains('VDC')")) {
                unit = 'VDC';
            }
            else if ($('#kpi-wdn  option:selected').is(":contains('PSI')")) {
                unit = 'PSI';
            }
            if (ChartTitle === 'Select Location') {
                ChartTitle = '';
            }
            
           
            console.log('JSON Result - : '+Result);
            var result2 = Result;
            console.log('labels :- ' + result2['lables']);
            console.log('Data :- '+result2['data']);
            $('#wdnchartcontainer').css('display', 'block');
            $('#wdnchartLoader').css('display', 'none');
            Highcharts.chart('wdnchartcontainer', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Water Distributon Network :- ' + ChartTitle
                },
                subtitle: {
                    text: 'Source: Ahmedabad Municipal Corporation'
                },
                xAxis: {
                    categories: result2['lables'],
                    crosshair: true,
                    title: {
                        text: 'Date And Time'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: YAxisText
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr>' +
                        '<td style="padding:0"><b>{point.y:.2f} ' + unit + '</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: YAxisText,
                    data: result2['data']

                }]
            });

          
        });


    }

    function loadWdnChart(wdnchart_data, wdnchart_labels, wdnchart_label) {
        var ctx = document.getElementById("wdnchart").getContext('2d');
        wdnChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: wdnchart_labels,
                datasets: [{
                    label: wdnchart_label,
                    backgroundColor: '#57b7fa',
                    barThickness: 25,
                    maxBarThickness: 50,
                    borderColor: '#57b7fa',
                    data: wdnchart_data,
                }]
            },

            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            },
        });
    }

    
    $("#KPI_datetime2").on("change", KpiWdnChanged);
    $("#kpi-wdn").on("change", KpiWdnChanged);
    $("#choices-multiple-remove-button").on("change", KpiWdnChanged);
    //$("#ddl-wdn").on("change", KpiWdnChanged);
    //$("#wdndays").on("change", KpiWdnChanged);
    //$("#ZoneListForChartWDS").on("change", loadWDSList);

    $("#kpi-wdn-LineChart").change(function () {
        KpiWdnLineChartChanged();
    });
    $("#KPI_datetime_wdn_chartToDate").change(function () {
        KpiWdnLineChartChanged();
    });
    $("#KPI_datetime_wdn_chartFromDate").change(function () {
        KpiWdnLineChartChanged();
    });
    $("#location3").change(function () {
        KpiWdnLineChartChanged();
    });

    function KpiWdnLineChartChanged() {
        $('#wdnchartLoaderLineChart').css('display', 'block');
        $('#wdnLinechartcontainerWithCompare').css('display', 'none');      
        var zonename = $('#ZoneListCharViewWDNLineChart').find(":selected").text();
        var wardname = $('#ward3').find(":selected").text();
        var location = $('#location3').find(":selected").text();
        var kpivalue = $('#kpi-wdn-LineChart').find(":selected").text();
        var fromDate = $('#KPI_datetime_wdn_chartFromDate').val().toString();//5 
        var toDate = $('#KPI_datetime_wdn_chartToDate').val().toString();//5 
        var wdnchartlable = kpivalue;

        if (wdnChart !== null) {
            wdnChart.destroy();
        }
       
        //---------------------new hicharts
      
        $.get("/Dashboard/UpdateWdnChartDataWithComparison", { "ZoneName": zonename, "WardName": wardname, "Location": location, "KPIName": kpivalue, "FromDate": fromDate, "ToDate": toDate }, function (Result) {
            $('#wdnchartLoaderLineChart').css('display', 'none');
            var YAxisText = $('#kpi-wdn  option:selected').text();
                var ChartTitle = $('#location2  option:selected').text();
            var unit = 'm3/hr vs PSI';
              
                if (ChartTitle === 'Select Location') {
                    ChartTitle = '';
                }
                var result2 = Result;
                console.log('labels :- ' + result2['lables']);
                console.log('Data :- ' + result2['data']);
                var jsonResponse = result2['data'];
                console.log(' json data res : ' + jsonResponse);
                var seriesOutput = jsonResponse;
                //var jsonResponse = '[{ "name": "09/02/2022 11:40:15 AM", "data": [15,20]}]';


            $('#wdnchartLoaderLineChart').css('display', 'none');
            $('#wdnLinechartcontainerWithCompare').css('display', 'block');             


            Highcharts.chart('wdnLinechartcontainerWithCompare', {
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: 'Flow vs Pressure'
                    },
                    subtitle: {
                        text: 'Source: Ahmedabad Municipal Corporation'
                    },
                    xAxis: {
                        categories: result2['lables']
                    },
                    yAxis: {
                        title: {
                            text: ''
                        }
                },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: true
                            },
                            enableMouseTracking: false
                        }
                    },
                    series: seriesOutput
                    //series: result2['data']

                });
            });

           

    }
});