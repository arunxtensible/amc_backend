﻿$(document).ready(function () {
    var map;
    var ZoneList = [];
    var KML_Hash = new Map();
    var KMlPath_Hash = new Map();
    var Rendered_Marker = [];
    var Zone_Boundry = [];
    var ZoneConfig = new Map();
    var mcg = L.markerClusterGroup({
        chunkedLoading: true,
        //singleMarkerMode: true,
        spiderfyOnMaxZoom: false
    });
    $('#status').select2({
        theme: "flat"
    });
    AddRemoveZoneKML();

    var con1capacity = 0;
    var con1cdq = 0;
    var con1ldq = 0;
    var con2capacity = 0;
    var con2cdq = 0;
    var con2ldq = 0;
    var con3capacity = 0;
    var con3cdq = 0;
    var con3ldq = 0;
    var con4capacity = 0;
    var con4cdq = 0;
    var con4ldq = 0;
    var con5capacity = 0;
    var con5cdq = 0;
    var con5ldq = 0;
   
    var ArrLocation = [];
    var isClicked = false;
   
    //var KMlPath_Hash = new Map();  //Defined already at line no 5 - Prashant
   
   


    RenderMap();
    function RenderMap() {
        var localOsmMap = L.tileLayer(/*"../maps/OSM/riysdh_OMM/{z}/os_{x}_{y}_{z}.png"*/ "E:/Projects/riyadh/OSM/riysdh_OMM/{z}/os_{x}_{y}_{z}.png", {
            maxZoom: 30
            , subdomains: ["mt0", "mt1", "mt2", "mt3"]
        }),
            ////GOOGLE EARTH HYB GOOGLE EARTH HYB GOOGLE EARTH HYB
            gooleEarthHyBrid = L.tileLayer("http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}", {
                maxZoom: 21,
                subdomains: ["mt0", "mt1", "mt2", "mt3"]
            }),
            ////GOOGLE EARTH STREET //GOOGLE EARTH STREET //GOOGLE EARTH STREET
            googleMap = L.tileLayer("http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}", {
                maxZoom: 30,
                subdomains: ["mt0", "mt1", "mt2", "mt3"]
            })
            , mapDefaultView = { loc: [], zoom: 0 };
        //mapDefaultView.loc = [LatLong[0], LatLong[1]];
        mapDefaultView.loc = ['23.01302279984001', '72.61390684415191'];
        mapDefaultView.zoom = 12;

        map = L.map('map', {

            // maxZoom: maxZoom,
            // minZoom: minZoom,
            attributionControl: false,
            // maxBounds: [[maxBoundNorthEast[0], maxBoundNorthEast[1]], [maxBoundSouthWest[0], maxBoundSouthWest[1]]],
            layers: [gooleEarthHyBrid],
            zoomControl: false,
            doubleClickZoom: false,
            //fullscreenControl: true,

        }).setView(['23.01302279984001', '72.61390684415191'], 12);
        ;

        //map.addControl(new L.Control.Fullscreen({
        //    title: {
        //        'false': 'View Fullscreen',
        //        'true': 'Exit Fullscreen'
        //    }
        //}));

        if (true) //THIS WILL BE DECIDED DYNAMICALLY BY A BIT IN DB !!TO BE IMPLEMENTED
            L.control.scale({
                maxWidth: 100,
                position: "bottomleft",
                updateWhenIdle: true
            }).addTo(map);

        var baseMaps = {
            "Map": googleMap,
            "Hybrid": gooleEarthHyBrid,
            "OSM map(offline)": localOsmMap
        };

        L.control.layers(
            baseMaps, //MAP LAYER CONTROL
            null, //OTHER OVERLAY LAYERS
            { position: 'bottomleft' }  //ADDED LAYER PARAMETERS
        ).addTo(map);

        new L.Control.Zoom({
            position: "bottomleft"
        }).addTo(map);


        hereMarker = L.icon({
            iconUrl: "../assets/images/icon/hereMarker.png",
            //iconUrl: "../images/hereMarker.gif",
            iconSize: [38, 95], // size of the icon
            shadowSize: [50, 64], // size of the shadow
            iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
            shadowAnchor: [4, 62], // the same for the shadow
            popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
        });


        pumpstation = L.icon({
            iconUrl: "../script/Leaflet/images/BuildingIcon.png",

            iconSize: [38, 60], // size of the icon
            shadowSize: [50, 64], // size of the shadow
            iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
            shadowAnchor: [4, 62], // the same for the shadow
            popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
        });

        tank = L.icon({
            iconUrl: "../script/Leaflet/images/Tank.png",

            iconSize: [30, 30], // size of the icon
            shadowSize: [50, 64], // size of the shadow
            iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
            shadowAnchor: [4, 62], // the same for the shadow
            popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
        });


        if (true) {
            /*CUSTOM CONTROL CENTER MAP*/
            var resetMapBtn = L.control({ position: 'bottomleft' })
                , rstBtnDivObj;
            resetMapBtn.onAdd = function (map) {
                rstBtnDivObj = L.DomUtil.create('div', 'leaflet-control-layers leaflet-control');
                rstBtnDivObj.innerHTML = '<div id="resetMapView" title="Reset map" style="margin: 5px;font-size: 22px;cursor: pointer;"><i class="fa fa-compress"></i></div>';
                return rstBtnDivObj;
            };
            resetMapBtn.addTo(map);
            function ResetMapZoom() {
                if (mapDefaultView) {
                    map.setView(mapDefaultView.loc, mapDefaultView.zoom);
                    $.each(Zone_Boundry, function (key, poly) {
                        map.removeLayer(poly);
                        map.addLayer(poly);
                    });
                } else {
                    console.warn("Default view undefined. Setting view to map max bound.");
                    map.fitBounds(map.options.maxBounds);
                }
            }
            rstBtnDivObj.addEventListener("click", ResetMapZoom, false);
            /*CUSTOM CONTROL CENTER MAP*/
        }

        Polygon();
    }
    $('#sel_Ward').unbind().change(function () {
       
        const zoneId = $('#sel_Zone option:selected').val();
        const wardId = $('#sel_Ward option:selected').val();
        var wardName = $('#sel_Ward option:selected').text();

        if (zoneId != '0' && zoneId && wardId != '0' && wardId) {
            $.ajax({
                type: "GET",
                //url: "GetWNSMasterByzonenadWardId_zon_id_2_wardId_24.json",
                url: "/map/GetWDNDataByZoneIdAndLocationId",
                contentType: "application/json",
                dataType: "json",
                data: { zoneId: zoneId, wardId: wardId },
                async: false,
                success: function (response) {

                    $.each(response, function (index, element) {
                        console.log(response);
                        $('#sel_Location').empty();
                        $('#KPI_ForMapView').val(0);
                        if (response.length > 0) {
                            $('#sel_Location').append("<option selected>Select Location</option>");
                            $.each(response, function (index, element) {
                                $('#sel_Location').append($('<option>', {
                                    value: element.id,
                                    text: element.locationName
                                }));
                            });
                        } else {
                            $('#sel_Location').append('<option>Empty</option>');
                        }

                    });

                },
                failure: function (response) {
                    alert(response1);
                }
            });
        } else {
            alert("Please select Zone/Ward");
        }

        if (Rendered_Marker.length > 0) {
            $.each(Rendered_Marker, function (key, marker) {
                mcg.removeLayer(marker);
            });
        }

        var type = $('#MapType option:selected').text();
        if (type == "---All---") {
            clearPins();
            RenderMarkerbyward(wardId);
            RenderMarkerForWDSZone(zoneId);

        }
        else if (type == "WDN") {
            clearPins();
            RenderMarkerbyward(wardId);
        }
        
    });

    function RenderMarkerbyward(wardId) {
        $.ajax({
            type: "GET",
            //url: "GetNetworkByWardId_wardid_24.json",
            url: "/map/GetNetworkByWardId",
            //contentType: "application/json",
            //dataType: "json",
            data: {
                wardId: wardId
            },
            success: function (response) {
                console.log(response);
                clearPins();
                if (response.length > 0) {
                    $.each(response, function (index, element) {

                        var tmpString = element.coordinate;
                        
                        ArrLocation.push(element.name);
                        hash.set(element.name, element.coordinate);
                        var ldr = moment(element.ldr).format("DD/MM/YYYY HH:mm");
                       

                        if (tmpString !== null) {

                            var dyndetails =
                                '<div class="" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 1020 !important;">'
                                + '<div class="modal-dialog" role="document">'
                                + '<div class="modal-content"  id="' + element.id + '" style="width:475px;background-color:white;color:white;border-radius:3%; border: 1px solid rgba(255, 249, 249, 0.71);height:  auto;    CURSOR: POINTER; margin-left:-130px">'
                                + '<div class="">'
                                + '<table id="tblDev" width="100%" class="tblcolor" style="color:black;" cellpadding="0"; cellspacing="0";>'
                                + '<tbody>'
                                + '<tr style="text-align-last: center;">'
                                + '<td colspan="4" class="clsthead">' + element.name + '</td>'
                                + '</tr>'
                                + '<tr style="text-align-last: left;">'
                                + '<td style="width:3000px!important">ID</td>'
                                + '<td style="width:2000px!important">: ' + element.id + ' </td>'
                                + '<td style="width:3000px!important">Parent WDS</td>'
                                + '<td style="width:2000px!important">: ' + element.wdsname + '</td>'
                                + '</tr>'

                                + '<tr style="text-align-last: left;">'
                                + '<td style="width:3000px!important">Diameter (mm)</td>'
                                + '<td style="width:2000px!important">: ' + element.lineSize + ' </td>'
                                + '<td style="width:3000px!important">Totaliser (m3)</td>'
                                + '<td style="width:2000px!important">: ' + element.totaliser.toFixed(2) + ' </td>'
                                + '</tr>'

                                + '<tr style="text-align-last: left;">'
                                + '<td style="width:3000px!important">Reverse Totaliser(m3)</td>'
                                + '<td style="width:2000px!important">: ' + element.totaliser2.toFixed(2) + ' </td>'
                                + '<td style="width:3000px!important">Flow (m3/hr)</td>'
                                + '<td style="width:2000px!important">: ' + element.flow.toFixed(2) + ' </td>'
                                + '</tr>'

                                + '<tr style="text-align-last: left;">'
                                + '<td style="width:3000px!important">CDQ (m3)</td>'
                                + '<td style="width:2000px!important">: ' + element.cdq.toFixed(2) + ' </td>'
                                + '<td style="width:3000px!important">LDQ (m3)</td>'
                                + '<td style="width:2000px!important">: ' + element.ldq.toFixed(2) + ' </td>'
                                + '</tr>'

                                + '<tr style="text-align-last: left;">'
                                + '<td style="width:3000px!important">Reverse CDQ (m3)</td>'
                                + '<td style="width:2000px!important">: ' + element.negative_CDQ.toFixed(2) + ' </td>'
                                + '<td style="width:3000px!important">Reverse LDQ (m3)</td>'
                                + '<td style="width:2000px!important">: ' + element.negative_LDQ.toFixed(2) + '</td>'
                                + '</tr>'

                                + '<tr style="text-align-last: left;">'
                                + '<td style="text-align-last: left;width:1000px!important">Pressure (PSI)</td>'
                                + '<td style="width:2000px!important">: ' + element.pressure.toFixed(2) + ' </td>'
                                + '<td style="text-align-last: left;width:1000px!important">Battery Voltage(VDC)</td>'
                                + '<td style="width:2000px!important">: ' + element.batteryvoltage.toFixed(2) + '</td>'
                                + '</tr>'

                                + '<tr style="text-align-last: left;">'
                                + '<td style="width:3000px!important">Panel Status</td>'
                                + '<td style="width:2000px!important">: ' + element.panelStatus + ' </td>'
                                + '<td style="width:3000px!important">MainSupply Status</td>'
                                + '<td style="width:2000px!important">: ' + element.mainSupplyStatus + '</td>'
                                + '</tr>'

                                + '<tr style="text-align-last: left;">'
                                + '<td style="width:3000px!important">Last Updated Time</td>'
                                + '<td style="width:3000px!important">: ' + ldr + ' </td>'
                                + '<td></td>'
                                + '<td></td>'
                                + '</tr>'
                                + '</tbody>'
                                + '</table>'
                                + '</div>';
                            + '</div>';

                            $.each(tmpString.split(/[[\]]{1,2}/), function (key, strcoordinate) {
                                //if (strcoordinate.trim(), ",") === ",") {
                                //    return;
                                //}
                                var tempArr = [];
                                var latlon = strcoordinate.split(",");
                                if (isNaN(latlon[0]) || isNaN(latlon[1])) {
                                    return;
                                }
                                //if (element.id>187) {
                                //    var marker_icon = `<div class="${(function () { if (element.devStatus == 1) return "pinGREEN"; else return "pinRED"; }).call(this)} bounce"></div>`
                                //} else {
                                //    var marker_icon = `<div class="${(function () { if (element.devStatus == 1) return "pinRED"; else return "pinGREEN"; }).call(this)} bounce"></div>`
                                //}
                              //  var marker_icon = `<div class="${(function () { if (element.isInstall = 0) return "pinRED"; else return "pinGREEN"; }).call(this)} bounce"></div>`
                                var marker_icon = `<div class="pinGREEN bounce"></div>`;
                                var icon1 = L.divIcon({
                                    html: marker_icon,
                                    // Specify a class name we can refer to in CSS.
                                    className: 'image-icon',
                                    // Set a markers width and height.
                                    iconSize: [52, 52]
                                });

                                tempArr.push(parseFloat(latlon[0].trim()), parseFloat(latlon[1].trim()));

                                marker = L.marker(new L.LatLng(tempArr[0], tempArr[1]), { icon: icon1, name: element.name, id: element.id })
                                    .bindPopup(dyndetails, { closeButton: false });

                                Rendered_Marker.push(marker);
                                marker.bindTooltip('<div  class="txtColor" style="width:auto;line-height:16px;"><label class="lblColor" style="margin: 6px 4px 4px 4px;" >' + element.name + '</label></div>', { permanent: true, opacity: 1, position: 'top', devStatus: element.devStatus }).openTooltip();
                                marker.on('click', function (e) {
                                    //if (!isClicked) {


                                        $.ajax({
                                            type: "GET",
                                            url: "/map/GetNetworkById",
                                            async: false,
                                            data: {
                                                id: e.sourceTarget.options.id //element.id//
                                            },
                                            success: function (response) {
                                                console.log(response);
                                                const element = response[0];

                                               
                                                var ldr2 = moment(element.ldr).format("DD/MM/YYYY HH:mm");
                                                const dyndt =
                                                    '<div class="" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 1020 !important;">'
                                                    + '<div class="modal-dialog" role="document">'
                                                    + '<div class="modal-content"  id="' + element.id + '" style="width:500px;background-color:white;color:white;border-radius:3%; border: 3px solid rgba(255, 249, 249, 0.71);height:  auto;    CURSOR: POINTER;">'
                                                    + '<div class="">'
                                                    + '<table id="tblDev" width="100%" class="tblcolor" style="color:black;" cellpadding="0"; cellspacing="0";>'
                                                    + '<tbody>'
                                                    + '<tr style="text-align-last: center;">'
                                                    + '<td colspan="4" class="clsthead">' + element.name + '</td>'
                                                    + '</tr>'
                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">ID</td>'
                                                    + '<td style="width:1000px!important">: ' + element.id + ' </td>'
                                                    + '<td style="width:4500px!important">Parent WDS</td>'
                                                    + '<td style="width:1000px!important">: ' + element.wdsname + '</td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Diameter (mm)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.lineSize + ' </td>'
                                                    + '<td style="width:4500px!important">Totaliser (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.totaliser.toFixed(2) + ' </td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Reverse Totaliser(m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.totaliser2.toFixed(2) + ' </td>'
                                                    + '<td style="width:4500px!important">Flow (m3/hr)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.flow.toFixed(2) + ' </td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">CDQ (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.cdq.toFixed(2) + ' </td>'
                                                    + '<td style="width:4500px!important">LDQ (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.ldq.toFixed(2) + ' </td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Reverse CDQ (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.negative_CDQ.toFixed(2) + ' </td>'
                                                    + '<td style="width:4500px!important">Reverse LDQ (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.negative_LDQ.toFixed(2) + '</td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Pressure (PSI)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.pressure.toFixed(2) + ' </td>'
                                                    + '<td style="width:4500px!important">Battery Voltage(VDC)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.batteryvoltage.toFixed(2) + '</td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Panel Status</td>'
                                                    + '<td style="width:1000px!important">: ' + element.panelStatus + ' </td>'
                                                    + '<td style="width:4500px!important">MainSupply Status</td>'
                                                    + '<td style="width:1000px!important">: ' + element.mainSupplyStatus + '</td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Last Updated Time</td>'
                                                    + '<td  colspan="3" style="width:1000px!important">: ' + ldr2 + ' </td>'

                                                    + '</tr>'
                                                    + '</tbody>'
                                                    + '</table>'
                                                    + '</div>';
                                                + '</div>';

                                                e.sourceTarget._popup.setContent(dyndt);

                                            }
                                        });
                                        this.openPopup();
                                        $(".leaflet-container a.leaflet-popup-close-button").css("display", "block");
                                    //}
                                })
                                    .on('mouseout', function (e) {
                                        if (!isClicked) {
                                            this.closePopup();
                                        }
                                    })
                                    .on('mouseover', function () {
                                        //isClicked = true
                                        //this.openPopup()
                                        //$('#' + element.pumpStationName).on('click', function () {
                                        //    window.location.href = "/Dashboard/PumpDashboard/" + element.id;
                                        //})
                                    });


                                mcg.addLayer(marker);
                            });


                        }
                    });

                    map.addLayer(mcg);
                    $("#getmeter").autocomplete({
                        source: ArrLocation
                    });

                } 

            },
            failure: function (response) {
                alert(response);
            }
        });

    }

    function RenderWDNMarkerbyZone(zoneId) {
        $.ajax({
            type: "GET",
            url: "/map/GetWDNNetworkByZoneId",
            data: {
                zoneId: zoneId
            },
            success: function (response) {
                console.log(response);
                
                if (response.length > 0) {
                   
                    $.each(response, function (index, element) {


                        var tmpString = element.coordinate;

                        ArrLocation.push(element.name);
                        hash.set(element.name, element.coordinate);
                        if (tmpString !== null) {

                           

                            $.each(tmpString.split(/[[\]]{1,2}/), function (key, strcoordinate) {
                                
                                var tempArr = [];
                                var latlon = strcoordinate.split(",");
                                if (isNaN(latlon[0]) || isNaN(latlon[1])) {
                                    return;
                                }
                                
                                var marker_icon = `<div class="pinGREEN bounce"></div>`;
                                var icon1 = L.divIcon({
                                    html: marker_icon,
                                    // Specify a class name we can refer to in CSS.
                                    className: 'image-icon',
                                    // Set a markers width and height.
                                    iconSize: [52, 52]
                                });

                                tempArr.push(parseFloat(latlon[0].trim()), parseFloat(latlon[1].trim()));

                                marker = L.marker(new L.LatLng(tempArr[0], tempArr[1]), { icon: icon1, name: element.name, id: element.id })
                                    .bindPopup('', { closeButton: false });

                                Rendered_Marker.push(marker);
                                marker.bindTooltip('<div  class="txtColor" style="width:auto;line-height:16px;"><label class="lblColor" style="margin: 6px 4px 4px 4px;" >' + element.name + '- WDN </label></div>', { permanent: true, opacity: 1, position: 'top', devStatus: '' }).openTooltip();
                                marker.on('click' , function (e) {
                                    //if (!isClicked) {


                                        $.ajax({
                                            type: "GET",
                                            url: "/map/GetNetworkById",
                                            async: false,
                                            data: {
                                                id: e.sourceTarget.options.id //element.id//
                                            },
                                            success: function (response) {
                                                console.log(response);
                                                const element = response[0];

                                               
                                                var ldr_2 = moment(element.ldr).format("DD/MM/YYYY HH:mm");
                                                const dyndt =
                                                    '<div class="" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 1020 !important;">'
                                                    + '<div class="modal-dialog" role="document">'
                                                    + '<div class="modal-content"  id="' + element.id + '" style="width:500px;background-color:white;color:white;border-radius:3%; border: 3px solid rgba(255, 249, 249, 0.71);height:  auto;    CURSOR: POINTER;">'
                                                    + '<div class="">'
                                                    + '<table id="tblDev" width="100%" class="tblcolor" style="color:black;" cellpadding="0"; cellspacing="0";>'
                                                    + '<tbody>'
                                                    + '<tr style="text-align-last: center;">'
                                                    + '<td colspan="4" class="clsthead">' + element.name + '</td>'
                                                    + '</tr>'
                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Site ID</td>'
                                                    + '<td style="width:1000px!important">: ' + element.id + ' </td>'
                                                    + '<td style="width:4500px!important">Parent WDS</td>'
                                                    + '<td style="width:1000px!important">: ' + element.wdsname + '</td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Diameter (mm)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.lineSize + ' </td>'
                                                    + '<td style="width:4500px!important">Totaliser (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.totaliser.toFixed(2) + ' </td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Reverse Totaliser(m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.totaliser2.toFixed(2) + ' </td>'
                                                    + '<td style="width:4500px!important">Flow (m3/hr)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.flow.toFixed(2) + ' </td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">CDQ (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.cdq.toFixed(2) + ' </td>'
                                                    + '<td style="width:4500px!important">LDQ (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.ldq.toFixed(2) + ' </td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Reverse CDQ (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.negative_CDQ.toFixed(2) + ' </td>'
                                                    + '<td style="width:4500px!important">Reverse LDQ (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.negative_LDQ.toFixed(2) + '</td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Pressure (PSI)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.pressure.toFixed(2) + ' </td>'
                                                    + '<td style="width:4500px!important">Battery Voltage(VDC)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.batteryvoltage.toFixed(2) + '</td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Panel Status</td>'
                                                    + '<td style="width:1000px!important">: ' + element.panelStatus + ' </td>'
                                                    + '<td style="width:4500px!important">MainSupply Status</td>'
                                                    + '<td style="width:1000px!important">: ' + element.mainSupplyStatus + '</td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Last Updated Time</td>'
                                                    + '<td  colspan="3" style="width:1000px!important">: ' + ldr_2 + ' </td>'

                                                    + '</tr>'
                                                    + '</tbody>'
                                                    + '</table>'
                                                    + '</div>';
                                                + '</div>';

                                                e.sourceTarget._popup.setContent(dyndt);

                                            }
                                        });
                                        this.openPopup();
                                        $(".leaflet-container a.leaflet-popup-close-button").css("display", "block");
                                    //}
                                })
                                    .on('mouseout', function (e) {
                                        if (!isClicked) {
                                            this.closePopup();
                                        }
                                    })
                                    .on('mouseover', function () {
                                        //isClicked = true
                                        //this.openPopup()
                                        //$('#' + element.pumpStationName).on('click', function () {
                                        //    window.location.href = "/Dashboard/PumpDashboard/" + element.id;
                                        //})
                                    });


                                mcg.addLayer(marker);
                            });


                        }
                        //if (tmpString !== null) {


                        //    $.each(tmpString.split(/[[\]]{1,2}/), function (key, strcoordinate) {

                        //        var tempArr = [];
                        //        var latlon = strcoordinate.split(",");
                        //        if (isNaN(latlon[0]) || isNaN(latlon[1])) {
                        //            return;
                        //        }

                        //        var dyndetails =
                        //            '<div class="" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 1020 !important;">'
                        //            + '<div class="modal-dialog" role="document">'
                        //            + '<div class="modal-content"  id="' + element.id + '" style="width:475px;background-color:white;color:white;border-radius:3%; border: 1px solid rgba(255, 249, 249, 0.71);height:  auto;    CURSOR: POINTER; margin-left:-130px">'
                        //            + '<div class="">'
                        //            + '<table id="tblDev" width="100%" class="tblcolor" style="color:black;" cellpadding="0"; cellspacing="0";>'
                        //            + '<tbody>'
                        //            + '<tr style="text-align-last: center;">'
                        //            + '<td colspan="4" class="clsthead">' + element.name + '</td>'
                        //            + '</tr>'
                        //            + '<tr style="text-align-last: left;">'
                        //            + '<td style="width:3000px!important">ID</td>'
                        //            + '<td style="width:2000px!important">: ' + element.id + ' </td>'
                        //            + '<td style="width:3000px!important">Name</td>'
                        //            + '<td style="width:2000px!important">: ' + element.name + '</td>'
                        //            + '</tr>'


                        //            + '</tbody>'
                        //            + '</table>'
                        //            + '</div>';
                        //        + '</div>';

                        //        //var marker_icon = `<div class="${(function () { if (element.isInstall = 0) return "pinRED"; else return "pinGREEN"; }).call(this)} bounce"></div>`
                        //        var marker_icon = `<div class="pinGREEN bounce"></div>`;
                        //        var icon1 = L.divIcon({
                        //            html: marker_icon,
                        //            // Specify a class name we can refer to in CSS.
                        //            className: 'image-icon',
                        //            // Set a markers width and height.
                        //            iconSize: [52, 52]
                        //        });

                        //        tempArr.push(parseFloat(latlon[0].trim()), parseFloat(latlon[1].trim()));

                        //        marker = L.marker(new L.LatLng(tempArr[0], tempArr[1]), { icon: icon1, name: element.name, id: element.id })
                        //            .bindPopup(dyndetails, { closeButton: false });

                        //        Rendered_Marker.push(marker);
                        //        marker.bindTooltip('<div  class="txtColor" style="width:auto;line-height:16px;"><label class="lblColor" style="margin: 6px 4px 4px 4px;" >' + element.name + ' -:WDN' + '</label></div>', { permanent: true, opacity: 1, position: 'top', devStatus: '' }).openTooltip();
                        //        marker.on('mouseover', function (e) {
                        //            $.ajax({
                        //                type: "GET",
                        //                url: "/map/GetNetworkById",
                        //                async: false,
                        //                data: {
                        //                    id: e.sourceTarget.options.id //element.id//
                        //                },
                        //                success: function (response) {
                        //                    console.log(response);
                        //                    const element = response[0];

                        //                    if (element.panelStatus == 1) {
                        //                        panelStatus = 'Door Closed';
                        //                    } else {
                        //                        panelStatus = 'Door Opened';
                        //                    }
                        //                    if (element.mainSupplyStatus == 1) {
                        //                        mainSupplyStatus = 'Main Power';
                        //                    } else {
                        //                        mainSupplyStatus = 'Battery';
                        //                    }
                        //                    var ldr2 = moment(element.ldr).format("DD/MM/YYYY HH:mm");
                        //                    const dyndt =
                        //                        '<div class="" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 1020 !important;">'
                        //                        + '<div class="modal-dialog" role="document">'
                        //                        + '<div class="modal-content"  id="' + element.id + '" style="width:500px;background-color:white;color:white;border-radius:3%; border: 3px solid rgba(255, 249, 249, 0.71);height:  auto;    CURSOR: POINTER;">'
                        //                        + '<div class="">'
                        //                        + '<table id="tblDev" width="100%" class="tblcolor" style="color:black;" cellpadding="0"; cellspacing="0";>'
                        //                        + '<tbody>'
                        //                        + '<tr style="text-align-last: center;">'
                        //                        + '<td colspan="4" class="clsthead">' + element.name + '</td>'
                        //                        + '</tr>'
                        //                        + '<tr style="text-align-last: left;">'
                        //                        + '<td style="width:4500px!important">ID</td>'
                        //                        + '<td style="width:1000px!important">: ' + element.id + ' </td>'
                        //                        + '<td style="width:4500px!important">Parent WDS</td>'
                        //                        + '<td style="width:1000px!important">: ' + element.wdsname + '</td>'
                        //                        + '</tr>'

                        //                        + '<tr style="text-align-last: left;">'
                        //                        + '<td style="width:4500px!important">Diameter (mm)</td>'
                        //                        + '<td style="width:1000px!important">: ' + element.lineSize + ' </td>'
                        //                        + '<td style="width:4500px!important">Totaliser (m3)</td>'
                        //                        + '<td style="width:1000px!important">: ' + element.totaliser.toFixed(2) + ' </td>'
                        //                        + '</tr>'

                        //                        + '<tr style="text-align-last: left;">'
                        //                        + '<td style="width:4500px!important">Reverse Totaliser(m3)</td>'
                        //                        + '<td style="width:1000px!important">: ' + element.totaliser2.toFixed(2) + ' </td>'
                        //                        + '<td style="width:4500px!important">Flow (m3/hr)</td>'
                        //                        + '<td style="width:1000px!important">: ' + element.flow.toFixed(2) + ' </td>'
                        //                        + '</tr>'

                        //                        + '<tr style="text-align-last: left;">'
                        //                        + '<td style="width:4500px!important">CDQ (m3)</td>'
                        //                        + '<td style="width:1000px!important">: ' + element.cdq.toFixed(2) + ' </td>'
                        //                        + '<td style="width:4500px!important">LDQ (m3)</td>'
                        //                        + '<td style="width:1000px!important">: ' + element.ldq.toFixed(2) + ' </td>'
                        //                        + '</tr>'

                        //                        + '<tr style="text-align-last: left;">'
                        //                        + '<td style="width:4500px!important">Reverse CDQ (m3)</td>'
                        //                        + '<td style="width:1000px!important">: ' + element.negative_CDQ.toFixed(2) + ' </td>'
                        //                        + '<td style="width:4500px!important">Reverse LDQ (m3)</td>'
                        //                        + '<td style="width:1000px!important">: ' + element.negative_LDQ.toFixed(2) + '</td>'
                        //                        + '</tr>'

                        //                        + '<tr style="text-align-last: left;">'
                        //                        + '<td style="width:4500px!important">Pressure (PSI)</td>'
                        //                        + '<td style="width:1000px!important">: ' + element.pressure.toFixed(2) + ' </td>'
                        //                        + '<td style="width:4500px!important">Battery Voltage(VDC)</td>'
                        //                        + '<td style="width:1000px!important">: ' + element.batteryvoltage.toFixed(2) + '</td>'
                        //                        + '</tr>'

                        //                        + '<tr style="text-align-last: left;">'
                        //                        + '<td style="width:4500px!important">Panel Status</td>'
                        //                        + '<td style="width:1000px!important">: ' + panelStatus + ' </td>'
                        //                        + '<td style="width:4500px!important">MainSupply Status</td>'
                        //                        + '<td style="width:1000px!important">: ' + mainSupplyStatus + '</td>'
                        //                        + '</tr>'

                        //                        + '<tr style="text-align-last: left;">'
                        //                        + '<td style="width:4500px!important">Last Updated Time</td>'
                        //                        + '<td  colspan="3" style="width:1000px!important">: ' + ldr2 + ' </td>'

                        //                        + '</tr>'
                        //                        + '</tbody>'
                        //                        + '</table>'
                        //                        + '</div>';
                        //                    + '</div>';

                        //                    e.sourceTarget._popup.setContent(dyndt);

                        //                }
                        //            });
                        //            //$.ajax({
                        //            //    type: "GET",
                        //            //    url: "/map/GetWDSDataByWDSName",
                        //            //    //contentType: "application/json",
                        //            //    //dataType: "json",
                        //            //    async: false,
                        //            //    data: {
                        //            //        WDSName: element.WDSName
                        //            //    },
                        //            //    success: function (response) {
                        //            //        console.log(response);
                        //            //        const element = response[0];


                        //            //        const dyndt1 =
                        //            //            '<div class="" id="myModalwdn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 1020 !important;">'
                        //            //            + '<div class="modal-dialog" role="document">'
                        //            //            + '<div class="modal-content"  id="' + element.id + '" style="width:475px;background-color:white;color:white;border-radius:3%; border: 1px solid rgba(255, 249, 249, 0.71);height:  auto;    CURSOR: POINTER; margin-left:-130px">'
                        //            //            + '<div class="">'
                        //            //            + '<table id="tblDev" width="100%" class="tblcolor" style="color:black;" cellpadding="0"; cellspacing="0";>'
                        //            //            + '<tbody>'
                        //            //            + '<tr style="text-align-last: center;">'
                        //            //            + '<td colspan="4" class="clsthead">' + element.name + '</td>'
                        //            //            + '</tr>'
                        //            //            + '<tr style="text-align-last: left;">'
                        //            //            + '<td style="width:3000px!important">ID</td>'
                        //            //            + '<td style="width:2000px!important">: ' + element.id + ' </td>'
                        //            //            + '<td style="width:3000px!important">WDS Name</td>'
                        //            //            + '<td style="width:2000px!important">: ' + element.name + '</td>'
                        //            //            + '</tr>'
                                                                                             

                        //            //            + '</tbody>'
                        //            //            + '</table>'
                        //            //            + '</div>';
                        //            //        + '</div>';

                        //            //        e.sourceTarget._popup.setContent(dyndt1);

                        //            //    }
                        //            //});
                        //            this.openPopup();
                        //            $(".leaflet-container a.leaflet-popup-close-button").css("display", "block");

                        //        })
                        //            .on('mouseout', function (e) {
                        //                if (!isClicked) {
                        //                    this.closePopup();
                        //                }
                        //            });

                        //        mcg.addLayer(marker);
                        //    });


                        //}


                    });
                    map.addLayer(mcg);
                    $("#getmeter").autocomplete({
                        source: ArrLocation
                    });

                } 

            },
            failure: function (response) {
                alert(response);
            }
        });

    }

    function Polygon() {
        $.ajax({
            type: "GET",
            //url: "GetZoneLine.json",
            url: "/map/GetZoneLine",
            contentType: "application/json",
            dataType: "json",

            success: function (response) {
                console.log(response);

                $.each(response, function (key, line) {
                    $.each(line.setView.split(/[[\]]{1,2}/), function (key, strcoordinate) {
                        var tempArr = [];
                        var latlon = strcoordinate.split(",");
                        if (isNaN(latlon[0]) || isNaN(latlon[1])) {
                            return;
                        }
                        tempArr.push(parseFloat(latlon[0].trim()), parseFloat(latlon[1].trim()));

                        ZoneConfig.set(line.line, {
                            view: [tempArr[0], tempArr[1]],
                            zoom: line.zoomLevel
                        });
                    });

                    var strCordinates;
                    $.each(line.coordinates.split(/[[\]]{1,2}/), function (key, strcoordinate) {
                        const chunk = (str) => {
                            return str.split(',')
                                .map(parseFloat)
                                .reduce((acc, val, i, arr) =>
                                    (i % 2) ? acc
                                        : [...acc, arr.slice(i, i + 2)]
                                    , [])
                        }
                        strCordinates = chunk(strcoordinate);
                    });
                    //var bounds = [[53.912257, 27.581640], [53.902257, 27.561640]];
                    var initialStyle = {
                        color: line.color,
                        weight: line.weight,
                        opacity: line.opacity,
                        smoothFactor: 1,
                        name: line.line,
                        id: line.id
                    }
                    //var poly = L.polygon([
                    //    [51.509, -0.08],
                    //    [51.503, -0.06],
                    //    [51.51, -0.047]
                    //]).addTo(map);
                    var rect = L.polygon(strCordinates, initialStyle).on('click', function (e) {
                        // There event is event object
                        // there e.type === 'click'
                        // there e.lanlng === L.LatLng on map
                        // there e.target.getLatLngs() - your rectangle coordinates
                        // but e.target !== rect

                    }).addTo(map);
                    Zone_Boundry.push(rect);

                    rect.bindTooltip(`<div  class="txtColor" style="width:auto;line-height:16px;"><label class="lblColor" style="margin: 6px 4px 4px 4px;" >${line.line}</label></div>`, { opacity: 1, position: 'top' });
                    rect.on('mouseover', function (e) {
                        var layer = e.target;

                        layer.setStyle({
                            color: 'white',
                            opacity: 2,
                            weight: 7,
                            name: line.line
                        });
                    });
                    rect.on('mouseout', function () {
                        this.setStyle(initialStyle)
                    });
                });

            },
            failure: function (response) {
                alert(response1);
            }
        });
    }

    $('#sel_Zone').unbind().change(function () {
        var Zone = $('#sel_Zone option:selected').val();
        var ZoneName = $('#sel_Zone option:selected').text();


        wardname(Zone);

        var kmllayer;
        var type = $('#MapType option:selected').text();
        if (type == "---All---") {
            clearPins();
            RenderWDNMarkerbyZone(Zone);
                RenderMarkerForWDSZone(Zone);
           
        }
        else if (type == "WDS") {     
            clearPins();
                RenderMarkerForWDSZone(Zone);            
        }
        else if (type == "WDN") {
            clearPins();
            RenderWDNMarkerbyZone(Zone);
        }
        if (Zone != '0') {


            var config = ZoneConfig.get(ZoneName);
            map.setView(config.view, config.zoom);
            if (Zone_Boundry.length > 0) {
                $.each(Zone_Boundry, function (key, poly) {
                    console.log(this);
                    if (ZoneName !== poly.options.name) {
                        map.removeLayer(poly);
                    } else {
                        map.addLayer(poly);
                    }
                })
            }
            if (Rendered_Marker.length > 0) {
                $.each(Rendered_Marker, function (key, marker) {
                    mcg.removeLayer(marker);
                });
            }
            ///////////////////////
            console.log("111 Zone " + Zone );
            console.log("111 " + KMlPath_Hash.get(parseInt(Zone)));
            kmllayer = new L.KML(`/assets/js/${KMlPath_Hash.get(parseInt(Zone))}`, {
                async: true,
                suppressInfoWindows: true,
                preserveViewport: false,
                map: map
            });
            console.log(kmllayer);
            kmllayer.on("loaded", function (e) {
                map.fitBounds(e.target.getBounds())
            });

            if (KML_Hash.size > 0) {
                for (var i = 0; i < ZoneList.length; i++) {
                    if (KML_Hash.get(ZoneList[i])) {
                        var K_Layer = KML_Hash.get(ZoneList[i])
                        KML_Hash.delete(ZoneList[i]);
                        map.removeLayer(K_Layer);
                    }
                }
            }
            KML_Hash.set(parseInt(Zone), kmllayer);
            map.addLayer(kmllayer);
            /////////////////////////
        }

       

    })
    $('#sel_Zone1').unbind().change(function () {
        var Zone = $('#sel_Zone1 option:selected').val();
        var ZoneName = $('#sel_Zone1 option:selected').text()
        wardname(Zone);
    })

    $('#sel_Zone1wdnline').unbind().change(function () {
        var Zone = $('#sel_Zone1wdnline option:selected').val();
        var ZoneName = $('#sel_Zone1wdnline option:selected').text()

        wardname(Zone);
    });
    function searchMarker() {
        if (hash.size != 0) {
            var meterpos = hash.get($('#sel_Location option:selected').text());
            //var meterpos = hash.get($('#getmeter').val());
            if (meterpos === undefined || meterpos === null) {
                //swal({
                //    title: "Site has no/invalid location details",
                //    type: "error"
                //})
                return;
            }

            var lat = meterpos.split(",")[0];
            var lon = meterpos.split(",")[1];

            if (lat !== undefined && lon !== undefined) {
                map.setView([meterpos.split(",")[0], meterpos.split(",")[1]],

                    `${(function () {
                        if ($("#mtrzoom").prop("checked")) {
                            return 18;
                        } else {
                            return 17;//map.getZoom();
                        }
                    }).call(this)}`
                    , {
                        "animate": true,
                        "pan": {
                            "duration": 2
                        }
                    }
                )
            }
            else {
                Swal.fire(
                    "Site has no/invalid location details",
                    "error"
                );
            }

            var highlightMarker = L.marker([lat, lon] /**/, {
                icon: hereMarker,
                riseOnHover: true
            });
            highlightMarker.addTo(map).on("click", function (e) {
                map.removeLayer(highlightMarker);
            }).on("mouseover", function (e) {
                map.removeLayer(highlightMarker);
            });

            setTimeout(function () {
                map.removeLayer(highlightMarker);
            }, 10000);
        }
    }
    
    function AddRemoveZoneKML() {
        $.ajax({
            type: "GET",
            url: "/map/getZoneList",
            contentType: "application/json",
            dataType: "json",

            success: function (response) {
                console.log(response);

                $.each(response, function (key, zone) {
                    console.dir(zone);
                    ZoneList.push(
                        zone.zoneId
                    );
                    KMlPath_Hash.set(zone.zoneId, zone.kmlPath == null ? 'NA' : zone.kmlPath);
                })
                console.log(ZoneList);
            },
            failure: function (response) {
                alert(response1);
            }
        });
    }
    var hash = new Map();
    $('#getmeter').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            searchMarker();
        }
    });
    $("#sel_Location").change(function () {
        searchMarker();
    });
    $("#ddlWDSLocation").change(function () {
        searchMarkerWDS();
    });

    function searchMarkerWDS() {
        
        if (hash.size != 0) {
            var meterpos = hash.get($('#ddlWDSLocation option:selected').text());
            //var meterpos = hash.get($('#getmeter').val());
            if (meterpos === undefined || meterpos === null) {
                //swal({
                //    title: "Site has no/invalid location details",
                //    type: "error"
                //})
                return;
            }
            var lat = meterpos.split(",")[0];
            var lon = meterpos.split(",")[1];
            
            if (lat !== undefined && lon !== undefined) {
                map.setView([meterpos.split(",")[0], meterpos.split(",")[1]],

                    `${(function () {
                        if ($("#mtrzoom").prop("checked")) {
                            return 18;
                        } else {
                            return 17;//map.getZoom();
                        }
                    }).call(this)}`
                    , {
                        "animate": true,
                        "pan": {
                            "duration": 2
                        }
                    }
                )
            }
            else {
                Swal.fire(
                    "Site has no/invalid location details",
                    "error"
                );
            }

            var highlightMarker = L.marker([lat, lon] /**/, {
                icon: hereMarker,
                riseOnHover: true
            });
            highlightMarker.addTo(map).on("click", function (e) {
                map.removeLayer(highlightMarker);
            }).on("mouseover", function (e) {
                map.removeLayer(highlightMarker);
            });

            setTimeout(function () {
                map.removeLayer(highlightMarker);
            }, 10000);
        }
    }

    $("#KPI_ForMapView").change(function () {
        var kpi = $('#KPI_ForMapView :selected').text();
        var zone = $('#sel_Zone :selected').text();
        var ward = $('#sel_Ward :selected').text();
        var location = $('#sel_Location :selected').text();

        var type = $('#MapType option:selected').text();
        if (type == "---All---") {
            clearPins();
            RenderMarkerForWDSZone($('#sel_Zone :selected').val());
            RenderMarkerbyKPI(kpi, zone, ward, location);

        }
        else if (type == "WDN") {
            clearPins();
            RenderMarkerbyKPI(kpi, zone, ward, location);
        }
       
    });

    function RenderMarkerbyKPI(kpi, zone, ward, location) {
  
        $.ajax({
            type: "GET",
            //url: "GetNetworkByWardId_wardid_24.json",
            url: "/map/GetNetworkByKPI",
            //contentType: "application/json",
            //dataType: "json",
            data: {
                kpi: kpi, zone: zone, ward: ward, location: location
            },
            success: function (response) {
                console.log(response);
                
                if (response.length > 0) {
                    
                    $.each(response, function (index, element) {

                        var tmpString = element.coordinate;
                        ArrLocation.push(element.name);
                        hash.set(element.name, element.coordinate);
                       
                        if (tmpString !== null) {


                            $.each(tmpString.split(/[[\]]{1,2}/), function (key, strcoordinate) {
                               
                                //if (strcoordinate.trim(), ",") === ",") {
                                //    return;
                                //}
                                var tempArr = [];
                                var latlon = strcoordinate.split(",");
                                if (isNaN(latlon[0]) || isNaN(latlon[1])) {
                                    return;
                                }
                                
                               
                                var marker_icon = `<div class="${(function () { if (element.isInstall = 0) return "pinRED"; else return "pinGREEN"; }).call(this)} bounce"></div>`
                                var icon1 = L.divIcon({
                                    html: marker_icon,
                                    // Specify a class name we can refer to in CSS.
                                    className: 'image-icon',
                                    // Set a markers width and height.
                                    iconSize: [52, 52]
                                });
                                
                                tempArr.push(parseFloat(latlon[0].trim()), parseFloat(latlon[1].trim()));
                                
                                marker = L.marker(new L.LatLng(tempArr[0], tempArr[1]), { icon: icon1, name: element.name, id: element.srno })
                                 .bindPopup('', { closeButton: false });
                                
                                Rendered_Marker.push(marker);
                                marker.bindTooltip('<div  class="txtColor" style="width:auto;line-height:16px;"><label class="lblColor" style="margin: 6px 4px 4px 4px;" >' + element.name + ' -: ' + kpi + '  (' + element.kpi+')'+ '</label></div>', { permanent: true, opacity: 1, position: 'top', devStatus: element.devStatus }).openTooltip();
                                marker.on('click', function (e) {
                                   // if (!isClicked) {


                                        $.ajax({
                                            type: "GET",
                                            url: "/map/GetNetworkById",
                                            async: false,
                                            data: {
                                                id: e.sourceTarget.options.id //element.id//
                                            },
                                            success: function (response) {
                                                console.log(response);
                                                const element = response[0];


                                                var ldr_2 = moment(element.ldr).format("DD/MM/YYYY HH:mm");
                                                const dyndt =
                                                    '<div class="" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 1020 !important;">'
                                                    + '<div class="modal-dialog" role="document">'
                                                    + '<div class="modal-content"  id="' + element.id + '" style="width:500px;background-color:white;color:white;border-radius:3%; border: 3px solid rgba(255, 249, 249, 0.71);height:  auto;    CURSOR: POINTER;">'
                                                    + '<div class="">'
                                                    + '<table id="tblDev" width="100%" class="tblcolor" style="color:black;" cellpadding="0"; cellspacing="0";>'
                                                    + '<tbody>'
                                                    + '<tr style="text-align-last: center;">'
                                                    + '<td colspan="4" class="clsthead">' + element.name + '</td>'
                                                    + '</tr>'
                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Site ID</td>'
                                                    + '<td style="width:1000px!important">: ' + element.id + ' </td>'
                                                    + '<td style="width:4500px!important">Parent WDS</td>'
                                                    + '<td style="width:1000px!important">: ' + element.wdsname + '</td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Diameter (mm)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.lineSize + ' </td>'
                                                    + '<td style="width:4500px!important">Totaliser (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.totaliser.toFixed(2) + ' </td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Reverse Totaliser(m3)</td>'
                                                    + '<td style="width:1000px!important">: ' +element.totaliser2.toFixed(2) + ' </td>'
                                                    + '<td style="width:4500px!important">Flow (m3/hr)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.flow.toFixed(2) + ' </td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">CDQ (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.cdq.toFixed(2) + ' </td>'
                                                    + '<td style="width:4500px!important">LDQ (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.ldq.toFixed(2) + ' </td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Reverse CDQ (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.negative_CDQ.toFixed(2)+ ' </td>'
                                                    + '<td style="width:4500px!important">Reverse LDQ (m3)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.negative_LDQ.toFixed(2) + '</td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Pressure (PSI)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.pressure.toFixed(2) + ' </td>'
                                                    + '<td style="width:4500px!important">Battery Voltage(VDC)</td>'
                                                    + '<td style="width:1000px!important">: ' + element.batteryvoltage.toFixed(2) + '</td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Panel Status</td>'
                                                    + '<td style="width:1000px!important">: ' + element.panelStatus + ' </td>'
                                                    + '<td style="width:4500px!important">MainSupply Status</td>'
                                                    + '<td style="width:1000px!important">: ' + element.mainSupplyStatus + '</td>'
                                                    + '</tr>'

                                                    + '<tr style="text-align-last: left;">'
                                                    + '<td style="width:4500px!important">Last Updated Time</td>'
                                                    + '<td  colspan="3" style="width:1000px!important">: ' + ldr_2 + ' </td>'

                                                    + '</tr>'
                                                    + '</tbody>'
                                                    + '</table>'
                                                    + '</div>';
                                                + '</div>';

                                                e.sourceTarget._popup.setContent(dyndt);

                                            }
                                        });
                                        this.openPopup();
                                        $(".leaflet-container a.leaflet-popup-close-button").css("display", "block");
                                   // }
                                })
                                    .on('mouseout', function (e) {
                                        if (!isClicked) {
                                            this.closePopup();
                                        }
                                    })
                                    .on('mouseoverclick', function () {
                                        //isClicked = true
                                        //this.openPopup()
                                        //$('#' + element.pumpStationName).on('click', function () {
                                        //    window.location.href = "/Dashboard/PumpDashboard/" + element.id;
                                        //})
                                    });
                                

                                mcg.addLayer(marker);
                            });


                        } 
                    });

                    map.addLayer(mcg);
                    $("#getmeter").autocomplete({
                        source: ArrLocation
                    });

                } 

            },
            failure: function (response) {
                alert(response);
            }
        });
       
    }

    //WDS bindin on map
   
    function RenderMarkerForWDSZone(zoneId) {
        $.ajax({
            type: "GET",
            url: "/map/GetWDSDataByZoneId",
            //url: "GetNetworkByWardId_wardid_24.json",
            //url: "/assets/js/wds_json.json",
            //contentType: "application/json",
            //dataType: "json",
            data: {
                zoneId: zoneId
            },
            success: function (response) {
                console.log(response);
                
               
                   
               
                $.each(response, function (index, element) {
                  
                   
                    var tmpString = element.coordinate;
                    
                    ArrLocation.push(element.WDSName);
                    hash.set(element.WDSName, element.coordinate);

                    if (tmpString !== null) {


                        $.each(tmpString.split(/[[\]]{1,2}/), function (key, strcoordinate) {

                            var tempArr = [];
                            var latlon = strcoordinate.split(",");
                            if (isNaN(latlon[0]) || isNaN(latlon[1])) {
                                return;
                            }

                            var dyndetails =
                                '<div class="" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 1020 !important;">'
                                + '<div class="modal-dialog" role="document">'
                                + '<div class="modal-content"  id="' + element.WDSId + '" style="width:475px;background-color:white;color:white;border-radius:3%; border: 1px solid rgba(255, 249, 249, 0.71);height:  auto;    CURSOR: POINTER; margin-left:-130px">'
                                + '<div class="">'
                                + '<table id="tblDev" width="100%" class="tblcolor" style="color:black;" cellpadding="0"; cellspacing="0";>'
                                + '<tbody>'
                                + '<tr style="text-align-last: center;">'
                                + '<td colspan="4" class="clsthead">' + element.WDSName + '</td>'
                                + '</tr>'
                                + '<tr style="text-align-last: left;">'
                                + '<td style="width:3000px!important">ID</td>'
                                + '<td style="width:2000px!important">: ' + element.WDSId + ' </td>'
                                + '<td style="width:3000px!important">Parent WDS</td>'
                                + '<td style="width:2000px!important">: ' + element.wdsname + '</td>'
                                + '</tr>'                             

                                
                                + '</tbody>'
                                + '</table>'
                                + '</div>';
                            + '</div>';

                            //var marker_icon = `<div class="${(function () { if (element.isInstall = 0) return "pinRED"; else return "pinGREEN"; }).call(this)} bounce"></div>`
                            var marker_icon = `<div class="pinRED bounce"></div>`;
                            var icon1 = L.divIcon({
                                html: marker_icon,
                                // Specify a class name we can refer to in CSS.
                                className: 'image-icon',
                                // Set a markers width and height.
                                iconSize: [52, 52]
                            });

                            tempArr.push(parseFloat(latlon[0].trim()), parseFloat(latlon[1].trim()));
                            
                            marker = L.marker(new L.LatLng(tempArr[0], tempArr[1]), { icon: icon1, name: element.WDSName, id: element.WDSId })
                                .bindPopup(dyndetails, { closeButton: false });

                            Rendered_Marker.push(marker);
                            marker.bindTooltip('<div  class="txtColor" style="width:auto;line-height:16px;"><label class="lblColor" style="margin: 6px 4px 4px 4px;" >' + element.WDSName + ' -:WDS' + '</label></div>', { permanent: true, opacity: 1, position: 'top', devStatus: '' }).openTooltip();
                            marker.on('click', function (e) {
                                $.ajax({
                                    type: "GET",
                                    url: "/map/GetWDSDataByWDSName",
                                    //contentType: "application/json",
                                    //dataType: "json",
                                    async: false,
                                    data: {
                                        WDSName: element.WDSName
                                    },
                                    success: function (response) {
                                        console.log(response);
                                        const element = response[0];

                                        
                                        const dyndt =
                                            '<div class="" id="myModalwds" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 1020 !important;">'
                                            + '<div class="modal-dialog" role="document">'
                                            + '<div class="modal-content"  id="' + element.WDSId + '" style="width:550px;background-color:white;color:white;border-radius:3%; border: 1px solid rgba(255, 249, 249, 0.71);height:  auto;    CURSOR: POINTER; margin-left:-130px">'
                                            + '<div class="">'
                                            + '<table id="tblDev" width="100%" class="tblcolor" style="color:black;" cellpadding="0"; cellspacing="0";>'
                                            + '<tbody>'
                                            + '<tr style="text-align-last: center;">'
                                            + '<td colspan="4" class="clsthead2">' + element.WDSName + '</td>'
                                            + '</tr>'

                                            + '<tr style="text-align-last: left;">'
                                            + '<td style="width:2000px!important">ID</td>'
                                            + '<td style="width:1000px!important">: ' + element.WDSId + ' </td>'
                                            + '<td style="width:2000px!important">WDS Name</td>'
                                            + '<td style="width:1000px!important">: ' +element.WDSName + '</td>'
                                            + '</tr>'

                                            + '<tr style="text-align-last: left;">'
                                            + '<td style="width:2000px!important"></td>'
                                            + '<td style="width:1000px!important"> </td>'
                                            + '<td style="width:2000px!important"> Intake Source WTP</td>'
                                            + '<td style="width:1000px!important">: ' + element.IntakeSource + '</td>'
                                            + '</tr>'

                                            + '<tr style="text-align-last: left;">'
                                            + '<td style="width:3000px!important">Sump Capacity (m3)</td>'
                                            + '<td style="width:1000px!important">: ' + element.SumpCapacity + ' </td>'
                                            + '<td style="width:2000px!important">Pump Design (W+S)</td>'
                                            + '<td style="width:1000px!important">: ' + element.PumpDesign + '</td>'
                                            + '</tr>'

                                            + '<tr style="text-align-last: left;">'
                                            + '<td style="width:2000px!important">InletFlow (m3/Hr)</td>'
                                            + '<td style="width:1000px!important">: ' + element.InletFlow + ' </td>'
                                            + '<td style="width:4000px!important">Outlet Flow Rate (m3/Hr)</td>'
                                            + '<td style="width:1000px!important">: ' + element.OutletFlowRate + '</td>'
                                            + '</tr>'

                                            + '<tr style="text-align-last: left;">'
                                            + '<td style="width:2000px!important">Sump Level (mtr)</td>'
                                            + '<td style="width:1000px!important">: ' + element.SumpLevel + ' </td>'
                                            + '<td style="width:3000px!important">Pressure (psi)</td>'
                                            + '<td style="width:1000px!important">: ' + element.Pressure + '</td>'
                                            + '</tr>'

                                            + '<tr style="text-align-last: left;">'
                                            + '<td style="width:2000px!important">Pump Status</td>'
                                            + '<td style="width:1000px!important">: ' + element.PumpStatus + ' </td>'
                                            + '<td style="width:3000px!important">Pump Run(HH:MM)</td>'
                                            + '<td style="width:1000px!important">: ' + element.PumpRun + '</td>'
                                            + '</tr>'

                                            + '<tr style="text-align-last: left;">'
                                            + '<td style="width:2000px!important">Inlet Volume (m3)</td>'
                                            + '<td style="width:1000px!important">: ' + element.InletVolume + ' </td>'
                                            + '<td style="width:2000px!important">OutletVolume (m3)</td>'
                                            + '<td style="width:1000px!important">: ' + element.OutletVolume + '</td>'
                                            + '</tr>'

                                            + '<tr style="text-align-last: left;">'
                                            + '<td style="width:2000px!important">Kwh/M3</td>'
                                            + '<td style="width:1000px!important">: ' + element.KWHM3 + ' </td>'
                                            + '<td style="width:2000px!important">Unit Consumption(kwh)</td>'
                                            + '<td style="width:1000px!important">: ' + element.UnitConsumption + '</td>'
                                            + '</tr>'

                                            + '<tr style="text-align-last: left;">'
                                            + '<td style="width:1000px!important">LDR</td>'
                                            + '<td style="width:6000px!important">: ' + element.ldr + ' </td>'
                                            + '<td style="width:0px!important"></td>'
                                            + '<td style="width:00px!important"></td>'
                                            + '</tr>'

                                            + '</tbody>'
                                            + '</table>'
                                            + '</div>';
                                        + '</div>';

                                        e.sourceTarget._popup.setContent(dyndt);

                                    }
                                });
                                this.openPopup();
                                $(".leaflet-container a.leaflet-popup-close-button").css("display", "block");

                            })
                            .on('mouseout', function (e) {
                                if (!isClicked) {
                                    this.closePopup();
                                }
                                })
                                .on('mouseover', function (e) {
                                    if (!isClicked) {
                                        this.closePopup();
                                    }
                                })
                                ;

                            mcg.addLayer(marker);
                        });


                    } 
                   

                });
                map.addLayer(mcg);
                $("#getmeter").autocomplete({
                    source: ArrLocation
                });

            },
            failure: function (response) {
                alert(response);
            }
        });

    }

    $("#MapType").change(function () {
        var type = $('#MapType option:selected').text();
        $('#sel_Zone').val(0);
        clearPins();
        if (type == '---All---') {

            $('#ddlWardRegion').css("display", "block");
            $('#ddlKPIRegion').css("display", "block");
            $('#ddlWardLocation').css("display", "block");
            $('#ddlWDSLocationRegion').css("display", "block");
           
               
            
            RenderMarkerForWDSZone(0);
            RenderWDNMarkerbyZone(0);
        }
        else if (type == 'WDS') {

            $('#ddlWardRegion').css("display", "none");
            $('#ddlKPIRegion').css("display", "none");
            $('#ddlWardLocation').css("display", "none");
            $('#ddlWDSLocationRegion').css("display", "block");
            $('#WDS_KpiRegion').css("display", "block");

            
            RenderMarkerForWDSZone(0);
        }
        else if (type == 'WDN') {
            $('#ddlWardRegion').css("display", "block");
            $('#ddlKPIRegion').css("display", "block");
            $('#ddlWardLocation').css("display", "block");
            $('#ddlWDSLocationRegion').css("display", "None");
            $('#WDS_KpiRegion').css("display", "None");
            RenderWDNMarkerbyZone(0);
        }
    });

    function clearPins() {
       
       
            if (Rendered_Marker.length > 0) {
                $.each(Rendered_Marker, function (key, marker) {
                    mcg.removeLayer(marker);
                });
            }
        
    }

   
    RenderMarkerForWDSZone(0);
    RenderWDNMarkerbyZone(0);

    $("#WDS_KPI_ForMapView").change(function () {
        var kpi = $('#WDS_KPI_ForMapView :selected').text();
        var zone = $('#sel_Zone :selected').text();
        var location = $('#ddlWDSLocation :selected').text();

        var type = $('#MapType option:selected').text();
        if (type == "---All---") {
            clearPins();
            RenderMarkerForWDSZone($('#sel_Zone :selected').val());
            RenderMarkerbyWDSKPI(kpi, zone, location);

        }
        else if (type == "WDS") {
            clearPins();
            RenderMarkerbyWDSKPI(kpi, zone, location);
        }

    });

    function RenderMarkerbyWDSKPI(kpi, zone, location) {

        $.ajax({
            type: "GET",
            url: "/map/GetWDSNetworkByKPI",
            data: {
                kpi: kpi, zone: zone, location: location
            },
            success: function (response) {
                console.log(response);

                if (response.length > 0) {

                    $.each(response, function (index, element) {

                        var tmpString = element.coordinate;
                        ArrLocation.push(element.WDSName);
                        hash.set(element.WDSName, element.coordinate);

                        if (tmpString !== null) {


                            $.each(tmpString.split(/[[\]]{1,2}/), function (key, strcoordinate) {

                                //if (strcoordinate.trim(), ",") === ",") {
                                //    return;
                                //}
                                var tempArr = [];
                                var latlon = strcoordinate.split(",");
                                if (isNaN(latlon[0]) || isNaN(latlon[1])) {
                                    return;
                                }


                                var marker_icon = `<div class="${(function () {  return "pinRED";  }).call(this)} bounce"></div>`
                                var icon1 = L.divIcon({
                                    html: marker_icon,
                                    // Specify a class name we can refer to in CSS.
                                    className: 'image-icon',
                                    // Set a markers width and height.
                                    iconSize: [52, 52]
                                });

                                tempArr.push(parseFloat(latlon[0].trim()), parseFloat(latlon[1].trim()));

                                marker = L.marker(new L.LatLng(tempArr[0], tempArr[1]), { icon: icon1, name: element.WDSName, id: element.WDSId })
                                    .bindPopup('', { closeButton: false });

                                Rendered_Marker.push(marker);
                                marker.bindTooltip('<div  class="txtColor" style="width:auto;line-height:16px;"><label class="lblColor" style="margin: 6px 4px 4px 4px;" >' + element.WDSName + ' -: ' + kpi + '  (' + element.kpi + ')' + '</label></div>', { permanent: true, opacity: 1, position: 'top', devStatus: element.devStatus }).openTooltip();
                                marker.on('click', function (e) {
                                    $.ajax({
                                        type: "GET",
                                        url: "/map/GetWDSDataByWDSName",
                                        //contentType: "application/json",
                                        //dataType: "json",
                                        async: false,
                                        data: {
                                            WDSName: element.WDSName
                                        },
                                        success: function (response) {
                                            console.log(response);
                                            const element = response[0];


                                            const dyndt =
                                                '<div class="" id="myModalwds" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index: 1020 !important;">'
                                                + '<div class="modal-dialog" role="document">'
                                                + '<div class="modal-content"  id="' + element.WDSId + '" style="width:550px;background-color:white;color:white;border-radius:3%; border: 1px solid rgba(255, 249, 249, 0.71);height:  auto;    CURSOR: POINTER; margin-left:-130px">'
                                                + '<div class="">'
                                                + '<table id="tblDev" width="100%" class="tblcolor" style="color:black;" cellpadding="0"; cellspacing="0";>'
                                                + '<tbody>'
                                                + '<tr style="text-align-last: center;">'
                                                + '<td colspan="4" class="clsthead2">' + element.WDSName + '</td>'
                                                + '</tr>'

                                                + '<tr style="text-align-last: left;">'
                                                + '<td style="width:2000px!important">ID</td>'
                                                + '<td style="width:1000px!important">: ' + element.WDSId + ' </td>'
                                                + '<td style="width:2000px!important">WDS Name</td>'
                                                + '<td style="width:1000px!important">: ' + element.WDSName + '</td>'
                                                + '</tr>'

                                                + '<tr style="text-align-last: left;">'
                                                + '<td style="width:2000px!important"></td>'
                                                + '<td style="width:1000px!important"> </td>'
                                                + '<td style="width:2000px!important"> Intake Source WTP</td>'
                                                + '<td style="width:1000px!important">: ' + element.IntakeSource + '</td>'
                                                + '</tr>'

                                                + '<tr style="text-align-last: left;">'
                                                + '<td style="width:3000px!important">Sump Capacity (m3)</td>'
                                                + '<td style="width:1000px!important">: ' + element.SumpCapacity + ' </td>'
                                                + '<td style="width:2000px!important">Pump Design (W+S)</td>'
                                                + '<td style="width:1000px!important">: ' + element.PumpDesign + '</td>'
                                                + '</tr>'

                                                + '<tr style="text-align-last: left;">'
                                                + '<td style="width:2000px!important">InletFlow (m3/Hr)</td>'
                                                + '<td style="width:1000px!important">: ' + element.InletFlow + ' </td>'
                                                + '<td style="width:4000px!important">Outlet Flow Rate (m3/Hr)</td>'
                                                + '<td style="width:1000px!important">: ' + element.OutletFlowRate + '</td>'
                                                + '</tr>'

                                                + '<tr style="text-align-last: left;">'
                                                + '<td style="width:2000px!important">Sump Level (mtr)</td>'
                                                + '<td style="width:1000px!important">: ' + element.SumpLevel + ' </td>'
                                                + '<td style="width:3000px!important">Pressure (psi)</td>'
                                                + '<td style="width:1000px!important">: ' + element.Pressure + '</td>'
                                                + '</tr>'

                                                + '<tr style="text-align-last: left;">'
                                                + '<td style="width:2000px!important">Pump Status</td>'
                                                + '<td style="width:1000px!important">: ' + element.PumpStatus + ' </td>'
                                                + '<td style="width:3000px!important">Pump Run(HH:MM)</td>'
                                                + '<td style="width:1000px!important">: ' + element.PumpRun + '</td>'
                                                + '</tr>'

                                                + '<tr style="text-align-last: left;">'
                                                + '<td style="width:2000px!important">Inlet Volume (m3)</td>'
                                                + '<td style="width:1000px!important">: ' + element.InletVolume + ' </td>'
                                                + '<td style="width:2000px!important">OutletVolume (m3)</td>'
                                                + '<td style="width:1000px!important">: ' + element.OutletVolume + '</td>'
                                                + '</tr>'

                                                + '<tr style="text-align-last: left;">'
                                                + '<td style="width:2000px!important">Kwh/M3</td>'
                                                + '<td style="width:1000px!important">: ' + element.KWHM3 + ' </td>'
                                                + '<td style="width:2000px!important">Unit Consumption(kwh)</td>'
                                                + '<td style="width:1000px!important">: ' + element.UnitConsumption + '</td>'
                                                + '</tr>'

                                                + '<tr style="text-align-last: left;">'
                                                + '<td style="width:1000px!important">LDR</td>'
                                                + '<td style="width:6000px!important">: ' + element.ldr + ' </td>'
                                                + '<td style="width:0px!important"></td>'
                                                + '<td style="width:00px!important"></td>'
                                                + '</tr>'

                                                + '</tbody>'
                                                + '</table>'
                                                + '</div>';
                                            + '</div>';

                                            e.sourceTarget._popup.setContent(dyndt);

                                        }
                                    });
                                    this.openPopup();
                                    $(".leaflet-container a.leaflet-popup-close-button").css("display", "block");

                                })
                                    
                                    .on('mouseout', function (e) {
                                        if (!isClicked) {
                                            this.closePopup();
                                        }
                                    })
                                    .on('mouseover', function () {
                                    });


                                mcg.addLayer(marker);
                            });


                        }
                    });

                    map.addLayer(mcg);
                    $("#getmeter").autocomplete({
                        source: ArrLocation
                    });

                }

            },
            failure: function (response) {
                alert(response);
            }
        });

    }
   
});

