/*
Bootstable
 @description  Javascript library to make HMTL tables editable, using Bootstrap
 @version 1.1
 @autor Tito Hinostroza
*/
"use strict";
//Global variables
var params = null;  		//Parameters
var colsEdi = null;
var newColHtml = '<div class="btn-group pull-right">' +
    '<button id="bEdit" type="button" class="btn btn-sm btn-default" onclick="butRowEdit(this);">' +
    '<span class="" >  <img src="assets/images/Icon awesome-pen.png" width="18" height="18"> </span>' +
    '</button>' +
    '<button id="bElim" type="button" class="btn btn-sm btn-default" onclick="butRowDelete(this);">' +
    '<span class="" > <img src="assets/images/Icon material-delete.png" width="15" height="15"></span>' +
    '</button>' +
    '<button id="bAcep" type="button" class="btn btn-sm btn-default" style="display:none;" onclick="butRowAcep(this);">' +
    '<span class="text-green" >Update  <img src="assets/images/Icon material-update.png" width="15" height="15"></span>' +
    '</button>' +
    '<button id="bCanc" type="button" class="btn btn-sm btn-default" style="display:none;" onclick="butRowCancel(this);">' +
    '<span class="" > X </span>' +
    '</button>' +
    '</div>';
//Case NOT Bootstrap
var newColHtml2 = '<div class="btn-group pull-right">' +
    '<button id="bEdit" type="button" class="btn btn-sm btn-default" onclick="butRowEdit(this);">' +
    '<span class="" >   <img src="assets/images/Icon awesome-pen.png" width="18" height="18"></span>' +
    '</button>' +
    '<button id="bElim" type="button" class="btn btn-sm btn-default" onclick="butRowDelete(this);">' +
    '<span class="" > X </span>' +
    '</button>' +
    '<button id="bAcep" type="button" class="btn btn-sm btn-default" style="display:none;" onclick="butRowAcep(this);">' +
    '<span class="" > Update <img src="assets/images/Icon material-update.png" width="15" height="15"></span>' +
    '</button>' +
    '<button id="bCanc" type="button" class="btn btn-sm btn-default" style="display:none;" onclick="butRowCancel(this);">' +
    '<span class="" > → </span>' +
    '</button>' +
    '</div>';
var colEdicHtml = '<td name="buttons">' + newColHtml + '</td>';
$.fn.SetEditable = function (options) {
    var defaults = {
        columnsEd: null,         //Index to editable columns. If null all td editables. Ex.: "1,2,3,4,5"
        $addButton: null,        //Jquery object of "Add" button
        bootstrap: true,         //Indicates bootstrap is present.
        onEdit: function () { },   //Called after edition
        onBeforeDelete: function () { }, //Called before deletion
        onDelete: function () { }, //Called after deletion
        onAdd: function () { }     //Called when added a new row
    };
    params = $.extend(defaults, options);
    var $tabedi = this;   //Read reference to the current table.
    $tabedi.find('thead tr').append('<th name="buttons">Action</th>');  //Add empty column
    if (!params.bootstrap) {
        colEdicHtml = '<td name="buttons">' + newColHtml2 + '</td>';
    }
    //Add column for buttons to all rows.
    $tabedi.find('tbody tr').append(colEdicHtml);
    //Process "addButton" parameter
    if (params.$addButton != null) {
        //There is parameter
        params.$addButton.click(function () {
            rowAddNew($tabedi.attr("id"));
        });
    }
    //Process "columnsEd" parameter
    if (params.columnsEd != null) {
        //Extract felds
        colsEdi = params.columnsEd.split(',');
    }
};
function IterarCamposEdit($cols, action) {
    //Iterate through editable fields in a row
    var n = 0;
    $cols.each(function () {
        n++;
        if ($(this).attr('name') == 'buttons') return;  //Exclude buttons column
        if (!IsEditable(n - 1)) return;   //It's not editable
        action($(this));
    });

    function IsEditable(idx) {
        //Indicates if the passed column is set to be editable
        if (colsEdi == null) {  //no se definió
            return true;  //todas son editable
        } else {  //hay filtro de campos
            for (var i = 0; i < colsEdi.length; i++) {
                if (idx == colsEdi[i]) return true;
            }
            return false;  //no se encontró
        }
    }
}
function ModoEdicion($row) {
    if ($row.attr('id') == 'editing') {
        return true;
    } else {
        return false;
    }
}
//Set buttons state
function SetButtonsNormal(but) {
    $(but).parent().find('#bAcep').hide();
    $(but).parent().find('#bCanc').hide();
    $(but).parent().find('#bEdit').show();
    $(but).parent().find('#bElim').show();
    var $row = $(but).parents('tr');  //accede a la fila
    $row.attr('id', '');  //quita marca
}
function SetButtonsEdit(but) {
    $(but).parent().find('#bAcep').show();
    $(but).parent().find('#bCanc').show();
    $(but).parent().find('#bEdit').hide();
    $(but).parent().find('#bElim').hide();
    var $row = $(but).parents('tr');  //accede a la fila
    $row.attr('id', 'editing');  //indica que está en edición
}
//Events functions
function butRowAcep(but) {
    //Acepta los cambios de la edición
    var $row = $(but).parents('tr');  //accede a la fila
    var $cols = $row.find('td');  //lee campos
    if (!ModoEdicion($row)) return;  //Ya está en edición
    //Está en edición. Hay que finalizar la edición
    IterarCamposEdit($cols, function ($td) {  //itera por la columnas
        var cont = $td.find('input').val() === undefined ? $td.find('select').val() : $td.find('input').val(); //lee contenido del input
        $td.html(cont);  //fija contenido y elimina controles
    });
    SetButtonsNormal(but);
    params.onEdit($row);
}
function butRowCancel(but) {
    //Rechaza los cambios de la edición
    var $row = $(but).parents('tr');  //accede a la fila
    var $cols = $row.find('td');  //lee campos
    if (!ModoEdicion($row)) return;  //Ya está en edición
    //Está en edición. Hay que finalizar la edición
    IterarCamposEdit($cols, function ($td) {  //itera por la columnas
        var cont = $td.find('div').html(); //lee contenido del div
        $td.html(cont);  //fija contenido y elimina controles
    });
    SetButtonsNormal(but);
}
function butRowEdit(but) {
    //Start the edition mode for a row.
    var $row = $(but).parents('tr');  //accede a la fila
    var $cols = $row.find('td');  //lee campos
    if (ModoEdicion($row)) return;  //Ya está en edición
    //Pone en modo de edición
    var focused = false;  //flag
    IterarCamposEdit($cols, function ($td) {  //itera por la columnas
        var cont = $td.html().trim(); //lee contenido
        //Save previous content in a hide <div>
        var div = '<div style="display: none;">' + cont + '</div>';
        var required = '';
        if ($td.attr('data-isrequired')) {
            required = "required";
        }
        var type = "text";
        if ($td.attr('data-type')) {
            type = $td.attr('data-type');
        }
        var pattern = "";
        if ($td.attr('data-pattern')) {
            pattern = $td.attr('data-pattern');
        }
        if (type == 'select') {
            var input = '<select class="form-control input-sm" ' + required + ' > </select>';
            $td.html(div + input);  //Set new content
            var url = $td.attr('data-url');
            if (url) {
                loadSelectData(url, $td.find('select'), cont, '');
            }
        } else if (type == 'checkbox') {
            if (cont == "True") {
                var input = '<select class="form-control input-sm" ' + required + ' ><option selected value="True">Yes</option><option value="False">No</option> </select>';
                $td.html(div + input);  //Set new content
            } else {
                var input = '<select class="form-control input-sm" ' + required + ' ><option value="True">True</option><option selected value="False">False</option> </select>';
                $td.html(div + input);  //Set new content
            }
        } else {
            var input = '<input type="' + type + '" ' + required + '  pattern="' + pattern + '" class="form-control input-sm"  value="' + cont + '">';
            $td.html(div + input);  //Set new content
        }
        //Set focus to first column
        if (!focused) {
            $td.find('input').focus();
            focused = true;
        }
    });
    SetButtonsEdit(but);
}

function loadSelectData(url, $select, cont, defaultOption) {
    //<option value="2">Zone 2</option><option selected value="1">Zone 1</option>
    $.ajax(
        {
            type: "GET",
            url: url,
            success: function (data) {
                var items = defaultOption ? '<option value="">' + defaultOption + '</option>' : '';
                $.each(data, function (i, item) {
                    if (cont == item.Value) {
                        items += "<option selected value='" + item.Value + "'>" + (item.Name) + "</option>";
                    } else {
                        items += "<option value='" + item.Value + "'>" + (item.Name) + "</option>";
                    }
                    if (item) {
                        $select.html(items);
                    }
                });

            }

        });

}
function butRowDelete(but) {  //Elimina la fila actual
    var $row = $(but).parents('tr');  //accede a la fila
    params.onBeforeDelete($row);
    $row.remove();
    params.onDelete();
}
//Functions that can be called directly
function rowAddNew(tabId, initValues = []) {
    /* Add a new row to a editable table. 
     Parameters: 
      tabId       -> Id for the editable table.
      initValues  -> Optional. Array containing the initial value for the 
                     new row.
    */
    var $tab_en_edic = $("#" + tabId);  //Table to edit
    var $rows = $tab_en_edic.find('tbody tr');
    //if ($rows.length==0) {
    //No hay filas de datos. Hay que crearlas completas
    var $row = $tab_en_edic.find('thead tr');  //encabezado
    var $cols = $row.find('th');  //lee campos
    //construye html
    var htmlDat = '';
    var i = 0;
    $cols.each(function () {
        if ($(this).attr('name') == 'buttons') {
            //Es columna de botones
            htmlDat = htmlDat + colEdicHtml;  //agrega botones
        } else {
            if (i < initValues.length) {
                htmlDat = htmlDat + '<td>' + initValues[i] + '</td>';
            } else {
                htmlDat = htmlDat + '<td></td>';
            }
        }
        i++;
    });
    $tab_en_edic.find('tbody').prepend('<tr class="table-danger">' + htmlDat + '</tr>');
    /*} else {
        //Hay otras filas, podemos clonar la última fila, para copiar los botones
        var $lastRow = $tab_en_edic.find('tr:last');
        $lastRow.clone().appendTo($lastRow.parent());  
        $lastRow = $tab_en_edic.find('tr:last');
        var $cols = $lastRow.find('td');  //lee campos
        $cols.each(function() {
            if ($(this).attr('name')=='buttons') {
                //Es columna de botones
            } else {
                $(this).html('');  //limpia contenido
            }
        });
    }*/
    params.onAdd();
}
function rowAddNewAndEdit(tabId, initValues = []) {
    /* Add a new row an set edition mode */
    rowAddNew(tabId, initValues);
    var $lastRow = $('#' + tabId + ' tr:first-child');
    butRowEdit($lastRow.find('#bEdit'));  //Pass a button reference
}
function TableToCSV(tabId, separator) {  //Convert table to CSV
    var datFil = '';
    var tmp = '';
    var $tab_en_edic = $("#" + tabId);  //Table source
    $tab_en_edic.find('tbody tr').each(function () {
        //Termina la edición si es que existe
        if (ModoEdicion($(this))) {
            $(this).find('#bAcep').click();  //acepta edición
        }
        var $cols = $(this).find('td');  //lee campos
        datFil = '';
        $cols.each(function () {
            if ($(this).attr('name') == 'buttons') {
                //Es columna de botones
            } else {
                datFil = datFil + $(this).html() + separator;
            }
        });
        if (datFil != '') {
            datFil = datFil.substr(0, datFil.length - separator.length);
        }
        tmp = tmp + datFil + '\n';
    });
    return tmp;
}
function TableToJson(tabId) {   //Convert table to JSON
    var json = '{';
    var otArr = [];
    var tbl2 = $('#' + tabId + ' tr').each(function (i) {
        var x = $(this).children();
        var itArr = [];
        x.each(function () {
            itArr.push('"' + $(this).text() + '"');
        });
        otArr.push('"' + i + '": [' + itArr.join(',') + ']');
    })
    json += otArr.join(",") + '}'
    return json;
}