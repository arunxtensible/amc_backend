﻿$(document).ready(function () {

    var wtpChart = null;
    function KpiWtpChanged() {

        var ddlvalue = $('#ddl-wtp').find(":selected").text(); //$('#ddl-wtp').val();
        var kpivalue = $('#kpi-wtp').find(":selected").text(); //$('#kpi-wtp').val();
        var wtpdaysvalue = $('#wtpdays').val();
        var wtpchartlable = $('#kpi-wtp').find(":selected").text(); //$('#kpi-wtp').val();

        if (wtpChart != null) {
            wtpChart.destroy();
        }


        //$.get("/Dashboard/UpdateWtpChartData", { "kpivalue": kpivalue, "ddlvalue": ddlvalue, "daysvalue": wtpdaysvalue }, function (Result) {
        //-----------Old chart code
        //$.get("/Dashboard/UpdateWtpChartData", { "WTPName": ddlvalue, "KPIName": kpivalue, "Days": wtpdaysvalue }, function (Result) {
        //    console.log(Result);
        //    wtpchart_data = wtpchart_data;
        //    wtpchart_labels = wtpchart_labels;

        //    var wtpchart_data = Result.data;
        //    var wtpchart_labels = Result.lables;
        //    var wtpchart_label = wtpchartlable;

        //    loadWtpChart(wtpchart_data, wtpchart_labels, wtpchart_label);
        //});
        //-------- hichart code
       
        $.get("/Dashboard/UpdateWtpChartData", { "WTPName": ddlvalue, "KPIName": kpivalue, "Days": wtpdaysvalue }, function (Result) {
            var YAxisText = $('#kpi-wtp  option:selected').text();
            var ChartTitle = $('#ddl-wtp  option:selected').text();
            var unit = 'm3';
            if ($('#kpi-wtp  option:selected').is(":contains('hr')")) {
                 unit = 'm3/hr';
            }
            else if ($('#kpi-wtp  option:selected').is(":contains('MTR')")) {
                 unit = 'MTR';
            }
            if (ChartTitle === 'Please Select WTP') {
                ChartTitle = 'Water Treatment Plant';
            }
            console.log("UpdateWtpChartData====");
            console.log(Result['data']);
            console.log(Result);
            var result2 = Result;
            console.log(result2['data']);
            Highcharts.chart('wtpchartcontainer', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Water Treatment Plant :- ' + ChartTitle 
                },
                subtitle: {
                    text: 'Source: Ahmedabad Municipal Corporation'
                },
                xAxis: {
                    categories: result2['lables'],
                    crosshair: true,
                    title: {
                        text: 'Date And Time'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: YAxisText
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr>' +
                        '<td style="padding:0"><b>{point.y:.2f} ' + unit+'</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: YAxisText,
                    data: result2['data']

                }]
            });
        });

    }

    function loadWtpChart(wtpchart_data, wtpchart_labels, wtpchart_label) {
        var ctx = document.getElementById("wtpchart").getContext('2d');
        wtpChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: wtpchart_labels,
                datasets: [{
                    label: wtpchart_label,
                    backgroundColor: '#57b7fa',
                    barThickness: 25,
                    maxBarThickness: 50,
                    borderColor: '#57b7fa',
                    data: wtpchart_data,
                }]
            },

            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            },
        });
    }

    function loadWTPList() {
        $('#wtp')
            .empty()
            .append('<option selected="selected" value="">select</option>')
            ;
        $.get("/Dashboard/GetWTPData", {
            "x": $("#ZoneListForChartWTP").val()
        }, function (Result) {
            $.each(Result, function (index) {
                $('#wtp')
                    .append($("<option></option>")
                        .attr("value", Result[index].Item1)
                        .text(Result[index].Item2));
            });
        });
    }


    $("#kpi-wtp").on("change", KpiWtpChanged);
    $("#ddl-wtp").on("change", KpiWtpChanged);
    $("#wtpdays").on("change", KpiWtpChanged);
    $("#ZoneListForChartWTP").on("change", loadWTPList);
});