﻿

function GetURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

function replaceNull(data, replaceBy, raiseError) {
    //var JSdata = JSON.stringify(data);
    if (replaceBy === undefined) replaceBy = false;
    if (raiseError === undefined) raiseError = false;
    if (data === undefined) {
        if (raiseError) throw ("#020416.125900 data was <undefined>");
        return replaceBy;
    } else if (data === null) {
        if (raiseError) throw ("#020416.125901 data was <null>");
        return replaceBy;

    } else if (data.length === 0) {
        if (raiseError) throw ("#020416.125902 data length was <0>");
        return replaceBy;
    }
    return data;
}