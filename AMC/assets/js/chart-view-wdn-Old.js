﻿$(document).ready(function () {

    var wdnChart = null; 
    $("#KPI_datetime_wdn_chart").change(function () {
        KpiWdnChanged();
    });
    $("#wdn_days").change(function () {
        KpiWdnChanged();
    });
    $("#location2").change(function () {
        KpiWdnChanged();
    });
    function KpiWdnChanged() {

        var zonename = $('#ZoneListCharViewWDN').find(":selected").text();
        var wardname = $('#ward2').find(":selected").text();
        var location = $('#location2').find(":selected").text();
        var kpivalue = $('#kpi-wdn').find(":selected").text();
        var wdndaysvalue = $('#KPI_datetime_wdn_chart').val().toString();//5 
        var wdn_days = $('#wdn_days').val();
        //$('#wdndays').val();  var date = //var ldr = moment(element.ldr).format("DD/MM/YYYY HH:mm");
        var wdnchartlable = $('#kpi-wdn').find(":selected").text(); //$('#kpi-wdn').val();

        if (wdnChart !== null) {
            wdnChart.destroy();
        }
       

        //$.get("/Dashboard/UpdateWdnChartData", { "ZoneName": zonename, "WardName": wardname, "Location": location, "KPIName": kpivalue, "Days": wdndaysvalue, "iterationdays": wdn_days }, function (Result) {
        //    console.log(Result);
        //    wdnchart_data = wdnchart_data;
        //    wdnchart_labels = wdnchart_labels;

        //    var wdnchart_data = Result.data;
        //    var wdnchart_labels = Result.lables;
        //    var wdnchart_label = wdnchartlable;//"New Label1";

        //    loadWdnChart(wdnchart_data, wdnchart_labels, wdnchart_label);
        //});

        //---------------------new hicharts
        $.get("/Dashboard/UpdateWdnChartData", { "ZoneName": zonename, "WardName": wardname, "Location": location, "KPIName": kpivalue, "Days": wdndaysvalue, "iterationdays": wdn_days }, function (Result) {
            var YAxisText = $('#kpi-wdn  option:selected').text();
            var ChartTitle = $('#location2  option:selected').text();
            var unit = 'm3';
            if ($('#kpi-wdn  option:selected').is(":contains('hr')")) {
                unit = 'm3/hr';
            }
            else if ($('#kpi-wdn  option:selected').is(":contains('MTR')")) {
                unit = 'MTR';
            }
            else if ($('#kpi-wdn  option:selected').is(":contains('VDC')")) {
                unit = 'VDC';
            }
            else if ($('#kpi-wdn  option:selected').is(":contains('PSI')")) {
                unit = 'PSI';
            }
            if (ChartTitle === 'Select Location') {
                ChartTitle = '';
            }
            console.log("UpdateWDSChartData====");
            console.log(Result['data']);
            console.log(Result);
            var result2 = Result;
            console.log(result2['data']);
            Highcharts.chart('wdnchartcontainer', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Water Distributon Network :- ' + ChartTitle
                },
                subtitle: {
                    text: 'Source: Ahmedabad Municipal Corporation'
                },
                xAxis: {
                    categories: result2['lables'],
                    crosshair: true,
                    title: {
                        text: 'Date And Time'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: YAxisText
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr>' +
                        '<td style="padding:0"><b>{point.y:.2f} ' + unit + '</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: YAxisText,
                    data: result2['data']

                }]
            });
        });


    }

    function loadWdnChart(wdnchart_data, wdnchart_labels, wdnchart_label) {
        var ctx = document.getElementById("wdnchart").getContext('2d');
        wdnChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: wdnchart_labels,
                datasets: [{
                    label: wdnchart_label,
                    backgroundColor: '#57b7fa',
                    barThickness: 25,
                    maxBarThickness: 50,
                    borderColor: '#57b7fa',
                    data: wdnchart_data,
                }]
            },

            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            },
        });
    }

    
    $("#KPI_datetime2").on("change", KpiWdnChanged);
    $("#kpi-wdn").on("change", KpiWdnChanged);
    //$("#ddl-wdn").on("change", KpiWdnChanged);
    //$("#wdndays").on("change", KpiWdnChanged);
    //$("#ZoneListForChartWDS").on("change", loadWDSList);
});