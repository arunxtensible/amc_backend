﻿$(document).ready(function () {

    var wdsChart = null;
    function KpiWdsChanged() {
        $('#wdschartcontainer').css('display', 'none');
        $('#wdschartLoader').css('display', 'block');

        var zonename = 'Central';
        var kpivalue = $('#kpi-wds').find(":selected").text(); //$('#kpi-wds').val();
        var iterationDatetime = '';
        if (kpivalue === 'Outlet Flow (m3/hr)') {
            $('#div_datetimepicker_ForWDSChartView').css("display", "block");
            iterationDatetime = $('#datetimepicker_ForWDSChartView').val().toString();
        }
        else {
            $('#div_datetimepicker_ForWDSChartView').css("display", "none");
        }
        var ddlvalue = $('#wds').find(":selected").text(); //$('#ddl-wds').val();
        var wdsdaysvalue = $('#wdsdays').val();
        var wdschartlable = $('#kpi-wds').find(":selected").text(); //$('#kpi-wds').val();

        if (wdsChart != null) {
            wdsChart.destroy();
        }

        //-----------old charts
        //$.get("/Dashboard/UpdateWdsChartData", { "ZoneName": zonename, "KPIName": kpivalue, "WDSName": ddlvalue, "Days": wdsdaysvalue }, function (Result) {
        //    console.log(Result);
        //    wdschart_data = wdschart_data;
        //    wdschart_labels = wdschart_labels;

        //    var wdschart_data = Result.data;
        //    var wdschart_labels = Result.lables;
        //    var wdschart_label = wdschartlable;

        //    loadWdsChart(wdschart_data, wdschart_labels, wdschart_label);
        //});

        //---------------------new hicharts
        $.get("/Dashboard/UpdateWdsChartData", { "ZoneName": zonename, "KPIName": kpivalue, "WDSName": ddlvalue, "Days": wdsdaysvalue, "iterationDatetime": iterationDatetime }, function (Result) {
            var YAxisText = $('#kpi-wds  option:selected').text();
            var ChartTitle = $('#wds  option:selected').text();
            var unit = 'm3';
            if ($('#kpi-wds  option:selected').is(":contains('hr')")) {
                unit = 'm3/hr';
            }
            else if ($('#kpi-wds  option:selected').is(":contains('MTR')")) {
                unit = 'MTR';
            }
            else if ($('#kpi-wds  option:selected').is(":contains('KWH')")) {
                unit = 'KWH';
            }
            else if ($('#kpi-wds  option:selected').is(":contains('PSI')")) {
                unit = 'PSI';
            }else if ($('#kpi-wds  option:selected').is(":contains('Minute')")) {
                unit = 'Minute';
            }
            if (ChartTitle === 'select') {
                ChartTitle = '';
            }
            console.log("UpdateWDSChartData====");
            console.log(Result['data']);
            console.log(Result);
            var result2 = Result;
            console.log(result2['data']);
            $('#wdschartcontainer').css('display', 'block');
            $('#wdschartLoader').css('display', 'none');
            Highcharts.chart('wdschartcontainer', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Water Distributon Station :- '+ChartTitle
                },
                subtitle: {
                    text: 'Source: Ahmedabad Municipal Corporation'
                },
                xAxis: {
                    categories: result2['lables'],
                    crosshair: true,
                    title: {
                        text: 'Date And Time'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: YAxisText
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr>' +
                        '<td style="padding:0"><b>{point.y:.2f} ' + unit + '</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: YAxisText,
                    data: result2['data']

                }]
            });
        });

    }

    function loadWdsChart(wdschart_data, wdschart_labels, wdschart_label) {
        var ctx = document.getElementById("wdschart").getContext('2d');
        wdsChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: wdschart_labels,
                datasets: [{
                    label: wdschart_label,
                    backgroundColor: '#57b7fa',
                    barThickness: 25,
                    maxBarThickness: 50,
                    borderColor: '#57b7fa',
                    data: wdschart_data,
                }]
            },

            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            },
        });
    }

    function loadWDSList() {
        $('#wds')
            .empty()
            .append('<option selected="selected" value="">select</option>')
            ;
        $.get("/Dashboard/GetWDSData", {
            "x": $("#ZoneListForChartWDS").val()
        }, function (Result) {
            $.each(Result, function (index) {
                $('#wds')
                    .append($("<option></option>")
                        .attr("value", Result[index].Item1)
                        .text(Result[index].Item2));
            });
        });
    }
    function loadWDSList1() {
        $('#wds-linechart')
            .empty()
            .append('<option selected="selected" value="">Select</option>')
            ;
        $.get("/Dashboard/GetWDSData", {
            "x": $("#ZoneListForLineChartWDS").val()
        }, function (Result) {
            $.each(Result, function (index) {
                $('#wds-linechart')
                    .append($("<option></option>")
                        .attr("value", Result[index].Item1)
                        .text(Result[index].Item2));
            });
        });
    }



    $("#kpi-wds").on("change", KpiWdsChanged);
    $("#ddl-wds").on("change", KpiWdsChanged);
    $("#wdsdays").on("change", KpiWdsChanged);
    $("#ZoneListForChartWDS").on("change", loadWDSList);
    $("#datetimepicker_ForWDSChartView").on("change", KpiWdsChanged);


    $("#ZoneListForLineChartWDS").on("change", loadWDSList1);
    $("#datetimepicker_ForWDSLineChartFromDate").on("change", KpiWdsLineChartChanged);
    $("#datetimepicker_ForWDSLineChartToDate").on("change", KpiWdsLineChartChanged);

    function KpiWdsLineChartChanged() {
        $('#wdschartLoaderLineChart').css('display', 'block');
        $('#wdsLinechartcontainerWithCompare').css('display', 'none');
        var zonename = $('#ZoneListForLineChartWDS').find(":selected").text();
        var location = $('#wds-linechart').find(":selected").text();
        var kpivalue = $('#kpi-wds-LineChart').find(":selected").text();
        var fromDate = $('#datetimepicker_ForWDSLineChartFromDate').val().toString();//5 
        var toDate = $('#datetimepicker_ForWDSLineChartToDate').val().toString();//5 
        var wdnchartlable = kpivalue;

        if (wdsChart !== null) {
            wdsChart.destroy();
        }

        //---------------------new hicharts

        $.get("/Dashboard/UpdateWdsChartDataWithComparison", { "ZoneName": zonename, "Location": location, "KPIName": kpivalue, "FromDate": fromDate, "ToDate": toDate }, function (Result) {
            $('#wdschartLoaderLineChart').css('display', 'none');
            var YAxisText = $('#kpi-wds-LineChart  option:selected').text();
            var ChartTitle = $('#wds-linechart  option:selected').text();
            var unit = 'm3/hr vs PSI';

            if (ChartTitle === 'Select Location') {
                ChartTitle = '';
            }
            var result2 = Result;
            console.log('labels :- ' + result2['lables']);
            console.log('Data :- ' + result2['data']);
            var jsonResponse = result2['data'];
            console.log(' json data res : ' + jsonResponse);
            var seriesOutput1 = jsonResponse;

            $('#wdschartLoaderLineChart').css('display', 'none');
            $('#wdsLinechartcontainerWithCompare').css('display', 'block');


            Highcharts.chart('wdsLinechartcontainerWithCompare', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Outlet Flow vs Pressure'
                },
                subtitle: {
                    text: 'Source: Ahmedabad Municipal Corporation'
                },
                xAxis: {
                    categories: result2['lables']
                },
                yAxis: {
                    title: {
                        text: ''
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: seriesOutput1
                //series: result2['data']

            });
        });



    }
});