//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AMC
{
    using System;
    using System.Collections.Generic;
    
    public partial class WDNMaster
    {
        public int WDNID { get; set; }
        public string WDNUniqueId { get; set; }
        public int ZoneId { get; set; }
        public int WardId { get; set; }
        public int WDSId { get; set; }
        public string Location { get; set; }
        public Nullable<decimal> LineSize { get; set; }
        public Nullable<decimal> CDQNet { get; set; }
        public Nullable<decimal> CDQNegative { get; set; }
        public Nullable<decimal> LDQNet { get; set; }
        public Nullable<decimal> LDQNegative { get; set; }
        public Nullable<decimal> Benchmark_water_qty_in_days { get; set; }
        public Nullable<decimal> Last_communicated_flow_rate { get; set; }
        public Nullable<decimal> Last_communicated_line_pressure { get; set; }
        public Nullable<decimal> Min_pressure_psi_current_day { get; set; }
        public Nullable<decimal> Max_pressure_current_day { get; set; }
        public string Coordinate { get; set; }
        public Nullable<bool> IsInstalled { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    
        public virtual WardMaster WardMaster { get; set; }
        public virtual WDSMaster WDSMaster { get; set; }
        public virtual ZoneMaster ZoneMaster { get; set; }

        public string PLCIPAddress { get; set; }
    }
}
