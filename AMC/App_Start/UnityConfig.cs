using AMC.IServices;
using AMC.Services;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace AMC
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IUser, UserRepository>();
            container.RegisterType<IZone, Zone>();
            container.RegisterType<IWTPInletOutlet, WTPInletOutlet>();
            container.RegisterType<IWDNInletOutlet, WDNInletOutlet>();
            container.RegisterType<IWtp, WTPRepository>();
            container.RegisterType<IWard, WardRepository>();
            container.RegisterType<IDevice, DeviceRepository>();
            container.RegisterType<IWds, WDSRepository>();
            container.RegisterType<IWdn, WDNRepository>();
            container.RegisterType<IData, DataRepository>();
            container.RegisterType<IMap, MapRepository>();
            container.RegisterType<IWdn, WDNRepository>();
            container.RegisterType<ISetting, SettingRepository>();
            container.RegisterType<IReport, Report>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}