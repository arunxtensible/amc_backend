﻿
using System.Web.Http;
//using Microsoft.Owin.Security.OAuth;

namespace AMC
{
    public static class WebApiConfig
    {
       
        public static void Register(HttpConfiguration config)
        {
            //config.DependencyResolver = new AMC.Api.NinjectResolver();

            //config.SuppressDefaultHostAuthentication();
            //config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            //config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "get", id = RouteParameter.Optional }
            );
         
        }
    }
}
