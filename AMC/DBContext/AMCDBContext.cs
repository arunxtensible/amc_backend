﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AMC.DBContext
{
    public class AMCDBContext : DbContext
    {
        public AMCDBContext() : base("MyConnString")
        {
            var objectContext = (this as System.Data.Entity.Infrastructure.IObjectContextAdapter).ObjectContext;

            // Sets the command timeout for all the commands
            objectContext.CommandTimeout = 500;
        }
    }
}