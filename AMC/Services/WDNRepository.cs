﻿using AMC.IServices;
using AMC.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq;
using System.Web;
using AMC.DBContext;

namespace AMC.Services
{
    public class WDNRepository : IWdn
    {
        AMCDBContext _db = new AMCDBContext();
        public string ConnectionString = ConfigurationManager.ConnectionStrings["MyConnString"].ConnectionString;
        Mapper map = null;
        public WDNRepository()
        {
            MapperConfiguration config = new MapperConfiguration(cfg => {
                cfg.CreateMap<WDNMaster, WDNModel>();
                cfg.CreateMap<WDNModel, WDNMaster>();
            });
            map = new Mapper(config);
        }

        public List<WDNModel> GetAllWDN()
        {
            //using (var ctx = new AMCEntities())
            //{
            //  var WdnList = ctx.WDNMasters.Where(y => (bool)!y.IsDeleted).OrderByDescending(o => o.WDNID).ToList(); // Is Deleted Change
            // // var WdnList = ctx.WDNMasters.OrderByDescending(o => o.WDNID).ToList();
            //    List<WDNModel> _Wdn = map.Map<List<WDNModel>>(WdnList);
            //    return _Wdn;
            //}
            List<WDNModel> locaitonList = new List<WDNModel>();

            try
            {
                using (_db = new AMCDBContext())
                {
                    locaitonList = _db.Database.SqlQuery<WDNModel>("EXEC GetAllWDNLocations").ToList();
                }

            }
            catch (Exception ex)
            {
            }

            return locaitonList;
        }

        public List<WDNZoneListModel> GetAllZones()
        {
            List<WDNZoneListModel> _lst = new List<WDNZoneListModel>();
            

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = new SqlCommand("GetWDNZone", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 1600;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                int i = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    WDNZoneListModel obj = new WDNZoneListModel();
                    obj.Id = Convert.ToInt32(dr["Id"]);
                    obj.Zone = (dr["Zone"]).ToString();
                    //obj.Capacity = Convert.ToDecimal(dr["Capacity"]); 
                    obj.LDQ = Convert.ToDecimal(dr["LDQ"]);
                    obj.CDQ = Convert.ToDecimal(dr["CDQ"]);

                    _lst.Add(obj);
                    i++;
                }

            }
            return _lst;
        }

        public List<WDNWardListModel> GetAllWards(int ZoneId)
        {
            List<WDNWardListModel> _lst = new List<WDNWardListModel>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = new SqlCommand("GetWDNWardList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@InZoneId", SqlDbType.Int).Value = ZoneId;
                cmd.CommandTimeout = 1600;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    WDNWardListModel obj = new WDNWardListModel();

                    obj.Id = Convert.ToInt32(dr["WardId"]);
                   // obj.ZoneId = Convert.ToInt32(dr["ZoneId"]);
                    obj.ZoneId = Convert.ToInt32(ZoneId);
                    obj.Ward = (dr["Ward"]).ToString();
                    //obj.Capacity = Convert.ToDecimal(dr["Capacity"]);
                    obj.LDQ = Convert.ToDecimal(dr["LDQ"]);
                    obj.CDQ = Convert.ToDecimal(dr["CDQ"]);

                    _lst.Add(obj);
                }
            }
            return _lst;
        }


        public List<WDNLocationListModel> GetAllLocations(int WardId)
        {
            List<WDNLocationListModel> _lst = new List<WDNLocationListModel>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = new SqlCommand("GetWDNLocationList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@InWardId", SqlDbType.Int).Value = WardId;
                cmd.CommandTimeout = 1600;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    WDNLocationListModel obj = new WDNLocationListModel();

                    obj.Id = Convert.ToInt32(dr["LocationId"]);
                    //obj.ZoneId = Convert.ToInt32(dr["ZoneId"]);
                    obj.WardId = Convert.ToInt32(dr["WardId"]);
                    obj.Location = (dr["Location"]).ToString();
                    //obj.Capacity = Convert.ToDecimal(dr["Capacity"]);
                    obj.LDQ = Convert.ToDecimal(dr["LDQ"]);
                    obj.CDQ = Convert.ToDecimal(dr["CDQ"]);

                    _lst.Add(obj);
                }
            }

            //try
            //{
            //    using (_db = new AMCDBContext())
            //    {
            //        _lst = _db.Database.SqlQuery<WDNLocationListModel>("EXEC AMC_GetWDNLocationList @InWardId={0}", WardId).ToList();
            //    }

            //}
            //catch (Exception ex)
            //{
            //}
            return _lst;
        }

        public WDNModel GetWDN(int id)
        {
            using (var ctx = new AMCEntities())
            {
                var _WDNName = ctx.WDNMasters.Find(id);
                if (_WDNName != null)
                {
                    WDNModel _Wdn = map.Map<WDNModel>(_WDNName);
                    return _Wdn;
                }
                return null;
            }
        }
        public WDNModel Create(WDNModel model)
        {
            using (var ctx = new AMCEntities())
            {
                var _Wds = map.Map<WDNMaster>(model);
                ctx.WDNMasters.Add(_Wds);
                ctx.SaveChanges();
                model.WDNID = _Wds.WDNID;

                return model;
            }
        }
        public bool Update(WDNModel model)
        {
            //using (var ctx = new AMCEntities())
            //{
            //    var _WDN = ctx.WDNMasters.Find(model.WDNID);
            //    if (_WDN != null)
            //    {
            //        _WDN.WDNID = model.WDNID;
            //        _WDN.WDNUniqueId = model.WDNUniqueId;
            //        _WDN.ZoneId = model.ZoneId;
            //        _WDN.WardId = model.WardId;
            //        _WDN.WDSId = model.WDSId;
            //        _WDN.Location = model.Location;
            //        _WDN.CDQNet = model.CDQNet;
            //        _WDN.LineSize = model.LineSize;
            //        _WDN.CDQNegative = model.CDQNegative;
            //        _WDN.LDQNet = model.LDQNet;
            //        _WDN.LDQNegative = model.LDQNegative;
            //        _WDN.Benchmark_water_qty_in_days = model.Benchmark_water_qty_in_days;
            //        _WDN.Last_communicated_flow_rate = model.Last_communicated_flow_rate;
            //        _WDN.Last_communicated_line_pressure = model.Last_communicated_line_pressure;
            //        _WDN.Min_pressure_psi_current_day = model.Min_pressure_psi_current_day;
            //        _WDN.Max_pressure_current_day = model.Max_pressure_current_day;
            //        _WDN.Coordinate = model.Coordinate;
            //        _WDN.IsInstalled = model.IsInstalled;
            //        _WDN.PLCIPAddress = model.PLCIPAddress;
            //        ctx.Entry(_WDN).State = EntityState.Modified;
            //        ctx.SaveChanges();
            //        return true;
            //    }
            //    return false;
            //}
            bool IsSaved = false;
            try
            {
                using (_db = new AMCDBContext())
                {
                    IsSaved = _db.Database.SqlQuery<bool>("EXEC UpdateWDNLocation @WDNID={0},@WDNUniqueId={1},@ZoneId={2},@WardId={3},@WDSId={4},@Location={5},@CDQNet={6},@LineSize={7},@CDQNegative={8},@LDQNet={9},@LDQNegative={10},@Benchmark_water_qty_in_days={11},@Last_communicated_flow_rate={12},@Last_communicated_line_pressure={13},@Min_pressure_psi_current_day={14},@Max_pressure_current_day={15},@Coordinate={16},@IsInstalled={17},@PLCIPAddress={18}", model.WDNID, model.WDNUniqueId, model.ZoneId, model.WardId, model.WDSId, model.Location, model.CDQNet, model.LineSize, model.CDQNegative, model.LDQNet, model.LDQNegative, model.Benchmark_water_qty_in_days, model.Last_communicated_flow_rate, model.Last_communicated_line_pressure, model.Min_pressure_psi_current_day, model.Max_pressure_current_day, model.Coordinate, model.IsInstalled, model.PLCIPAddress).FirstOrDefault();
                }

            }
            catch (Exception ex)
            {
            }

            return IsSaved;
        }
        public bool Delete(int id)
        {
            using (var ctx = new AMCEntities())
            {
                var _WDN = ctx.WDNMasters.Find(id);
                if (_WDN != null)
                {
                    _WDN.IsDeleted = true;
                    ctx.Entry(_WDN).State = EntityState.Modified;
                    ctx.SaveChanges();
                    return true;
                }
                return false;
            }
        }
    }
}