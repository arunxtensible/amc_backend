﻿using AMC.IServices;
using AMC.Models;
using AutoMapper;
using System;
using System.Collections.Generic;

using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq;
using System.Web;

using System.IO;


namespace AMC.Services
{
    public class WTPRepository : IWtp
    {
        public string ConnectionString = ConfigurationManager.ConnectionStrings["MyConnString"].ConnectionString;
        Mapper map = null;
        public WTPRepository()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<WTP_Master, WTPModel>();
                cfg.CreateMap<WTPModel, WTP_Master>();
            });
            map = new Mapper(config);
        }

        public List<WTPModel> GetAllWTP()
        {
            using (var ctx = new AMCEntities())
            {
                var WtpList = ctx.WTP_Master.Where(y => !y.IsDeleted).OrderByDescending(o => o.WTPId).ToList();  // Is Deleted Change 
                List<WTPModel> _Wtp = map.Map<List<WTPModel>>(WtpList);
                return _Wtp;
            }
        }

        public WTPModel GetWTP(int id)
        {
            using (var ctx = new AMCEntities())
            {
                var _WTPName = ctx.WTP_Master.Find(id);
                if (_WTPName != null)
                {
                    WTPModel _Wtp = map.Map<WTPModel>(_WTPName);
                    return _Wtp;
                }
                return null;
            }
        }


        public List<WTPGroundWaterModel> GetAllWTPGroundWater()
        {
            List<WTPGroundWaterModel> _Wtp = new List<WTPGroundWaterModel>();


            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                System.Data.DataTable dt = new System.Data.DataTable();

                SqlCommand cmd = new SqlCommand("GetWTPGroundWater", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    WTPGroundWaterModel obj = new WTPGroundWaterModel();

                    obj.LocationName = (dr["LocationName"]).ToString();
                    obj.RPT_INLET_FLOW_CRNT_TOTAL = Convert.ToInt32(dr["RPT_INLET_FLOW_CRNT_TOTAL"]);
                    obj.RPT_OUTLET_FLOW_CRNT_TOTAL = Convert.ToInt32(dr["RPT_OUTLET_FLOW_CRNT_TOTAL"]);
                    obj.last_day_qty = Convert.ToInt32(dr["last_day_qty"]);

                    _Wtp.Add(obj);
                }

            }
            return _Wtp;
        }

      

     

        public WTPModel Add(WTPModel model)
        {
            using (var ctx = new AMCEntities())
            {
                var _WTP = new WTP_Master();
                _WTP.WTPName = model.WTPName;
                _WTP.Capacity = model.Capacity;
                ctx.WTP_Master.Add(_WTP);
                ctx.SaveChanges();
                model.WTPId = _WTP.WTPId;
                return model;
            }
        }

        public bool Update(WTPModel model)
        {
            using (var ctx = new AMCEntities())
            {
                var _WTP = ctx.WTP_Master.Find(model.WTPId);  
                if (_WTP != null)
                {
                    _WTP.WTPName = model.WTPName;
                    _WTP.Capacity = model.Capacity;
                    ctx.Entry(_WTP).State = EntityState.Modified;
                    ctx.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public bool Delete(int id)
        {
            using (var ctx = new AMCEntities())
            {
                var _WTP = ctx.WTP_Master.Find(id);
                if (_WTP != null)
                {
                    _WTP.IsDeleted = true; // Is Deleted Change 
                    ctx.Entry(_WTP).State = EntityState.Modified;
                    ctx.SaveChanges();
                    return true;
                }
                return false;
            }
        }
    }
}