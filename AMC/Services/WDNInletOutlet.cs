﻿using AMC.DBContext;
using AMC.IServices;
using AMC.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AMC.Services
{
    public class WDNInletOutlet : IWDNInletOutlet
    {
        AMCDBContext _db = new AMCDBContext();
        public string ConnectionString = ConfigurationManager.ConnectionStrings["MyConnString"].ConnectionString;

        public const string GetWDN_Modal_Details = "exec GetWDNInletOutletDetails @TagName={0}";
        public const string GetWDN_ModalPopup_Details = "exec GetWDNPopupDetailsByLocation @LocationName={0}";
        public const string GetWDNLocation_GraphData = "exec GetWDNLocationGraphData @inWardId={0}";
        //public const string GetWDNLocation_GraphData = "exec AMC_GetWDNLocationGraphData @inWardId={0}";

        Mapper map = null;
        public WDNInletOutlet()
        {
            //MapperConfiguration config = new MapperConfiguration(cfg => {
            //    cfg.CreateMap<ZoneMaster, ZoneModel>();
            //    cfg.CreateMap<ZoneModel, ZoneMaster>();
            //});
            //map = new Mapper(config);
        }


        List<WDNInletOutletModel> IWDNInletOutlet.GetWDNZoneGraphData()
        {
            List<WDNInletOutletModel> WDNOutlet = new List<WDNInletOutletModel>();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {

                DataTable dt = new DataTable();


                SqlCommand cmd = new SqlCommand("GetWDNZoneGraphData", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    WDNInletOutletModel obj = new WDNInletOutletModel();

                    obj.WDNName = (dr["WDNName"]).ToString();
                    obj.Capacity = Convert.ToDecimal(dr["Capacity"]);
                    obj.CDQ = Convert.ToDecimal(dr["CDQ"]);
                    obj.LDQ = Convert.ToDecimal(dr["LDQ"]);
                    obj.WDNId = Convert.ToInt32(dr["WDNId"]);
                    obj.LDRDateAndTime = (dr["LDRDateTime"]).ToString();
                    WDNOutlet.Add(obj);
                }



            }


            /////////--------------------
            //try
            //{
            //    using (_db = new AMCDBContext())
            //    {
            //        WDNOutlet = _db.Database.SqlQuery<WDNInletOutletModel>("exec AMC_GetWDNZoneGraphData").ToList();
            //    }
            //}
            //catch (Exception ex)
            //{
            //}

            return WDNOutlet;
        }


        List<WDNInletOutletModel> IWDNInletOutlet.GetWDNWardGraphData(int Id)
        {
            List<WDNInletOutletModel> WDNOutlet = new List<WDNInletOutletModel>();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {

                DataTable dt = new DataTable();


                SqlCommand cmd = new SqlCommand("GetWDNWardGraphData", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@inZoneId", SqlDbType.Int).Value = Id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    WDNInletOutletModel obj = new WDNInletOutletModel();

                    obj.WDNName = (dr["WDNName"]).ToString();
                    // obj.Capacity = Convert.ToDecimal(dr["Capacity"]);
                    obj.CDQ = Convert.ToDecimal(dr["CDQ"]);
                    obj.LDQ = Convert.ToDecimal(dr["LDQ"]);
                    obj.WDNId = Convert.ToInt32(dr["WDNId"]);
                    obj.LDRDateAndTime = (dr["LDRDateTime"]).ToString();
                    WDNOutlet.Add(obj);
                }
            }
            /////////--------------------
            //try
            //{
            //    using (_db = new AMCDBContext())
            //    {
            //        WDNOutlet = _db.Database.SqlQuery<WDNInletOutletModel>("exec AMC_GetWDNWardGraphData @inZoneId={0}",Id).ToList();
            //    }
            //}
            //catch (Exception ex)
            //{
            //}
            return WDNOutlet;
        }


        List<WDNInletOutletModel> IWDNInletOutlet.GetWDNLocationGraphData(int Id)
        {
            List<WDNInletOutletModel> WDNLocationList = new List<WDNInletOutletModel>();
            try
            {
                using (_db = new AMCDBContext())
                {
                    WDNLocationList = _db.Database.SqlQuery<WDNInletOutletModel>(GetWDNLocation_GraphData, Id).ToList();
                }
            }
            catch (Exception ex)
            {
            }           
            return WDNLocationList;
        }

        List<WDNInletOutletDetailModel> IWDNInletOutlet.WDNInletOutletDetailByWDNName(string TagName)
        {
            List<WDNInletOutletDetailModel> WDNInlet = new List<WDNInletOutletDetailModel>();
            using (_db=new AMCDBContext())
            {                
               // WDNInlet = _db.Database.SqlQuery<WDNInletOutletDetailModel>(GetWDN_Modal_Details, TagName).ToList();
                WDNInlet = _db.Database.SqlQuery<WDNInletOutletDetailModel>(GetWDN_ModalPopup_Details, TagName).ToList();
            }  
            return WDNInlet;
        }

        public List<WDNInletOutletModel> GetAllInlet(string Type)
        {
            throw new NotImplementedException();
        }
    }
}