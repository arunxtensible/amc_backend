﻿using AMC.DBContext;
using AMC.IServices;
using AMC.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AMC.Services
{
    public class SettingRepository : ISetting
    {
        AMCDBContext _db = new AMCDBContext();
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["MyConnString"].ConnectionString;
        #region Internal_static_methods_for_get_and_set

        public static string GetWDNData(string datetime)
        {
            // string datetime = (DateTime.Now - new TimeSpan(0, 1, 0)).ToString("yyyy-MM-dd HH:mm:ss.fff");
            string Data = "";
            var client = new RestClient("http://amcwdnchetas.dyndns.org:8444//WDN_WIMS_PARA.asmx");
            // var client = new RestClient("http://103.97.243.80:8444/WDN_WIMS_PARA.asmx");
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "8468fbc3-475b-ea8d-89fc-b986896d6c68");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "text/xml");
            request.AddParameter("text/xml", "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n  <soap:Body>\r\n    <WDN_WIMS_Parameters xmlns=\"http://tempuri.org/\">\r\n      <Date>" + datetime + "</Date>\r\n    </WDN_WIMS_Parameters>\r\n  </soap:Body>\r\n</soap:Envelope>", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            Data = response.Content;
            int index = -1;
            if (Data.Contains("<?xml"))
            {
                index = Data.IndexOf("<?xml");
            }
            if (index >= 0)
            {
                Data = Data.Substring(0, index).Trim();
            }
            return Data;
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        public static void SetWDNData(DataTable WDNData)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "Sync_WDN_Data";// "AMC_WDN";
                    cmd.Parameters.Clear();
                    cmd.CommandTimeout = 32760;
                    cmd.Parameters.Add("@tblWDNTableVariable", SqlDbType.Structured).Value = WDNData;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception er)
            {

            }
        }

        public static string GetAlarmData(string datetime)
        {
            // string datetime = (DateTime.Now - new TimeSpan(0, 1, 0)).ToString("yyyy-MM-dd HH:mm:ss.fff");
            string Data = "";
            //var client = new RestClient("http://103.97.243.80:8444/WDN_WIMS_PARA.asmx");
            var client = new RestClient("http://amcwdnchetas.dyndns.org:8444//WDN_WIMS_PARA.asmx");
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "8468fbc3-475b-ea8d-89fc-b986896d6c68");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "text/xml");
            request.AddParameter("text/xml", "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n  <soap:Body>\r\n    <WDN_WIMS_ALARMS_EVENTS xmlns=\"http://tempuri.org/\">\r\n      <Date>" + datetime + "</Date>\r\n    </WDN_WIMS_ALARMS_EVENTS>\r\n  </soap:Body>\r\n</soap:Envelope>", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            Data = response.Content;
            int index = -1;
            if (Data.Contains("<?xml"))
            {
                index = Data.IndexOf("<?xml");
            }
            if (index >= 0)
            {
                Data = Data.Substring(0, index).Trim();
            }
            return Data;
        }

        public static void SetAlarmData(DataTable WDNData)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "AMC_WDN_WIMS_ALARMS_EVENTS";// "AMC_WDN";
                    cmd.Parameters.Clear();
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.Add("@tblWDNTableVariable", SqlDbType.Structured).Value = WDNData;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception er)
            {

            }
        }

        public static string GetPumpRunData(string datetime)
        {

            string Data = "";
            //  var client = new RestClient("http://103.97.243.80:8444/WDN_WIMS_PARA.asmx");
            var client = new RestClient("http://amcwdnchetas.dyndns.org:8444//WDN_WIMS_PARA.asmx");
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "8468fbc3-475b-ea8d-89fc-b986896d6c68");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "text/xml");
            request.AddParameter("text/xml", "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n  <soap:Body>\r\n    <WDS_WIMS_PUMP_RUNNING_STATUS xmlns=\"http://tempuri.org/\">\r\n      <Date>" + datetime + "</Date>\r\n    </WDS_WIMS_PUMP_RUNNING_STATUS>\r\n  </soap:Body>\r\n</soap:Envelope>", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            Data = response.Content;
            int index = -1;
            if (Data.Contains("<?xml"))
            {
                index = Data.IndexOf("<?xml");
            }
            if (index >= 0)
            {
                Data = Data.Substring(0, index).Trim();
            }
            return Data;
        }

        public static void SetPumpRunData(DataTable WDNData)
        {
            try
            {

                using (SqlConnection con = new SqlConnection(ConnectionString))
                {

                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "Sync_WIMS_PUMP_RUNNING_STATUS";// "AMC_WDN";
                    cmd.Parameters.Clear();
                    cmd.CommandTimeout = 200;
                    cmd.Parameters.Add("@tblWDNTableVariable", SqlDbType.Structured).Value = WDNData;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                }


            }
            catch (Exception er)
            {

            }
        }

        public static string GetWDSWTPData(string DateAndTime)
        {
            string Data = "";

            var client = new RestClient("http://182.74.132.190:8222/AMCWIMSPARAMETERS.asmx/AMCWIMSPARAMETER?Date=" + DateAndTime + "");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);

            Data = response.Content;
            int index = -1;
            if (Data.Contains("<?xml"))
            {
                index = Data.IndexOf("<?xml");
            }
            if (index >= 0)
            {
                Data = Data.Substring(0, index).Trim();
            }

            return Data;
        }

        public static void Set_WDS_WTP_Data(DataTable WDSData, DateTime SyncDateTime)
        {
            try
            {

                using (SqlConnection con = new SqlConnection(ConnectionString))
                {

                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "AMC_WDS_New";
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@tblWDSTableVariable", SqlDbType.Structured).Value = WDSData;
                    cmd.Parameters.Add("@DataReceivedDateAndTime", SqlDbType.DateTime).Value = SyncDateTime;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                }


            }
            catch (Exception er)
            {

            }
        }
        #endregion


        #region Synchronization_Methods


        public bool SyncWDNData(string startTime, string endTime)
        {
            bool isSync = false;
            try
            {
                DateTime start = Convert.ToDateTime(startTime);
                DateTime end = Convert.ToDateTime(endTime);
                while (start <= end)
                {

                    List<WDNSyncModel> WDNData = new List<WDNSyncModel>();
                    string datetime = start.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    string Data = GetWDNData(datetime);
                    Newtonsoft.Json.JsonSerializer s = new Newtonsoft.Json.JsonSerializer();
                    if (Data != "[]")
                    {
                        WDNData = s.Deserialize<List<WDNSyncModel>>(new JsonTextReader(new StringReader(Data)));

                        DataTable tblWDNData = ToDataTable(WDNData.ToList());
                        if (WDNData.Any())
                        {
                            SetWDNData(tblWDNData);
                        }
                    }

                    start = (start + new TimeSpan(0, 1, 0));
                }
                isSync = true;
            }
            catch (Exception ex)
            {
            }
            return isSync;
        }

        public bool SyncPumpData(string startTime, string endTime)
        {
            bool isSync = false;
            try
            {
                DateTime start = Convert.ToDateTime(startTime);
                DateTime end = Convert.ToDateTime(endTime);
                while (start <= end)
                {

                    List<WDNSyncModel> WDNData = new List<WDNSyncModel>();
                    string datetime = start.ToString("yyyy-MM-dd HH:mm:ss.fff");


                    string PumpRunData = GetPumpRunData(datetime);
                    Newtonsoft.Json.JsonSerializer Pump_serialise = new Newtonsoft.Json.JsonSerializer();
                    if (PumpRunData != "[]")
                    {
                        WDNData.Clear();
                        WDNData = Pump_serialise.Deserialize<List<WDNSyncModel>>(new JsonTextReader(new StringReader(PumpRunData)));

                        DataTable tblPumpData = ToDataTable(WDNData.ToList());
                        if (WDNData.Any())
                        {
                            SetPumpRunData(tblPumpData);
                        }
                    }

                    start = (start + new TimeSpan(0, 1, 0));
                }
                isSync = true;
            }
            catch (Exception ex)
            {
            }
            return isSync;
        }

        public bool SyncAlarmData(string startTime, string endTime)
        {
            bool isSync = false;
            try
            {
                DateTime start = Convert.ToDateTime(startTime);
                DateTime end = Convert.ToDateTime(endTime);
                while (start <= end)
                {

                    List<WDNSyncModel> WDNData = new List<WDNSyncModel>();
                    string datetime = start.ToString("yyyy-MM-dd HH:mm:ss.fff");


                    string AlarmData = GetAlarmData(datetime);
                    Newtonsoft.Json.JsonSerializer alarm_serialise = new Newtonsoft.Json.JsonSerializer();
                    if (AlarmData != "[]")
                    {
                        WDNData.Clear();
                        WDNData = alarm_serialise.Deserialize<List<WDNSyncModel>>(new JsonTextReader(new StringReader(AlarmData)));

                        DataTable tblalarmData = ToDataTable(WDNData.ToList());
                        if (WDNData.Any())
                        {
                            SetAlarmData(tblalarmData);
                        }
                    }


                    start = (start + new TimeSpan(0, 1, 0));
                }
                isSync = true;
            }
            catch (Exception ex)
            {
            }
            return isSync;
        }

        public bool Sync_WDS_WTPData(string startTime, string endTime)
        {
            bool isSync = false;
            try
            {
                DateTime start = Convert.ToDateTime(startTime);
                DateTime end = Convert.ToDateTime(endTime);
                while (start <= end)
                {

                    List<WDS_WTP_Model> WDS_Data = new List<WDS_WTP_Model>();
                    string datetime = start.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    string WDS_WTP_Data = GetWDSWTPData(datetime);

                    Newtonsoft.Json.JsonSerializer wds_serialise = new Newtonsoft.Json.JsonSerializer();
                    if (WDS_WTP_Data != "[]")
                    {
                        WDS_Data.Clear();
                        WDS_Data = wds_serialise.Deserialize<List<WDS_WTP_Model>>(new JsonTextReader(new StringReader(WDS_WTP_Data)));

                        DataTable tblWDSData = ToDataTable(WDS_Data.ToList());
                        if (WDS_Data.Any())
                        {
                            Set_WDS_WTP_Data(tblWDSData, start);
                        }
                    }


                    start = (start + new TimeSpan(0, 15, 0));
                }
                isSync = true;
            }
            catch (Exception ex)
            {
            }
            return isSync;
        }

        public List<LDQCustomSettingModel> GetWTP_WDS_LDQValues()
        {
            List<LDQCustomSettingModel> WTP_WDS_LDQValuesList = new List<LDQCustomSettingModel>();
            try
            {
                using (_db = new AMCDBContext())
                {

                    WTP_WDS_LDQValuesList = _db.Database.SqlQuery<LDQCustomSettingModel>("EXEC GetWDS_WTPLDQCustomValues").ToList();
                }

            }
            catch (Exception ex)
            {
            }

            return WTP_WDS_LDQValuesList;
        }

        public List<WaterQualitySettingModel> GetWaterQualityValues()
        {
            List<WaterQualitySettingModel> waterQualities = new List<WaterQualitySettingModel>();
            try
            {
                using (_db = new AMCDBContext())
                {

                    waterQualities = _db.Database.SqlQuery<WaterQualitySettingModel>("EXEC GetWaterQualityValues").ToList();
                }

            }
            catch (Exception ex)
            {
            }

            return waterQualities;
        }


        public List<AlertMailConfigurationModel> GetAlertMailConfigValues()
        {
            List<AlertMailConfigurationModel> values = new List<AlertMailConfigurationModel>();
            try
            {
                using (_db = new AMCDBContext())
                {

                    values = _db.Database.SqlQuery<AlertMailConfigurationModel>("EXEC GetAlertMailConfigValues").ToList();
                }

            }
            catch (Exception ex)
            {
            }

            return values;
        }

        public List<WDNReportConfigurationModel> GetWDNReportConfigValues(string Type)
        {
            List<WDNReportConfigurationModel> values = new List<WDNReportConfigurationModel>();
            try
            {
                using (_db = new AMCDBContext())
                {

                    values = _db.Database.SqlQuery<WDNReportConfigurationModel>("EXEC GetWdnReportConfigValues @Type={0}", Type).ToList();
                }

            }
            catch (Exception ex)
            {
            }

            return values;
        }
        #endregion


        #region UPDATE
        public bool SetCustomLDQValue(string PropertyName, string value)
        {
            bool isSaved = false;
            try
            {
                using (_db = new AMCDBContext())
                {

                    isSaved = _db.Database.SqlQuery<bool>("EXEC ChangeWDS_WTPLDQCustomValues @PropertyName={0}, @value={1}", PropertyName, Convert.ToDecimal(value)).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return isSaved;
        }

        public bool SetWaterQualityValue(string PropertyName, string Min, string Max)
        {
            bool isSaved = false;
            try
            {
                using (_db = new AMCDBContext())
                {

                    isSaved = _db.Database.SqlQuery<bool>("EXEC ChangeWaterQualityValues @PropertyName={0}, @Min={1}, @Max={2}", PropertyName, Convert.ToDecimal(Min), Convert.ToDecimal(Max)).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return isSaved;
        }

        public bool SetWaterQualityAnalysisTime(string time)
        {
            bool isSaved = false;
            try
            {
                using (_db = new AMCDBContext())
                {

                    isSaved = _db.Database.SqlQuery<bool>("EXEC ChangeWaterQualityAnalysisTime @Time={0}", time).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return isSaved;
        }

        public bool SetAlertMailConfigValue(int Id, string EastMailIds, string WestMailIds, string SouthMailIds, string NorthMailIds, string EmailSubject, DateTime MailTime)
        {
            bool isSaved = false;
            try
            {
                using (_db = new AMCDBContext())
                {
                    isSaved = _db.Database.SqlQuery<bool>("EXEC ChangeAlertMailConfigValues @Id={0},@EastMailIds={1}, @WestMailIds={2}, @SouthMailIds={3}, @NorthMailIds={4}, @EmailSubject={5}, @MailTime={6}", Id, EastMailIds, WestMailIds, SouthMailIds, NorthMailIds, EmailSubject, MailTime).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return isSaved;
        }


        public bool SetWDNReportConfigValue(int Id, string MailIds, string EmailSubject)
        {
            bool isSaved = false;
            try
            {
                using (_db = new AMCDBContext())
                {
                    isSaved = _db.Database.SqlQuery<bool>("EXEC ChangeWdnReportConfigValues @Id={0},@MailIds={1},@EmailSubject={2}", Id, MailIds, EmailSubject).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return isSaved;
        }

        public bool SetAlertMailConfigurationSetting(string alertname, string zone, string mailtime, string mailto, string mailcc, string mailbcc, string subject)
        {
            bool isSaved = false;
            try
            {
                using (_db = new AMCDBContext())
                {
                    isSaved = _db.Database.SqlQuery<bool>("EXEC InsertOrUpdateAlertMailConfigurationDetails @alertname={0},@zone={1},@mailtime={2},@mailto={3},@mailcc={4},@mailbcc={5},@subject={6}", alertname, zone, mailtime, mailto, mailcc, mailbcc, subject).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return isSaved;
        }

        public List<AlertMailConfigurationSettingModel> GetAlertMailConfigurationSetting(string alertname, string zone)
        {
            List<AlertMailConfigurationSettingModel> values = new List<AlertMailConfigurationSettingModel>();
            try
            {
                using (_db = new AMCDBContext())
                {

                    values = _db.Database.SqlQuery<AlertMailConfigurationSettingModel>("EXEC GetAlertMailDetailsByNameAndZone @AlertName={0},@Zone={1}", alertname, zone).ToList();
                }

            }
            catch (Exception ex)
            {
            }

            return values;
        }
        #endregion
    }
}