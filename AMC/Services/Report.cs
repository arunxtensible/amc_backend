﻿using AMC.DBContext;
using AMC.IServices;
using AMC.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AMC.Services
{
    public class Report : IReport
    {
        AMCDBContext _db = new AMCDBContext();
        public static string ConnectionString = ConfigurationManager.ConnectionStrings["MyConnString"].ConnectionString;

        public List<ReportDataModelHourly> GetWDNReportByType(string Zone, string Ward, string Location, string ReportType, string Datetime)
        {

            List<ReportDataModelHourly> reportData = new List<ReportDataModelHourly>();

            try
            {

                using (_db = new AMCDBContext())
                {
                    _db.Database.CommandTimeout = 600;
                    reportData = _db.Database.SqlQuery<ReportDataModelHourly>("EXEC GetWDNMonthlyOrHourlyReport @Zone={0},@Ward={1},@Location={2},@ReportType={3},@Datetime={4}", Zone, Ward, Location, ReportType,Datetime).ToList();

                }


            }
            catch (Exception ex)
            {
            }

            return reportData;
        }
        public List<ReportDataModelDB2DBDataHourly> GetWDNReportByDb2DBReader( string Location,  string Datetime)
        {

            List<ReportDataModelDB2DBDataHourly> reportData = new List<ReportDataModelDB2DBDataHourly>();

            try
            {

                

                using (DataReaderDbContext _dbRemote = new DataReaderDbContext())
                {
                    _dbRemote.Database.CommandTimeout = 100;
                    //from Harsha Report SP
                    reportData = _dbRemote.Database.SqlQuery<ReportDataModelDB2DBDataHourly>("EXEC AMC_REPORTNEWtEST @Date={0},@EndDate={1},@SiteName={2}", Convert.ToDateTime(Datetime), null, Location).ToList();
                    
                }

                List<ReportDataModelDB2DBDataHourly> reportData1 = new List<ReportDataModelDB2DBDataHourly>();
                using (_db = new AMCDBContext())
                {
                    
                    _db.Database.CommandTimeout = 100;
                    //from Harsha Report SP
                    reportData1 = _db.Database.SqlQuery<ReportDataModelDB2DBDataHourly>("EXEC GetHourlyWDNReport @Date={0},@Location={1}",Datetime,  Location).ToList();

                }
                if (reportData1.Any())
                {
                    reportData.AddRange(reportData1);
                }

            }
            catch (Exception ex)
            {
            }

            return reportData.OrderByDescending(o => o.Timestamp).ToList();
        }

        public List<ReportDataModelMonthly> GetWDNReportByTypeMonthly(string Zone, string Ward, string Location, string ReportType, string Datetime)
        {
            List<ReportDataModelMonthly> reportData = new List<ReportDataModelMonthly>();

            try
            {



                using (DataReaderDbContext _dbRemote = new DataReaderDbContext())
                {
                    _dbRemote.Database.CommandTimeout = 100;
                    //from Harsha Report SP
                    reportData = _dbRemote.Database.SqlQuery<ReportDataModelMonthly>("EXEC GetMonthlyLDQPressureReport @dateAndtime={0},@LocationName={1}", Convert.ToDateTime(Datetime),  Location).ToList();
                }
               
                //using (DataReaderDbContext _dbRemote = new DataReaderDbContext())
                //{
                //    _dbRemote.Database.CommandTimeout = 600;
                //    //from Harsha Report SP
                //    reportData = _dbRemote.Database.SqlQuery<ReportDataModelMonthly>("EXEC sp_SiteVolumeReportFOR_Pressure @datetime={0},@locName={1}", Convert.ToDateTime(Datetime), Location).ToList();
                //}

            }
            catch (Exception ex)
            {
            }

            return reportData;

            //List<ReportDataModelMonthly> reportData = new List<ReportDataModelMonthly>();

            //try
            //{
            //    using (_db = new AMCDBContext())
            //    {
            //        _db.Database.CommandTimeout = 600;
            //        reportData = _db.Database.SqlQuery<ReportDataModelMonthly>("EXEC GetWDNMonthlyOrHourlyReport @Zone={0},@Ward={1},@Location={2},@ReportType={3},@Datetime={4}", Zone, Ward, Location, ReportType, Datetime).ToList();
            //    }

            //}
            //catch (Exception ex)
            //{
            //}

            //return reportData;
        }

        public List<WDNLdqReportDataModel> GetWDNLdqReport(string Datetime)
        {
            List<WDNLdqReportDataModel> reportData = new List<WDNLdqReportDataModel>();
           
            try
            {
                using (_db = new AMCDBContext())
                {
                    _db.Database.CommandTimeout = 600;
                    reportData = _db.Database.SqlQuery<WDNLdqReportDataModel>("EXEC GetWDNLdqReport @DateAndtime={0}", Datetime).OrderByDescending(o=>o.LDQ).ToList();
                }

            }
            catch (Exception ex)
            {
            }

            return reportData;
        }


        public List<WDSLdqReportDataModel> GetWDSLdqReport(string Datetime)
        {
            List<WDSLdqReportDataModel> reportData = new List<WDSLdqReportDataModel>();

            try
            {
                using (_db = new AMCDBContext())
                {
                    _db.Database.CommandTimeout = 600;
                    reportData = _db.Database.SqlQuery<WDSLdqReportDataModel>("EXEC GetWDSLdqReport @DateAndtime={0}", Datetime).ToList();
                }

            }
            catch (Exception ex)
            {
            }

            return reportData;
        }


        
        public List<ReportDataModelWDSDetailsWithParentWTP> GetZonewiseWDSLDqReport(string Zone, string Datetime)
        {
            List<ReportDataModelWDSDetailsWithParentWTP> reportData = new List<ReportDataModelWDSDetailsWithParentWTP>();

            try
            {
                using (_db = new AMCDBContext())
                {
                    _db.Database.CommandTimeout = 600;
                    reportData = _db.Database.SqlQuery<ReportDataModelWDSDetailsWithParentWTP>("EXEC Report_WDSDetailsWithParentWTP @Zone={0},@Datetime={1}", Zone, Datetime).ToList();
                }

            }
            catch (Exception ex)
            {
            }

            return reportData;
        }

        public List<ReportDataModelWDNDetailsWithParentWDS> GetWDNReportWithParentWDSDetails(string Zone, string ParentWDS, string Ward, string Location, string Datetime)
        {
            List<ReportDataModelWDNDetailsWithParentWDS> reportData = new List<ReportDataModelWDNDetailsWithParentWDS>();

            try
            {

                using (_db = new AMCDBContext())
                {
                    _db.Database.CommandTimeout = 600;
                    reportData = _db.Database.SqlQuery<ReportDataModelWDNDetailsWithParentWDS>("EXEC Report_WDNWithParentWDSDetails @Zone={0},@ParentWDS={1},@Ward={2},@Location={3},@Datetime={4}", Zone,ParentWDS, Ward, Location, Datetime).ToList();

                }


            }
            catch (Exception ex)
            {
            }

            return reportData;
        }

    }
}