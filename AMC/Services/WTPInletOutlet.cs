﻿using AMC.DBContext;
using AMC.IServices;
using AMC.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AMC.Services
{
    public class WTPInletOutlet: IWTPInletOutlet
    {
        public  string ConnectionString = ConfigurationManager.ConnectionStrings["MyConnString"].ConnectionString;
        AMCDBContext _db = new AMCDBContext();
        Mapper map = null;
        public WTPInletOutlet()
        {
            //MapperConfiguration config = new MapperConfiguration(cfg => {
            //    cfg.CreateMap<ZoneMaster, ZoneModel>();
            //    cfg.CreateMap<ZoneModel, ZoneMaster>();
            //});
            //map = new Mapper(config);
        }
       

        List<WTPInletOutletModel> IWTPInletOutlet.GetAllInlet(string Type, string searchVal)
        {
            List<WTPInletOutletModel> WTPOutlet = new List<WTPInletOutletModel>();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {

                DataTable dt = new DataTable();


                SqlCommand cmd = new SqlCommand("GetWTPInletOutlet", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Type", SqlDbType.VarChar,100).Value = Type;
                if(searchVal!=null)
                    cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100).Value = searchVal;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    WTPInletOutletModel obj = new WTPInletOutletModel();

                    obj.WTPName = (dr["WTPName"]).ToString();
                    obj.Capacity = Math.Round(Convert.ToDecimal(Convert.ToDecimal(dr["Capacity"])), 2);
                    obj.CDQ = Math.Round(Convert.ToDecimal(Convert.ToDecimal(dr["CDQ"])), 2);
                    obj.LDQ = Math.Round(Convert.ToDecimal(Convert.ToDecimal(dr["LDQ"])), 2);
                    obj.UnitConsumption = Math.Round(Convert.ToDecimal(Convert.ToDecimal(dr["UnitConsumption"])), 2);
                    obj.WTPId = Convert.ToInt32(dr["WTPId"]);
                    obj.LDRDateTime = Convert.ToDateTime(dr["LDRDateTime"]);
                    WTPOutlet.Add(obj);
                }



            }
            return WTPOutlet;
        }

        List<WTPInletOutletDetailModel> IWTPInletOutlet.WTPInletOutletDetailByWTPName(Int32 WTPId, string Type)
        {
            List<WTPInletOutletDetailModel> WTPInlet = new List<WTPInletOutletDetailModel>();
            try
            {
                using (_db = new AMCDBContext())
                {
                    WTPInlet = _db.Database.SqlQuery<WTPInletOutletDetailModel>("GetWTPInletOutletDetails @WTPId={0},@Type={1}", WTPId, Type).ToList();

                }
            }
            catch (Exception ex)
            {

            }
            
            //using (SqlConnection con = new SqlConnection(ConnectionString))
            //{

            //    DataTable dt = new DataTable();


            //    SqlCommand cmd = new SqlCommand("GetWTPInletOutletDetails", con);
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    cmd.Parameters.Add("@WTPId", SqlDbType.Int).Value = WTPId;
            //    cmd.Parameters.Add("@Type", SqlDbType.VarChar,10).Value = Type;
            //    SqlDataAdapter da = new SqlDataAdapter(cmd);
            //    da.Fill(dt);

            //    foreach (DataRow dr in dt.Rows)
            //    {
            //        WTPInletOutletDetailModel obj = new WTPInletOutletDetailModel();

            //        obj.InletFlow = Convert.ToDecimal(dr["InletFlow"]);
            //        obj.SumpLevel = Convert.ToDecimal(dr["SumpLevel"]);
            //        obj.OutletFlowRate = Convert.ToDecimal(dr["OutletFlowRate"]);
            //        obj.Pressure = Convert.ToDecimal(dr["Pressure"]);
            //        obj.SumpCapacity = Convert.ToDecimal(dr["SumpCapacity"]);

            //        obj.PumpStatus = (dr["PumpStatus"]).ToString();
            //        obj.PumpDesign = (dr["PumpDesign"]).ToString();
            //        obj.PumpRun = (dr["PumpRun"]).ToString();
            //        obj.IntakeSource = (dr["IntakeSource"]).ToString();

            //        obj.KPI_Current= (dr["KPI_Current"]).ToString();
            //        obj.PeakLevelMTR_Current = Convert.ToDecimal(dr["PeakLevelMTR_Current"]);
            //        obj.InletVolume_Current = Convert.ToDecimal(dr["InletVolume_Current"]);
            //        obj.OutletVolume_Current = Convert.ToDecimal(dr["OutletVolume_Current"]);
            //        obj.PeakPressure_Current = Convert.ToDecimal(dr["PeakPressure_Current"]);
            //        obj.TotalPumpingHour_Current = Convert.ToDecimal(dr["TotalPumpingHour_Current"]);
            //        obj.UnitConsumption_Current = Convert.ToDecimal(dr["UnitConsumption_Current"]);
            //        obj.KWHM3_Current = Convert.ToDecimal(dr["KWHM3_Current"]);

            //        obj.KPI_prev= (dr["KPI_prev"]).ToString();
            //        obj.PeakLevelMTR_prev = Convert.ToDecimal(dr["PeakLevelMTR_prev"]);
            //        obj.InletVolume_prev = Convert.ToDecimal(dr["InletVolume_prev"]);
            //        obj.OutletVolume_prev = Convert.ToDecimal(dr["OutletVolume_prev"]);
            //        obj.PeakPressure_prev = Convert.ToDecimal(dr["PeakPressure_prev"]);
            //        obj.TotalPumpingHour_prev = Convert.ToDecimal(dr["TotalPumpingHour_prev"]);
            //        obj.UnitConsumption_prev = Convert.ToDecimal(dr["UnitConsumption_prev"]);
            //        obj.KWHM3_prev = Convert.ToDecimal(dr["KWHM3_prev"]);
            //        obj.LDR_DateTime = Convert.ToDateTime(dr["LDR_DateTime"]).ToString("dd/MM/yyyy hh:mm tt");
                                       

            //        WTPInlet.Add(obj);
            //    }



            //}
            return WTPInlet;
        }
    }
}