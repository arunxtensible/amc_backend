﻿using AMC.IServices;
using AMC.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AMC.Services
{
    public class WardRepository : IWard
    {
        public string ConnectionString = ConfigurationManager.ConnectionStrings["MyConnString"].ConnectionString;
        Mapper map = null;
        public WardRepository()
        {
            MapperConfiguration config = new MapperConfiguration(cfg => {
                cfg.CreateMap<WardMaster, WardModel>();
                cfg.CreateMap<WardModel, WardMaster>();
            });
            map = new Mapper(config);
        }

        public List<WardModel> GetAllWard()
        {
            using (var ctx = new AMCEntities())
            {
                var WardList = ctx.WardMasters.OrderByDescending(o => o.WardID).ToList();
                List<WardModel> _Ward = map.Map<List<WardModel>>(WardList);
                return _Ward;
            }
        }


        public List<WardModel> GetAllWards()
        {
            List<WardModel> WDSInlet = new List<WardModel>();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {

                DataTable dt = new DataTable();

                SqlCommand cmd = new SqlCommand("GetAllWards", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    WardModel obj = new WardModel();

                    obj.WardID = Convert.ToInt32((dr["WardID"]));
                    obj.WardName = (dr["WardName"]).ToString();

                    WDSInlet.Add(obj);
                }
            }

            return WDSInlet;

        }


        public WardModel GetWard(int id)
        {
            using (var ctx = new AMCEntities())
            {
                var _WardName = ctx.WardMasters.Find(id);
                if (_WardName != null)
                {
                    WardModel _Ward = map.Map<WardModel>(_WardName);
                    return _Ward;
                }
                return null;
            }
        }
        public WardModel Add(WardModel model)
        {
            using (var ctx = new AMCEntities())
            {
                var _Ward = new WardMaster();
                _Ward.WardName = model.WardName;
                ctx.WardMasters.Add(_Ward);
                ctx.SaveChanges();
                model.WardID = _Ward.WardID;
                return model;
            }
        }
        public List<WardModel> GetAllWards(int Id)
        {
            //using (var ctx = new AMCEntities())
            //{
            //    var WardList = ctx.WardMasters.OrderByDescending(o => o.WardID).ToList();
            //    List<WardModel> _Zones = map.Map<List<WardModel>>(WardList);
            //    return _Zones;
            //}

            List<WardModel> _wards = new List<WardModel>();


            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                DataTable dt = new DataTable();

              //  SqlCommand cmd = new SqlCommand("GetAllWardsByZoneId", con);
                SqlCommand cmd = new SqlCommand("GetAllWardsByWardId", con);
                cmd.CommandType = CommandType.StoredProcedure;
               // cmd.Parameters.Add("@InZoneId", SqlDbType.Int).Value = Id;
                cmd.Parameters.Add("@InWardId", SqlDbType.Int).Value = Id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    WardModel obj = new WardModel();

                    obj.WardID = Convert.ToInt32(dr["WardID"]);
                    obj.WardName = (dr["WardName"]).ToString();

                    _wards.Add(obj);
                }

            }
            return _wards;
        }

        public int GetZoneIdByWardId(int Id)
        {
            int ZoneId = 0;
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("SELECT top 1 ZoneId FROM WardMaster where WardId=" + Id, con))
                    {
                        using (IDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                ZoneId = Convert.ToInt32(dr["ZoneId"]);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                ZoneId = 0;
            }

            return ZoneId;
        }
    }
}