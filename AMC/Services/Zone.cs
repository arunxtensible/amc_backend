﻿using AMC.DBContext;
using AMC.IServices;
using AMC.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AMC.Services
{
    public class Zone : IZone
    {
        AMCDBContext _db = new AMCDBContext();
        public string ConnectionString = ConfigurationManager.ConnectionStrings["MyConnString"].ConnectionString;
        public const string GetAllDashboardAlertsData = "GetAllDashboardAlertsData";

        public const string GetWTPGroundWaterData = "GetWTPGroundWaterData";
        Mapper map = null;
        public Zone()
        {
            MapperConfiguration config = new MapperConfiguration(cfg => {
                cfg.CreateMap<ZoneMaster, ZoneModel>();
                cfg.CreateMap<ZoneModel, ZoneMaster>();
            });
            map = new Mapper(config);
        }

        public List<ZoneModel> GetAllZones()
        {          
            List<ZoneModel> _Zones = new List<ZoneModel>();

            try
            {
                using (_db = new AMCDBContext())
                {

                    _Zones = _db.Database.SqlQuery<ZoneModel>("EXEC GetAllZones").ToList();
                }
            }
            catch (Exception ex)
            {
                _Zones = null;
            }           
               
                return _Zones;
           
        }

        public ZoneModel Add(ZoneModel model)
        {
            using (var ctx = new AMCEntities())
            {
                var _Zone = new ZoneMaster();
                _Zone.ZoneName = model.ZoneName;
                _Zone.HintText = model.HintText;
                ctx.ZoneMasters.Add(_Zone);
                ctx.SaveChanges();
                model.ZoneID = _Zone.ZoneID;
                return model;
            }
        }

        public bool Update(ZoneModel model)
        {
            using (var ctx = new AMCEntities())
            {
                var _Zone = ctx.ZoneMasters.Find(model.ZoneID);
                if (_Zone != null)
                {
                    _Zone.ZoneName = model.ZoneName;
                    _Zone.HintText = model.HintText;
                    ctx.Entry(_Zone).State = EntityState.Modified;
                    ctx.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        // Hard Delete 
        public bool Delete(int id)
        {
            using (var ctx = new AMCEntities())
            {
                var _Zone = ctx.ZoneMasters.Find(id);
                if (_Zone != null)
                {
                    ctx.ZoneMasters.Remove(_Zone);
                    ctx.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public ZoneModel GetZone(int id)
        {
            using (var ctx = new AMCEntities())
            {
                var _ZoneName = ctx.ZoneMasters.Find(id);
                if (_ZoneName != null)
                {
                    ZoneModel _Zone = map.Map<ZoneModel>(_ZoneName);
                    return _Zone;
                }
                return null;
            }
        }

        public List<DashboardAlertsModel> GetDashboardAlertData()
        {
            List<DashboardAlertsModel> AlertsData = new List<DashboardAlertsModel>();
            using (var ctx = new AMCEntities())
            {
                AlertsData = ctx.Database.SqlQuery<DashboardAlertsModel>(GetAllDashboardAlertsData).ToList();
            }
            return AlertsData;
        }

        public List<WTPGroundWaterDataModel> GetAllWTPGroundWater()
        {
            List<WTPGroundWaterDataModel> _Wtp = new List<WTPGroundWaterDataModel>();

            using (var ctx = new AMCEntities())
            {
                _Wtp = ctx.Database.SqlQuery<WTPGroundWaterDataModel>(GetWTPGroundWaterData).ToList();
            }
            return _Wtp;

        }
    }
}