﻿using AMC.DBContext;
using AMC.IServices;
using AMC.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AMC.Services
{
    public class DataRepository : IData
    {
        public string ConnectionString = ConfigurationManager.ConnectionStrings["MyConnString"].ConnectionString;

        AMCDBContext _db = new AMCDBContext();

        public const string Tampringdata = "GetTampringAlertsData @Type={0}";

        public List<DashboardGraphsModel> GetDashboardGraphData()
        {
            List<DashboardGraphsModel> Graphdata = new List<DashboardGraphsModel>();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                     con.Open();               
                    DataTable dt = new DataTable();
                    SqlCommand cmd = new SqlCommand("GetAllDashboardGrapData", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 1600;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);

                    foreach (DataRow dr in dt.Rows)
                    {
                        DashboardGraphsModel obj = new DashboardGraphsModel();
                        obj.PropertyName = (dr["PropertyName"]).ToString();
                        obj.Capacity = Convert.ToDecimal(dr["Capacity"]);
                        obj.CDQ = Convert.ToDecimal(dr["CDQ"]);
                        obj.LDQ = Convert.ToDecimal(dr["LDQ"]);
                        obj.LDRDate = (dr["LDRDate"]).ToString();
                        Graphdata.Add(obj); 
                    }

            }
            return Graphdata;
        }

        public List<WardMaster> GetWardByZoneId(int ZoneId)
        {
            List<WardMaster> wardList = new List<WardMaster>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT [WardId],[WardName]FROM WardMaster where ZoneId="+ ZoneId + " Order by WardId asc", con))
                {

                    cmd.CommandTimeout = 1600;
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            WardMaster obj = new WardMaster();
                            obj.WardID = Convert.ToInt32(dr["WardID"]);
                            obj.WardName = (dr["WardName"]).ToString();
                            wardList.Add(obj);
                        }
                    }
                }
            }
            return wardList;
        }

        public IEnumerable<WTP_Master> GetWTPList()
        {
            List<WTP_Master> WTPList = new List<WTP_Master>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT [WTPId],[WTPName]FROM WTP_Master where IsDeleted = 0 Order by WTPId asc", con))
                {
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            WTP_Master obj = new WTP_Master();
                            obj.WTPId = Convert.ToInt32(dr["WTPId"]);
                            obj.WTPName = (dr["WTPName"]).ToString();
                            WTPList.Add(obj);
                        }
                    }
                }
            }
            return WTPList;
        }
        public IEnumerable<WDSMaster> GetWDSList(int ZoneId)
        {
            List<WDSMaster> WDSList = new List<WDSMaster>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT [WDSId],[WDSName]FROM WDSMaster where ZoneId =" + ZoneId+" Order by WDSId asc", con))
                {
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            WDSMaster obj = new WDSMaster();
                            obj.WDSId = Convert.ToInt32(dr["WDSId"]);
                            obj.WDSName = (dr["WDSName"]).ToString();
                            WDSList.Add(obj);
                        }
                    }
                }
            }
            return WDSList;
        }
        IEnumerable<ZoneMaster> IData.GetZones()
        {
            List<ZoneMaster> ZoneList = new List<ZoneMaster>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT [ZoneId],[ZoneName]FROM ZoneMaster Order by ZoneId asc", con))
                {
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ZoneMaster obj = new ZoneMaster();
                            obj.ZoneID = Convert.ToInt32(dr["ZoneID"]);
                            obj.ZoneName = (dr["ZoneName"]).ToString();
                            ZoneList.Add(obj);
                        }
                    }
                }
            }
            return ZoneList;
        }
        IEnumerable<ZoneMaster> IData.GetWDNZones()
        {
            List<ZoneMaster> ZoneList = new List<ZoneMaster>();
            try
            {
                using (_db = new AMCDBContext())
                {
                    
                    ZoneList = _db.Database.SqlQuery<ZoneMaster>("EXEC GetWDNZones").ToList();
                }

            }
            catch (Exception ex)
            {
            }
            return ZoneList;
        }

        IEnumerable<ZoneMaster> IData.GetReportZones()
        {
            List<ZoneMaster> ZoneList = new List<ZoneMaster>();
            try
            {
                using (_db = new AMCDBContext())
                {

                    ZoneList = _db.Database.SqlQuery<ZoneMaster>("EXEC GetDDLForReport @Type={0}", "Zone").ToList();
                }

            }
            catch (Exception ex)
            {
            }
            return ZoneList;
        }

        IEnumerable<ReportParentWDS> IData.GetReportParentWDS()
        {
            List<ReportParentWDS> List = new List<ReportParentWDS>();
            try
            {
                using (_db = new AMCDBContext())
                {

                    List = _db.Database.SqlQuery<ReportParentWDS>("EXEC GetDDLForReport @Type={0}", "ParentWDS").ToList();
                }

            }
            catch (Exception ex)
            {
            }
            return List;
        }
        IEnumerable<ReportWard> IData.GetReportWard()
        {
            List<ReportWard> List = new List<ReportWard>();
            try
            {
                using (_db = new AMCDBContext())
                {

                    List = _db.Database.SqlQuery<ReportWard>("EXEC GetDDLForReport @Type={0}", "Ward").ToList();
                }

            }
            catch (Exception ex)
            {
            }
            return List;
        }
        IEnumerable<ReportWDNLocation> IData.GetReportWDNLocations()
        {
            List<ReportWDNLocation> List = new List<ReportWDNLocation>();
            try
            {
                using (_db = new AMCDBContext())
                {

                    List = _db.Database.SqlQuery<ReportWDNLocation>("EXEC GetDDLForReport @Type={0}", "Location").ToList();
                }

            }
            catch (Exception ex)
            {
            }
            return List;
        }

        IEnumerable<ReportParentWDS> IData.GetReportParentWDSByZone(string Zone)
        {
            List<ReportParentWDS> List = new List<ReportParentWDS>();
            try
            {
                using (_db = new AMCDBContext())
                {

                    List = _db.Database.SqlQuery<ReportParentWDS>("EXEC GetParentWDSByZone @Zone={0}", Zone).ToList();
                }

            }
            catch (Exception ex)
            {
            }
            return List;
        }
        IEnumerable<ReportWard> IData.GetReportWardByParentWDS(string ParentWDS)
        {
            List<ReportWard> List = new List<ReportWard>();
            try
            {
                using (_db = new AMCDBContext())
                {

                    List = _db.Database.SqlQuery<ReportWard>("EXEC GetWardByParentWDS @ParentWDS={0}", ParentWDS).ToList();
                }

            }
            catch (Exception ex)
            {
            }
            return List;
        }
        IEnumerable<ReportWDNLocation> IData.GetReportWDNLocationsByWard(string parentWDS,string Ward) 
        {
            List<ReportWDNLocation> List = new List<ReportWDNLocation>();
            try
            {
                using (_db = new AMCDBContext())
                {
                    if(parentWDS=="All")
                    List = _db.Database.SqlQuery<ReportWDNLocation>("EXEC GetWDNLocationsByWard @Ward={0}",Ward).ToList();
                    else
                    List = _db.Database.SqlQuery<ReportWDNLocation>("EXEC GetWDNLocationsByWard @Ward={0},@ParentWDS={1}", Ward,parentWDS).ToList();
                }

            }
            catch (Exception ex)
            {
            }
            return List;
        }


        public IEnumerable<ZoneMaster> GetZonesByType(string Type)
        {
            List<ZoneMaster> ZoneList = new List<ZoneMaster>();
            try
            {
                using (_db = new AMCDBContext())
                {

                    ZoneList = _db.Database.SqlQuery<ZoneMaster>("EXEC GetZonesByType @Type={0}",Type).ToList();
                }

            }
            catch (Exception ex)
            {
            }
            return ZoneList;
        }
        //public List<WDNMaster> GetWDNMaster(int zoneId, int wardId)
        //{

        //        List < WDNMaster > locaitonList = new List<WDNMaster>();

        //    using (SqlConnection con = new SqlConnection(ConnectionString))
        //    {
        //        con.Open();
        //        using (SqlCommand cmd = new SqlCommand($"select [wdnid], [Location] from dbo.WDNMaster where ZoneId ={zoneId} and WardId ={wardId}  Order by [wdnid] asc", con))
        //        {
        //            using (IDataReader dr = cmd.ExecuteReader())
        //            {
        //                while (dr.Read())
        //                {
        //                    WDNMaster obj = new WDNMaster();
        //                    obj.WDNID = Convert.ToInt32(dr["wdnid"]);
        //                    obj.Location = (dr["Location"]).ToString();
        //                    locaitonList.Add(obj);
        //                }
        //            }
        //        }
        //    }
        //    return locaitonList;

        //}

        public List<WDNMaster> GetWDNMaster(string zone, string ward)
        {

            List<WDNMaster> locaitonList = new List<WDNMaster>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand($"select [LocationName] from dbo.WDN_Locations_Today where Zone ='{zone}' and Ward ='{ward}'  Order by Ward asc", con))
                {
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            WDNMaster obj = new WDNMaster();
                            obj.WDNID = 0; //Convert.ToInt32(dr["wdnid"]);
                            obj.Location = (dr["LocationName"]).ToString();
                            locaitonList.Add(obj);
                        }
                    }
                }
                con.Close();
            }
            return locaitonList;

        }

        public List<WDNMaster> GetLocations(string zone)
        {

            List<WDNMaster> locaitonList = new List<WDNMaster>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand($"select WDSName as LocationName from dbo.WDSMaster where ZoneId ='{zone}'", con))
                {
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            WDNMaster obj = new WDNMaster();
                            obj.WDNID = 0; //Convert.ToInt32(dr["wdnid"]);
                            obj.Location = (dr["LocationName"]).ToString();
                            locaitonList.Add(obj);
                        }
                    }
                }
            }
            return locaitonList;

        }

        public List<WDSLocationModel> GetWDSLocationByZone(int zone)
        {

            List<WDSLocationModel> locaitonList = new List<WDSLocationModel>();
            
            try
            {
                using (_db = new AMCDBContext())
                {
                    locaitonList = _db.Database.SqlQuery<WDSLocationModel>("EXEC GetWDSLocationsByZone @ZoneId={0}", zone).ToList();
                }

            }
            catch (Exception ex)
            {
            }

            return locaitonList;

        }

        public DashboardAlertsModel GetDashboardAlertData()
        {
            DashboardAlertsModel AlertsData = new DashboardAlertsModel();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("GetAllDashboardAlertsData", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 1600;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                      
                foreach (DataRow dr in dt.Rows)
                {
                    //DashboardAlertsModel obj = new DashboardAlertsModel();
                    AlertsData.TamperingAlerts = Convert.ToDecimal(dr["TamperingAlerts"]);
                    AlertsData.WaterPressureAlerts = Convert.ToDecimal(dr["WaterPressureAlerts"]);
                    AlertsData.WaterQualityAlerts = Convert.ToDecimal(dr["WaterQualityAlerts"]);
                    AlertsData.PowerCutAlerts = Convert.ToDecimal(dr["PowerCutAlerts"]);
                    AlertsData.CommunicationAlerts = Convert.ToDecimal(dr["CommunicationAlerts"]);
                    AlertsData.FlowDeviiationAlerts = Convert.ToDecimal(dr["FlowDeviiationAlerts"]);
                    AlertsData.PumpingHourAlerts = Convert.ToDecimal(dr["PumpingHourAlerts"]);
                    AlertsData.ZeroFlowAlerts = Convert.ToDecimal(dr["ZeroFlowAlerts"]);
                    AlertsData.WaterVSEnergyAlerts = Convert.ToDecimal(dr["WaterVSEnergyAlerts"]);
                    AlertsData.BatteryVoltageAlerts = Convert.ToDecimal(dr["BatteryVoltageAlerts"]);
                    AlertsData.PowerCutAlerts = Convert.ToDecimal(dr["PowerCutAlerts"]);
                    AlertsData.WaterVSEnergy = Convert.ToDecimal(dr["WaterVSEnergy"]);
                    AlertsData.PopulationVSDemandCurrentDay = Convert.ToDecimal(dr["PopulationVSDemandCurrentDay"]);
                    AlertsData.PopulationVSDemandPreviousDay = Convert.ToDecimal(dr["PopulationVSDemandPreviousDay"]);
                }

            }
            return AlertsData;
        }

        public DataTable GetWTPChartData(string WTPName, string KPIName, int Days)
        {
            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                SqlCommand cmd = new SqlCommand("GetWTPChartData", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@inWTPName", SqlDbType.NVarChar, 100).Value = WTPName;
                cmd.Parameters.Add("@inKPIName", SqlDbType.NVarChar, 100).Value = KPIName;
                cmd.Parameters.Add("@inDays", SqlDbType.Int).Value = Days;

                cmd.CommandTimeout = 1600;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);               
            }

            return dt;
        }


        public DataTable GetWDSChartData(string ZoneName, string WDSName, string KPIName, int Days,string iterationDatetime)
        {
            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetWDSChartData", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@inZoneName", SqlDbType.NVarChar, 100).Value = ZoneName;
                cmd.Parameters.Add("@inWDSName", SqlDbType.NVarChar, 100).Value = WDSName;
                cmd.Parameters.Add("@inKPIName", SqlDbType.NVarChar, 100).Value = KPIName;
                cmd.Parameters.Add("@inDays", SqlDbType.Int).Value = Days;
                cmd.Parameters.Add("@iterationDatetime", SqlDbType.NVarChar, 500).Value = iterationDatetime;

                cmd.CommandTimeout = 1600;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }




        public DataTable GetWDNChartData(string ZoneName, string WardName, string Location, string KPIName, string Days, int iterationdays)
        {
            DataTable dt = new DataTable();
            DateTime dtime = Convert.ToDateTime(Days);
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetWDNChartData", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@inZoneName", SqlDbType.NVarChar, 100).Value = ZoneName;
                cmd.Parameters.Add("@inWardName", SqlDbType.NVarChar, 100).Value = WardName;
                cmd.Parameters.Add("@inLocation", SqlDbType.NVarChar, 100).Value = Location;
                cmd.Parameters.Add("@inKPIName", SqlDbType.NVarChar, 100).Value = KPIName;
                cmd.Parameters.Add("@inDays", SqlDbType.DateTime, 100).Value = Convert.ToDateTime(dtime);
                cmd.Parameters.Add("@iterationdays", SqlDbType.Int).Value = iterationdays;

                cmd.CommandTimeout = 1600;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        //public LineChartModel[] GetWDNChartDataCompare(string ZoneName, string WardName, string Location, string KPIName, string Days, int iterationdays)
        //{


        //    List<LineChartModel> model = new List<LineChartModel>();
        //    if (!(KPIName.Contains("Flow") && KPIName.Contains("Pressure")))
        //    {
        //        return model.ToArray();
        //    }
        //    DateTime iterationDateAndTime = Convert.ToDateTime(Days);
        //    string kpi = KPIName.Replace(")", "),");
        //    var lastComma = kpi.LastIndexOf(',');
        //    if (lastComma != -1)
        //    {
        //        kpi = kpi.Remove(lastComma, 1).Insert(lastComma, "").Trim();

        //    }
        //        List<LineChartIterationDateModel> iterationDateList = new List<LineChartIterationDateModel>();

        //    try
        //    {
        //        using (_db = new AMCDBContext())
        //        {
        //            //iterationDateList = _db.Database.SqlQuery<LineChartIterationDateModel>("EXEC GetIterationDateForWDNLineChart @inZoneName={0},@inWardName={1},@inLocation={2},@inKPIName={3},@inDays={4},@iterationdays={5}", ZoneName,WardName,Location, kpi, Days, iterationdays).ToList();
        //            //if (iterationDateList.Any())
        //            //{
        //                List<decimal> lastvalueListForFlow = new List<decimal>();
        //                List<decimal> lastvalueListForPressure = new List<decimal>();
        //            //foreach (var item in iterationDateList)
        //            //{
        //            for (int i = 0; i < iterationdays; i++)
        //            {
        //                decimal lastvalFlow = _db.Database.SqlQuery<decimal>("EXEC GetWDNChartDataWithMultipleKPI @inZoneName={0},@inWardName={1},@inLocation={2},@Tag={3},@iterationDatetime={4}", ZoneName, WardName, Location, "Flow", Convert.ToDateTime(iterationDateAndTime).AddDays(-i)).FirstOrDefault();
        //                lastvalueListForFlow.Add(lastvalFlow);

        //                decimal lastvalPressure = _db.Database.SqlQuery<decimal>("EXEC GetWDNChartDataWithMultipleKPI @inZoneName={0},@inWardName={1},@inLocation={2},@Tag={3},@iterationDatetime={4}", ZoneName, WardName, Location, "Pressure", Convert.ToDateTime(iterationDateAndTime).AddDays(-i)).FirstOrDefault();                        
        //                lastvalueListForPressure.Add(lastvalPressure);
        //            }
        //                //}
        //                decimal[] valuesFlow = lastvalueListForFlow.ToArray();
        //                LineChartModel objFlow = new LineChartModel();
        //                objFlow.name = "Flow";
        //                objFlow.data = valuesFlow;
        //                model.Add(objFlow);

        //                decimal[] valuesPressure = lastvalueListForPressure.ToArray();
        //                LineChartModel objPressure = new LineChartModel();
        //                objPressure.name = "Pressure";
        //                objPressure.data = valuesPressure;
        //                model.Add(objPressure);
        //            //}
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    //LineChartModel valObj = new LineChartModel();
        //    //valObj.name = "flow";

        //    //List<decimal> valueList = new List<decimal>();
        //    //valueList.Add(10);
        //    //valueList.Add(15);
        //    //valueList.Add(7);
        //    //valueList.Add(27);
        //    //decimal[] values = valueList.ToArray();
        //    //valObj.data = values;
        //    //model.Add(valObj);

        //    //LineChartModel valObj1 = new LineChartModel();
        //    //valObj1.name = "pressure";

        //    //List<decimal> valueList1 = new List<decimal>();
        //    //valueList1.Add(6);
        //    //valueList1.Add(11);
        //    //valueList1.Add(17);
        //    //valueList1.Add(22);
        //    //decimal[] values1 = valueList1.ToArray();
        //    //valObj1.data = values1;
        //    //model.Add(valObj1);
        //    var arrModel = model.ToArray();
        //    return arrModel;
        //}
        public LineChartModel[] GetWDNChartDataCompare(string ZoneName, string WardName, string Location, string KPIName, string fromDate, string toDate)
        {
            List<LineChartModel> model = new List<LineChartModel>();
           
            List<LineChartIterationDateTimeModel> iterationDateList = new List<LineChartIterationDateTimeModel>();

            try
            {
                
                using (_db = new AMCDBContext())
                {
                    iterationDateList = _db.Database.SqlQuery<LineChartIterationDateTimeModel>("EXEC GetIterationDateForWDNLineChart @inZoneName={0},@inWardName={1},@inLocation={2},@inKPIName={3},@FromDate={4},@ToDate={5},@IsFormatNeeded={6}", ZoneName, WardName, Location, KPIName,fromDate, toDate,"NO").ToList();
                    if (iterationDateList.Any())
                    {
                        List<decimal> lastvalueListForFlow = new List<decimal>();
                        List<decimal> lastvalueListForPressure = new List<decimal>();
                        string IterationDatetimeLIST = string.Join(",", iterationDateList.Select(S=>S.DateAndTime.ToString()));
                        
                            var lastvalFlow = _db.Database.SqlQuery<decimal>("EXEC GetWDNChartDataWithMultipleKPI @inZoneName={0},@inWardName={1},@inLocation={2},@Tag={3},@iterationDatetime={4}", ZoneName, WardName, Location, "Flow", IterationDatetimeLIST).ToList();
                            //lastvalueListForFlow.Add(lastvalFlow);

                            var lastvalPressure = _db.Database.SqlQuery<decimal>("EXEC GetWDNChartDataWithMultipleKPI @inZoneName={0},@inWardName={1},@inLocation={2},@Tag={3},@iterationDatetime={4}", ZoneName, WardName, Location, "Pressure", IterationDatetimeLIST).ToList();
                            //lastvalueListForPressure.Add(lastvalPressure);
                            
                        //foreach (var item in iterationDateList)
                        //{
                        //    //for (int i = 0; i < 10; i++)
                        //    //{
                        //    decimal lastvalFlow = _db.Database.SqlQuery<decimal>("EXEC GetWDNChartDataWithMultipleKPI @inZoneName={0},@inWardName={1},@inLocation={2},@Tag={3},@iterationDatetime={4}", ZoneName, WardName, Location, "Flow", item.DateAndTime).FirstOrDefault();
                        //    lastvalueListForFlow.Add(lastvalFlow);

                        //    decimal lastvalPressure = _db.Database.SqlQuery<decimal>("EXEC GetWDNChartDataWithMultipleKPI @inZoneName={0},@inWardName={1},@inLocation={2},@Tag={3},@iterationDatetime={4}", ZoneName, WardName, Location, "Pressure", item.DateAndTime).FirstOrDefault();
                        //    lastvalueListForPressure.Add(lastvalPressure);
                        //    // }
                        //}
                        //decimal[] valuesFlow = lastvalueListForFlow.ToArray();
                        decimal[] valuesFlow = lastvalFlow.ToArray();
                        LineChartModel objFlow = new LineChartModel();
                        objFlow.name = "Flow";
                        objFlow.data = valuesFlow;
                        model.Add(objFlow);

                        //decimal[] valuesPressure = lastvalueListForPressure.ToArray();
                        decimal[] valuesPressure = lastvalPressure.ToArray();
                        LineChartModel objPressure = new LineChartModel();
                        objPressure.name = "Pressure";
                        objPressure.data = valuesPressure;
                        model.Add(objPressure);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
            }
            
            var arrModel = model.ToArray();
            return arrModel;
        }

        public LineChartModel[] GetWDSChartDataCompare(string ZoneName,  string Location, string KPIName, string fromDate, string toDate)
        {
            List<LineChartModel> model = new List<LineChartModel>();

            List<LineChartIterationDateTimeModel> iterationDateList = new List<LineChartIterationDateTimeModel>();

            try
            {
                using (_db = new AMCDBContext())
                {
                    iterationDateList = _db.Database.SqlQuery<LineChartIterationDateTimeModel>("EXEC GetIterationDateForWDSLineChart @inZoneName={0},@inLocation={1},@inKPIName={2},@FromDate={3},@ToDate={4},@IsFormatNeeded={5}", ZoneName,  Location, KPIName, fromDate, toDate, "NO").ToList();
                    if (iterationDateList.Any())
                    {
                        List<decimal> lastvalueListForFlow = new List<decimal>();
                        List<decimal> lastvalueListForPressure = new List<decimal>();
                        string IterationDatetimeLIST = string.Join(",", iterationDateList.Select(S => S.DateAndTime.ToString()));

                        var lastvalFlow = _db.Database.SqlQuery<decimal>("EXEC GetWDSChartDataWithMultipleKPI @inZoneName={0},@inLocation={1},@Tag={2},@iterationDatetime={3}", ZoneName,  Location, "Outlet Flow", IterationDatetimeLIST).ToList();
                        //lastvalueListForFlow.Add(lastvalFlow);

                        var lastvalPressure = _db.Database.SqlQuery<decimal>("EXEC GetWDSChartDataWithMultipleKPI @inZoneName={0},@inLocation={1},@Tag={2},@iterationDatetime={3}", ZoneName,  Location, "Pressure", IterationDatetimeLIST).ToList();
                       
                        decimal[] valuesFlow = lastvalFlow.ToArray();
                        LineChartModel objFlow = new LineChartModel();
                        objFlow.name = "Outlet Flow";
                        objFlow.data = valuesFlow;
                        model.Add(objFlow);

                        //decimal[] valuesPressure = lastvalueListForPressure.ToArray();
                        decimal[] valuesPressure = lastvalPressure.ToArray();
                        LineChartModel objPressure = new LineChartModel();
                        objPressure.name = "Pressure";
                        objPressure.data = valuesPressure;
                        model.Add(objPressure);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
            }

            var arrModel = model.ToArray();
            return arrModel;
        }


        public List<LineChartIterationDateModel> GetWDNChartDataLabel(string ZoneName, string WardName, string Location, string KPIName, string fromDate, string toDate)
        {

            List<LineChartIterationDateModel> iterationDateList = new List<LineChartIterationDateModel>();

            try
            {
                using (_db = new AMCDBContext())
                {
                    iterationDateList = _db.Database.SqlQuery<LineChartIterationDateModel>("EXEC GetIterationDateForWDNLineChart @inZoneName={0},@inWardName={1},@inLocation={2},@inKPIName={3},@FromDate={4},@ToDate={5},@IsFormatNeeded={6}", ZoneName, WardName, Location, KPIName, fromDate, toDate,"YES").ToList();
                    
                }
            }
            catch (Exception ex)
            {
            }

            return iterationDateList;
        }

        public List<LineChartIterationDateModel> GetWDSChartDataLabel(string ZoneName, string Location, string KPIName, string fromDate, string toDate)
        {

            List<LineChartIterationDateModel> iterationDateList = new List<LineChartIterationDateModel>();

            try
            {
                using (_db = new AMCDBContext())
                {
                    iterationDateList = _db.Database.SqlQuery<LineChartIterationDateModel>("EXEC GetIterationDateForWDSLineChart @inZoneName={0},@inLocation={1},@inKPIName={2},@FromDate={3},@ToDate={4},@IsFormatNeeded={5}", ZoneName, Location, KPIName, fromDate, toDate, "YES").ToList();

                }
            }
            catch (Exception ex)
            {
            }

            return iterationDateList;
        }

        public List<TampringAlerts> GetTampringAlertsData(string Type)
        {
            List<TampringAlerts> tampringAlerts = new List<TampringAlerts>();

            dynamic query = null;
            using (_db = new AMCDBContext())
            {
                query = _db.Database.SqlQuery<TampringAlerts>(Tampringdata,Type).ToList();
                tampringAlerts = query;
            }
                return tampringAlerts;
        }



        public DataTable GetWDSProjectScoreChartData(string ZoneName, string WDSLocation, string KPIName, string date_time)
        {
            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetWDSProjectScoreChartData", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@inZoneName", SqlDbType.NVarChar, 100).Value = ZoneName;
                cmd.Parameters.Add("@inWDSLocation", SqlDbType.NVarChar, 100).Value = WDSLocation;
                cmd.Parameters.Add("@inKPIName", SqlDbType.NVarChar, 100).Value = KPIName;
                cmd.Parameters.Add("@inDays", SqlDbType.NVarChar, 100).Value = date_time;

                cmd.CommandTimeout = 1600;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }
        public DataTable GetWDSProjectScoreChartDataPressure(string ZoneName, string WDSLocation, string KPIName, string date_time)
        {
            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetWDSProjectScoreChartData_2", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@inZoneName", SqlDbType.NVarChar, 100).Value = ZoneName;
                cmd.Parameters.Add("@inWDSLocation", SqlDbType.NVarChar, 100).Value = WDSLocation;
                cmd.Parameters.Add("@inKPIName", SqlDbType.NVarChar, 100).Value = KPIName;
                cmd.Parameters.Add("@inDays", SqlDbType.NVarChar, 100).Value = date_time;

                cmd.CommandTimeout = 1600;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }


        public DataTable GetWDNProjectScoreChartData(string ZoneName, string WardName, string Location, string KPIName, int Days)
        {
            DataTable dt = new DataTable();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetWDNProjectScoreChartData", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@inZoneName", SqlDbType.NVarChar, 100).Value = ZoneName;
                cmd.Parameters.Add("@inWardName", SqlDbType.NVarChar, 100).Value = WardName;
                cmd.Parameters.Add("@inLocation", SqlDbType.NVarChar, 100).Value = Location;
                cmd.Parameters.Add("@inKPIName", SqlDbType.NVarChar, 100).Value = KPIName;
                cmd.Parameters.Add("@inDays", SqlDbType.Int).Value = Days;

                cmd.CommandTimeout = 1600;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            return dt;
        }

        public List<UFWModel> GetUFWDetailsByType(string type)
        {
            List<UFWModel> ufwList = new List<UFWModel>();
        
            using (_db = new AMCDBContext())
            {
                ufwList = _db.Database.SqlQuery<UFWModel>("EXEC GetUFWDetails @Type={0}", type).ToList();               
            }
            return ufwList;
        }

        public List<LDQValuesForUFWModel> GetUFW_LDQDetails()
        {
            List<LDQValuesForUFWModel> ufwLDQList = new List<LDQValuesForUFWModel>();

            using (_db = new AMCDBContext())
            {
                ufwLDQList = _db.Database.SqlQuery<LDQValuesForUFWModel>("EXEC GetUFW_LDQDetails").ToList();
            }
            return ufwLDQList;
        }

        public List<WaterVsEnergyModel> GetWaterVsEnergyDetails()
        {
            List<WaterVsEnergyModel> waterVsEnergy = new List<WaterVsEnergyModel>();

            dynamic query = null;
            using (_db = new AMCDBContext())
            {
                waterVsEnergy = _db.Database.SqlQuery<WaterVsEnergyModel>(Tampringdata, "water_vs_energy_count").ToList();
               
            }
            return waterVsEnergy;
        }
    }
}