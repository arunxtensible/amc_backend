﻿using AMC.IServices;
using AMC.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AMC.DBContext;

namespace AMC.Services
{
    public class WDSRepository : IWds
    {
        AMCDBContext _db = new AMCDBContext();
        public string ConnectionString = ConfigurationManager.ConnectionStrings["MyConnString"].ConnectionString;
        Mapper map = null;
        public WDSRepository()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<WDSMaster, WDSModel>();
                cfg.CreateMap<WDSModel, WDSMaster>();
            });
            map = new Mapper(config);
        }

        public List<WDSModel> GetAllWDS()
        {
            using (var ctx = new AMCEntities())
            {
                var WdsList = ctx.WDSMasters.Where(y => (bool)!y.IsDeleted).OrderByDescending(o => o.WDSId).ToList();
                List<WDSModel> _Wds = map.Map<List<WDSModel>>(WdsList);
                return _Wds;
            }
        }

        public WDSModel GetWDS(int id)
        {
            using (var ctx = new AMCEntities())
            {
                var _WDSName = ctx.WDSMasters.Find(id);
                if (_WDSName != null)
                {
                    WDSModel _Wds = map.Map<WDSModel>(_WDSName);
                    return _Wds;
                }
                return null;
            }
        }

        //List<WDSModel> IWds.GetAllWDS()
        //{
        //    throw new NotImplementedException();
        //}

        List<WDSInletOutletModel> IWds.GetAllZoneInletOutlet(string Type)
        {
            List<WDSInletOutletModel> WDSInlet = new List<WDSInletOutletModel>();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {

                DataTable dt = new DataTable();


                SqlCommand cmd = new SqlCommand("GetWDSInletOrOutletList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Type", SqlDbType.VarChar, 10).Value = Type;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    WDSInletOutletModel obj = new WDSInletOutletModel();

                    obj.ZoneId = Convert.ToInt32((dr["ZoneId"]));
                    obj.ZoneName = (dr["ZoneName"]).ToString();
                    obj.ZoneCDQ = Convert.ToDecimal(dr["ZoneCDQ"]);
                    obj.ZoneLDQ = Convert.ToDecimal(dr["ZoneLDQ"]);
                    obj.ZoneUnitConsumption = Convert.ToDecimal(dr["ZoneUnitConsumption"]);
                    obj.LDRDateTime = (Convert.ToDateTime(dr["LDRDateTime"])).ToString();


                    WDSInlet.Add(obj);
                }
            }

            return WDSInlet;

        }

        WDSModel IWds.GetWDS(int id)
        {
            throw new NotImplementedException();
        }

        List<WDSInletOutletLocationModel> IWds.GetWDSLocationsByZoneId(int? ZoneId, string Type)
        {
            List<WDSInletOutletLocationModel> WDSLocationList = new List<WDSInletOutletLocationModel>();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {

                DataTable dt = new DataTable();


                SqlCommand cmd = new SqlCommand("GetWDSInletOrOutletLocationListByZoneId", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ZoneId", SqlDbType.Int).Value = ZoneId;
                cmd.Parameters.Add("@Type", SqlDbType.VarChar, 500).Value = Type;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    WDSInletOutletLocationModel obj = new WDSInletOutletLocationModel();

                    obj.LocationName = (dr["LocationName"]).ToString();
                    obj.Capacity = Convert.ToDecimal(dr["Capacity"]);
                    obj.CDQ = Convert.ToDecimal(dr["CDQ"]);
                    obj.LDQ = Convert.ToDecimal(dr["LDQ"]);
                    obj.UnitConsumption = Convert.ToDecimal(dr["UnitConsumption"]);
                    obj.LDR_DateTime = (Convert.ToDateTime(dr["LDR_DateTime"]));
                    obj.ZoneList = new SelectList(GetZoneList(), "ZoneId", "ZoneName"); // model binding

                    WDSLocationList.Add(obj);
                }
            }
            return WDSLocationList;
        }

        List<WDSInletOutletLocationModel> IWds.GetWDSLocationsByZoneIdSearchLocation(int? ZoneId, string LocationName, string Type)
        {
            List<WDSInletOutletLocationModel> WDSLocationList = new List<WDSInletOutletLocationModel>();
            //try
            //{
            //    using (_db = new AMCDBContext())
            //    {

            //        WDSLocationList = _db.Database.SqlQuery<WDSInletOutletLocationModel>("EXEC GetWDSInletOrOutletLocationListByZoneIdAndLocation @ZoneId={0}, @LocationName={1},@Type={2}", ZoneId, LocationName, Type).ToList();
            //    }
            //}
            //catch (Exception ex)
            //{
            //}

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {

                DataTable dt = new DataTable();


                SqlCommand cmd = new SqlCommand("GetWDSInletOrOutletLocationListByZoneIdAndLocation", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ZoneId", SqlDbType.Int).Value = ZoneId;
                cmd.Parameters.Add("@LocationName", SqlDbType.VarChar, 500).Value = LocationName;
                cmd.Parameters.Add("@Type", SqlDbType.VarChar, 500).Value = Type;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    WDSInletOutletLocationModel obj = new WDSInletOutletLocationModel();

                    obj.LocationName = (dr["LocationName"]).ToString();
                    obj.Capacity = Convert.ToDecimal(dr["Capacity"]);
                    obj.CDQ = Convert.ToDecimal(dr["CDQ"]);
                    obj.LDQ = Convert.ToDecimal(dr["LDQ"]);
                    obj.LDR_DateTime = (Convert.ToDateTime(dr["LDR_DateTime"]));
                    obj.ZoneList = new SelectList(GetZoneList(), "ZoneId", "ZoneName"); // model binding

                    WDSLocationList.Add(obj);
                }
            }

            return WDSLocationList;
        }
        public IEnumerable<ZoneMaster> GetZoneList()
        {



            List<ZoneMaster> ZoneList = new List<ZoneMaster>();


            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT [ZoneId],[ZoneName]FROM ZoneMaster Order by ZoneId asc", con))
                {
                    using (IDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ZoneMaster obj = new ZoneMaster();
                            obj.ZoneID = Convert.ToInt32(dr["ZoneID"]);
                            obj.ZoneName = (dr["ZoneName"]).ToString();
                            ZoneList.Add(obj);
                        }
                    }
                }
            }
            return ZoneList;
        }

        List<WDSInletOutletLocationDetailsModel> IWds.WDSInletOutletDetailByLocationName(string LocaionName, string Type)
        {
            List<WDSInletOutletLocationDetailsModel> WDSLocationDetailsList = new List<WDSInletOutletLocationDetailsModel>();
            try
            {
                using (_db = new AMCDBContext())
                {

                    WDSLocationDetailsList = _db.Database.SqlQuery<WDSInletOutletLocationDetailsModel>("EXEC GetWDSInletOutletLocationDetailsByLocationName @LocationName={0}, @Type={1}", LocaionName, Type).ToList();
                }

            }
            catch (Exception ex)
            {
            }
            //using (SqlConnection con = new SqlConnection(ConnectionString))
            //{

            //    DataTable dt = new DataTable();


            //    SqlCommand cmd = new SqlCommand("GetWDSInletOutletLocationDetailsByLocationName", con);
            //    cmd.CommandType = CommandType.StoredProcedure;
            //    cmd.Parameters.Add("@LocationName", SqlDbType.VarChar, 100).Value = LocaionName;
            //    cmd.Parameters.Add("@Type", SqlDbType.VarChar, 10).Value = Type;
            //    SqlDataAdapter da = new SqlDataAdapter(cmd);
            //    da.Fill(dt);

            //    foreach (DataRow dr in dt.Rows)
            //    {
            //        WDSInletOutletLocationDetailsModel obj = new WDSInletOutletLocationDetailsModel();

            //        obj.ParentWTPName = "";//(dr["ParentWTPName"]).ToString();
            //        obj.InletFlow = Convert.ToDecimal(dr["InletFlow"]);
            //        obj.SumpLevel = Convert.ToDecimal(dr["SumpLevel"]);
            //        obj.OutletFlowRate = Convert.ToDecimal(dr["OutletFlowRate"]);
            //        obj.Pressure = Convert.ToDecimal(dr["Pressure"]);
            //        obj.SumpCapacity = Convert.ToDecimal(dr["SumpCapacity"]);

            //        obj.PumpStatus = (dr["PumpStatus"]).ToString();
            //        obj.PumpDesign = (dr["PumpDesign"]).ToString();
            //        obj.PumpRun = (dr["PumpRun"]).ToString();

            //        obj.KPI_Current = (dr["KPI_Current"]).ToString();
            //        obj.PeakLevelMTR_Current = Convert.ToDecimal(dr["PeakLevelMTR_Current"]);
            //        obj.InletVolume_Current = Convert.ToDecimal(dr["InletVolume_Current"]);
            //        obj.OutletVolume_Current = Convert.ToDecimal(dr["OutletVolume_Current"]);
            //        obj.PeakPressure_Current = Convert.ToDecimal(dr["PeakPressure_Current"]);
            //        obj.TotalPumpingHour_Current = Convert.ToDecimal(dr["TotalPumpingHour_Current"]);
            //        obj.UnitConsumption_Current = Convert.ToDecimal(dr["UnitConsumption_Current"]);
            //        obj.KWHM3_Current = Convert.ToDecimal(dr["KWHM3_Current"]);

            //        obj.KPI_prev = (dr["KPI_prev"]).ToString();
            //        obj.PeakLevelMTR_prev = Convert.ToDecimal(dr["PeakLevelMTR_prev"]);
            //        obj.InletVolume_prev = Convert.ToDecimal(dr["InletVolume_prev"]);
            //        obj.OutletVolume_prev = Convert.ToDecimal(dr["OutletVolume_prev"]);
            //        obj.PeakPressure_prev = Convert.ToDecimal(dr["PeakPressure_prev"]);
            //        obj.TotalPumpingHour_prev = Convert.ToDecimal(dr["TotalPumpingHour_prev"]);
            //        obj.UnitConsumption_prev = Convert.ToDecimal(dr["UnitConsumption_prev"]);
            //        obj.KWHM3_prev = Convert.ToDecimal(dr["KWHM3_prev"]);
            //        obj.LDR_DateTime = Convert.ToDateTime(dr["LDR_DateTime"]).ToString("dd/MM/yyyy hh:mm tt");

            //        obj.IntakeSource = (dr["IntakeSource"]).ToString();

            //        WDSLocationDetailsList.Add(obj);
            //    }



            //}
            return WDSLocationDetailsList;
        }
        public WDSModel Create(WDSModel model)
        {
            using (var ctx = new AMCEntities())
            {
                var _Wds = map.Map<WDSMaster>(model);
                ctx.WDSMasters.Add(_Wds);
                ctx.SaveChanges();
                model.WDSId = _Wds.WDSId;

                return model;
            }
        }
        public bool Update(WDSModel model)
        {
            using (var ctx = new AMCEntities())
            {
                var _WDS = ctx.WDSMasters.Find(model.WDSId);
                if (_WDS != null)
                {
                    _WDS.WDSName = model.WDSName;
                    _WDS.Type = model.Type;
                    _WDS.capacity = model.Capacity;
                    _WDS.PumpCount = model.PumpCount;
                    _WDS.ZoneId = model.ZoneId;
                    _WDS.WorkingPumpCount = model.WorkingPumpCount;
                    _WDS.StandbyPumpCount = model.StandbyPumpCount;
                    ctx.Entry(_WDS).State = EntityState.Modified;
                    ctx.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        public bool Delete(int id)
        {
            using (var ctx = new AMCEntities())
            {
                var _WDS = ctx.WDSMasters.Find(id);
                if (_WDS != null)
                {
                    _WDS.IsDeleted = true;
                    ctx.Entry(_WDS).State = EntityState.Modified;
                    ctx.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public bool SetWDSFillUpTime(string time, string WDSName)
        {
            bool isSetTime = false;
            TimeSpan _time = TimeSpan.Parse(time);
            try
            {
                isSetTime = _db.Database.SqlQuery<bool>("EXEC SetWDSFillUpTime @Time={0}, @WDSName={1}", _time, WDSName).FirstOrDefault();
            }
            catch (Exception ex)
            {
            }
            return isSetTime;
        }
    }
}