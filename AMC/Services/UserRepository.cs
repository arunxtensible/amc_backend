﻿using AMC.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AMC.IServices;
using System.Data.SqlClient;
using System.Data;

namespace AMC.Services
{
   //public interface IUser
   // {
   //     bool AuthonticateUser(Login obj);  
   //     bool AddOrUpdateUser(UserVM obj);
   //     bool DeleteUser(int id);

   //     UserVM GetUser(int id);
   //     //List<UserVM> GetAllUsers();
   // }
    public class UserRepository : IUser
    {
        public string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["MyConnString"].ConnectionString;

        Mapper map = null;
        public UserRepository()
        {
            MapperConfiguration config =   new MapperConfiguration(cfg => {
                cfg.CreateMap<User, UserVM>();
                cfg.CreateMap<UserVM, User>();
            });
            map = new Mapper(config);
        }
   
        public bool AddOrUpdateUser(UserVM obj)
        {
            throw new NotImplementedException();
        }

        public bool AuthonticateUser(Login user)
        {
            //using (var ctx = new AMCEntities())
            //{
            //    var obj = ctx.Users.Where(w=>w.LoginId.ToLower().Trim() == user.UserName.ToLower().Trim() && w.Password == user.Password).FirstOrDefault();
            //    if (obj!=null)
            //    {
            //        return true;
            //    }
            //    return false;
            //}

            //using (var ctx = new AMCEntities())
            //{
            //    var obj = ctx.Users.Where(w => w.LoginId.ToLower().Trim() == user.UserName.ToLower().Trim() && w.Password == user.Password).FirstOrDefault();
            //    if (obj != null)
            //    {
            //        return true;
            //    }
            //    return false;
            //}

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                DataTable dt = new DataTable();

                SqlCommand cmd = new SqlCommand("Login", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@InUserName", SqlDbType.VarChar).Value = user.UserName;
                cmd.Parameters.Add("@InPassword", SqlDbType.VarChar).Value = user.Password;
                cmd.CommandTimeout = 1600;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                if (dt.Rows.Count >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

        }

        public bool DeleteUser(int id)
        {
            throw new NotImplementedException();
        }

        public List<UserVM> GetAllUsers()
        {
            using (var ctx = new AMCEntities())
            {
                var UserList = ctx.Users.OrderByDescending(o=>o.LoginId).ToList();             
                List<UserVM> userDto = map.Map<List<UserVM>>(UserList); 
                return userDto;
            }
        }

        public UserVM GetUser(int id)
        {
            using (var ctx = new AMCEntities())
            {
                var obj = ctx.Users.Find(id);
                if (obj != null)
                {
                    UserVM userDto = map.Map<UserVM>(obj);
                    return userDto;
                }
                return null;
            }
        }
    }
}