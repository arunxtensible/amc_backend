﻿using AMC.IServices;
using AMC.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AMC.Services
{
    public class DeviceRepository : IDevice
    {
        Mapper map = null;
        public DeviceRepository()
        {
            MapperConfiguration config = new MapperConfiguration(cfg => {
                cfg.CreateMap<DeviceMaster, DeviceModel>();
                cfg.CreateMap<DeviceModel, DeviceMaster>();
            });
            map = new Mapper(config);
        }

        public List<DeviceModel> GetAllDevice()
        {
            using (var ctx = new AMCEntities())
            {
                var DeviceList = ctx.DeviceMasters.OrderByDescending(o => o.ID).ToList();
                List<DeviceModel> _Wtp = map.Map<List<DeviceModel>>(DeviceList);
                return _Wtp;
            }
        }

        public DeviceModel GetDevice(int id)
        {
            using (var ctx = new AMCEntities())
            {
                var _DeviceName = ctx.WTP_Master.Find(id);
                if (_DeviceName != null)
                {
                    DeviceModel _Wtp = map.Map<DeviceModel>(_DeviceName);
                    return _Wtp;
                }
                return null;
            }
        }

        public DeviceModel Add(DeviceModel model)
        {
            using (var ctx = new AMCEntities())
            {
                var _Device = new DeviceMaster();
                _Device.DeviceName = model.DeviceName;
                //_Device.Coordinate = model.Coordinates;
                _Device.IsInstalled = model.IsInstalled;
                ctx.DeviceMasters.Add(_Device);
                ctx.SaveChanges();
                model.ID = _Device.ID;
                return model;
            }
        }

        public bool Update(DeviceModel model)
        {
            using (var ctx = new AMCEntities())
            {
                var _Device = ctx.DeviceMasters.Find(model.ID);
                if (_Device != null)
                {
                    _Device.DeviceName = model.DeviceName;
                  //  _Device.Coordinate = model.Coordinates;
                    _Device.IsInstalled = model.IsInstalled;
                    ctx.Entry(_Device).State = EntityState.Modified;
                    ctx.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public bool Delete(int id)
        {
            using (var ctx = new AMCEntities())
            {
                var _Device = ctx.DeviceMasters.Find(id);
                if (_Device != null)
                {
                    ctx.DeviceMasters.Remove(_Device);
                    ctx.SaveChanges();
                    return true;
                }
                return false;
            }
        }

    }
}