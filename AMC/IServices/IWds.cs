﻿using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMC.IServices
{
    public interface IWds
    {
        WDSModel GetWDS(int id);

        List<WDSModel> GetAllWDS();

        List<WDSInletOutletModel> GetAllZoneInletOutlet(string Type);
        List<WDSInletOutletLocationModel> GetWDSLocationsByZoneId(int? ZoneId, string Type);
        List<WDSInletOutletLocationModel> GetWDSLocationsByZoneIdSearchLocation(int? ZoneId, string SearchValue, string Type);
        List<WDSInletOutletLocationDetailsModel> WDSInletOutletDetailByLocationName(string LocaionName, string Type);

        IEnumerable<ZoneMaster> GetZoneList();
        bool Delete(int id);
        bool Update(WDSModel model);
        WDSModel Create(WDSModel model);

        bool SetWDSFillUpTime(string time, string WDSName);
    }
}
