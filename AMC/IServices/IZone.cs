﻿using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.IServices
{
    public interface IZone
    {
        ZoneModel GetZone(int id);
        List<ZoneModel> GetAllZones();

        ZoneModel Add(ZoneModel model);

        bool Update(ZoneModel model);

        bool Delete(int id);

        List<DashboardAlertsModel> GetDashboardAlertData();

        List<WTPGroundWaterDataModel> GetAllWTPGroundWater();
    }
}