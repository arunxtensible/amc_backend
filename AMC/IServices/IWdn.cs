﻿using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMC.IServices
{
    public interface IWdn
    {
        WDNModel GetWDN(int id);

        List<WDNModel> GetAllWDN();
        WDNModel Create(WDNModel model);
        bool Update(WDNModel model);
        bool Delete(int id);
        List<WDNZoneListModel> GetAllZones();
        List<WDNWardListModel> GetAllWards(int ZoneId);
        List<WDNLocationListModel> GetAllLocations(int WardId);
    }
}
