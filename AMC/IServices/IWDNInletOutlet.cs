﻿using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.IServices
{
    public interface IWDNInletOutlet
    {
        List<WDNInletOutletModel> GetAllInlet(string Type);
        List<WDNInletOutletModel> GetWDNZoneGraphData();
        List<WDNInletOutletModel> GetWDNWardGraphData(int Id);
        List<WDNInletOutletModel> GetWDNLocationGraphData(int Id);
        //List<WDNInletOutletDetailModel> WDNInletOutletDetailByWDNName(Int32 WDNName, string Type);
        List<WDNInletOutletDetailModel> WDNInletOutletDetailByWDNName(string TagName);
    }
}