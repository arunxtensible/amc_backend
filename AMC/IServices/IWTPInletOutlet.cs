﻿using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMC.IServices
{
    public interface  IWTPInletOutlet
    {
        List<WTPInletOutletModel> GetAllInlet(string Type , string searchKey);

        List<WTPInletOutletDetailModel> WTPInletOutletDetailByWTPName(Int32 WTPName, string Type);
    }
}