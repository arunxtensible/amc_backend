﻿using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMC.IServices
{
    public interface ISetting
    {
        bool SyncWDNData(string startTime, string endTime);
        bool Sync_WDS_WTPData(string startTime, string endTime);
        bool SyncAlarmData(string startTime, string endTime);
        bool SyncPumpData(string startTime, string endTime);
        List<LDQCustomSettingModel> GetWTP_WDS_LDQValues();
        bool SetCustomLDQValue(string PropertyName, string value);

        List<WaterQualitySettingModel> GetWaterQualityValues();
        bool SetWaterQualityValue(string PropertyName, string Min, string Max);
        bool SetWaterQualityAnalysisTime(string time);
        List<AlertMailConfigurationSettingModel> GetAlertMailConfigurationSetting(string alertname, string zone);
        bool SetAlertMailConfigurationSetting(string alertname, string zone, string mailtime, string mailto, string mailcc, string mailbcc, string subject);
        List<AlertMailConfigurationModel> GetAlertMailConfigValues();
        bool SetAlertMailConfigValue(int Id, string EastMailIds, string WestMailIds, string SouthMailIds, string NorthMailIds, string EmailSubject, DateTime MailTime);

        List<WDNReportConfigurationModel> GetWDNReportConfigValues(string Type);
        bool SetWDNReportConfigValue(int Id, string MailId, string EmailSubject);
    }
}
