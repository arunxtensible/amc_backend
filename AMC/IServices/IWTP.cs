﻿using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMC.IServices
{
    public interface IWtp
    {

        WTPModel GetWTP(int id);
        List<WTPModel> GetAllWTP();

        List<WTPGroundWaterModel> GetAllWTPGroundWater();


        WTPModel Add(WTPModel model);
        bool Update(WTPModel model);
        bool Delete(int id);
    }
}
