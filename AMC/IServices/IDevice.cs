﻿using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMC.IServices
{
    public interface IDevice
    {
        DeviceModel GetDevice(int id);
        List<DeviceModel> GetAllDevice();

        DeviceModel Add(DeviceModel model);
        bool Update(DeviceModel model);
        bool Delete(int id);
    }
}
