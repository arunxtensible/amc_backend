﻿using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMC.IServices
{
    public interface IMap
    {
        List<MapZoneModel> GetZoneForMap();
        List<MapZoneLineModel> GetZoneLineForMap();
        List<MapWDNDataModel> GetWDNByZoneAndWardForMap(int zoneid, int wardid);
        List<MapWDSDataModel> GetWDSByZoneForMap(int zoneid);
        List<MapWDSDetailsModel> GetWDSDetailsByNameForMap(string wdsname);
        List<MapWDNNetworkByWardModel> GetNetworkByWard(int wardid);
        List<MapWDNLocationNetworkModel> GetWDNNetworkByZone(int wardid);
        List<MapWDSNetworkByZoneModel> GetWDSNetworkByZone(int zoneId);
        List<MapWDNNetworkByWardModel> GetNetworkById(int id);
        List<MapWDNNetworkByKPIModel> GetNetworkByKpi(string kpi, string zone, string ward, string location);
        List<MapWDSNetworkByKPIModel> GetWDSNetworkByKpi(string kpi, string zone,string location);
        List<MapWDNNetworkByKPIModel> GetNetworkByKpi_Id(string kpiID, string kpi);
    }
}
