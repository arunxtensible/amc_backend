﻿using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMC.IServices
{
    public interface IWard
    {

        WardModel GetWard(int id);
        List<WardModel> GetAllWard();
        List<WardModel> GetAllWards();

        List<WardModel> GetAllWards(int Id);
        int GetZoneIdByWardId(int Id);
    }

}
