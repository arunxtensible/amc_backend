﻿using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMC.IServices
{
    public interface IData
    {
        IEnumerable<ZoneMaster> GetZones();
        IEnumerable<ZoneMaster> GetWDNZones();
        IEnumerable<ZoneMaster> GetReportZones();
        IEnumerable<ReportParentWDS> GetReportParentWDS();
        IEnumerable<ReportWard> GetReportWard();
        IEnumerable<ReportWDNLocation> GetReportWDNLocations();
        IEnumerable<ZoneMaster> GetZonesByType(string Type);
        IEnumerable<WTP_Master> GetWTPList();
        List<WardMaster> GetWardByZoneId(int ZoneId);
        List<DashboardGraphsModel> GetDashboardGraphData();
        DashboardAlertsModel GetDashboardAlertData();
        System.Data.DataTable GetWTPChartData(string WTPName, string KPIName, int Days);
        System.Data.DataTable GetWDSChartData(string ZoneName, string WDSName, string KPIName, int Days, string iterationDatetime);
        System.Data.DataTable GetWDSProjectScoreChartData(string ZoneName, string WDSLocation, string KPIName, string Days);
        System.Data.DataTable GetWDSProjectScoreChartDataPressure(string ZoneName, string WDSLocation, string KPIName, string Days);
        System.Data.DataTable GetWDNProjectScoreChartData(string ZoneName, string WardName, string Location, string KPIName, int Days);
        System.Data.DataTable GetWDNChartData(string ZoneName, string WardName, string Location, string KPIName, string Days, int iterationdays);
        //LineChartModel[] GetWDNChartDataCompare(string ZoneName, string WardName, string Location, string KPIName, string Days, int iterationdays);
        List<LineChartIterationDateModel> GetWDNChartDataLabel(string ZoneName, string WardName, string Location, string KPIName, string fromDate, string toDate);
        List<LineChartIterationDateModel> GetWDSChartDataLabel(string ZoneName, string Location, string KPIName, string fromDate, string toDate);
        LineChartModel[] GetWDNChartDataCompare(string ZoneName, string WardName, string Location, string KPIName, string fromDate, string toDate);
        LineChartModel[] GetWDSChartDataCompare(string ZoneName, string Location, string KPIName, string fromDate, string toDate);
        IEnumerable<WDSMaster> GetWDSList(int ZoneId);

        List<WDNMaster> GetWDNMaster(string zone, string ward);
        List<WDNMaster> GetLocations(string zone);
        List<WDSLocationModel> GetWDSLocationByZone(int zone);

        List<TampringAlerts> GetTampringAlertsData(string alert_type);

        List<UFWModel> GetUFWDetailsByType(string type);
        List<LDQValuesForUFWModel> GetUFW_LDQDetails();

        List<WaterVsEnergyModel> GetWaterVsEnergyDetails();

        IEnumerable<ReportParentWDS> GetReportParentWDSByZone(string Zone);
        IEnumerable<ReportWard> GetReportWardByParentWDS(string ParentWDS);
        IEnumerable<ReportWDNLocation> GetReportWDNLocationsByWard(string parentWDS, string Ward);
    }
}
