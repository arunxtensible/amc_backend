﻿using AMC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMC.IServices
{
    public interface IReport
    {
        List<ReportDataModelHourly> GetWDNReportByType(string Zone, string Ward, string Location, string ReportType, string Datetime);
        List<ReportDataModelMonthly> GetWDNReportByTypeMonthly(string Zone, string Ward, string Location, string ReportType, string Datetime);
        List<WDNLdqReportDataModel> GetWDNLdqReport( string Datetime);
        List<WDSLdqReportDataModel> GetWDSLdqReport( string Datetime);
        List<ReportDataModelWDSDetailsWithParentWTP> GetZonewiseWDSLDqReport(string Zone, string Datetime);

        List<ReportDataModelDB2DBDataHourly> GetWDNReportByDb2DBReader(string Location, string Datetime);

        List<ReportDataModelWDNDetailsWithParentWDS> GetWDNReportWithParentWDSDetails(string Zone, string ParentWDS, string Ward, string Location,string Datetime);
    }
}
